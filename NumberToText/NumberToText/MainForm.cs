﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NumberToText.Zubarev;

namespace NumberToText
{
	public partial class MainForm :Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			txtNumber.Text = 123.12.ToString();

			DoConvert();
		}

		private void btnExit_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnCopy_Click(object sender, EventArgs e)
		{
			if(txtResult.SelectionLength == 0)
				txtResult.SelectAll();

			txtResult.Copy();
		}

		private void txtNumber_TextChanged(object sender, EventArgs e)
		{
			DoConvert();
		}

		private void DoConvert()
		{
			try
			{
				double num = double.Parse( txtNumber.Text.Trim() );
				Валюта valuta = GetValuta();
				txtResult.Text = Сумма.Пропись(num, valuta);
				lblError.Visible = false;
			}
			catch(Exception ex)
			{
				ShowError( ex.Message );
				txtNumber.Focus();
			}
		}

		private void ShowError(string message)
		{
			lblError.Text = message;
			lblError.Visible = true;
			lblError.Location = txtResult.Location;
			lblError.Size = txtResult.Size;
		}

		private Валюта GetValuta()
		{
			if(rbRuble.Checked)
				return Валюта.Рубли;

			if(rbDollar.Checked)
				return Валюта.Доллары;

			if(rbEvro.Checked)
				return Валюта.Евро;

			return Валюта.Нет;
		}

		private void rb_CheckedChanged(object sender, EventArgs e)
		{
			DoConvert();
		}
	}
}
