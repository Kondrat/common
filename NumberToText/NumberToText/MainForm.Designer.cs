﻿namespace NumberToText
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.btnExit = new System.Windows.Forms.Button();
			this.txtNumber = new System.Windows.Forms.TextBox();
			this.txtResult = new System.Windows.Forms.TextBox();
			this.btnCopy = new System.Windows.Forms.Button();
			this.rbRuble = new System.Windows.Forms.RadioButton();
			this.rbDollar = new System.Windows.Forms.RadioButton();
			this.rbEvro = new System.Windows.Forms.RadioButton();
			this.rbNo = new System.Windows.Forms.RadioButton();
			this.lblError = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(14, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(42, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Число:";
			// 
			// btnExit
			// 
			this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnExit.Location = new System.Drawing.Point(417, 15);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(75, 23);
			this.btnExit.TabIndex = 5;
			this.btnExit.Text = "E&xit";
			this.btnExit.UseVisualStyleBackColor = true;
			this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
			// 
			// txtNumber
			// 
			this.txtNumber.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtNumber.Location = new System.Drawing.Point(62, 17);
			this.txtNumber.Name = "txtNumber";
			this.txtNumber.Size = new System.Drawing.Size(245, 22);
			this.txtNumber.TabIndex = 1;
			this.txtNumber.TextChanged += new System.EventHandler(this.txtNumber_TextChanged);
			// 
			// txtResult
			// 
			this.txtResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtResult.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtResult.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.txtResult.HideSelection = false;
			this.txtResult.Location = new System.Drawing.Point(17, 67);
			this.txtResult.Multiline = true;
			this.txtResult.Name = "txtResult";
			this.txtResult.ReadOnly = true;
			this.txtResult.Size = new System.Drawing.Size(477, 87);
			this.txtResult.TabIndex = 3;
			this.txtResult.Text = "213";
			// 
			// btnCopy
			// 
			this.btnCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCopy.Location = new System.Drawing.Point(321, 15);
			this.btnCopy.Name = "btnCopy";
			this.btnCopy.Size = new System.Drawing.Size(90, 23);
			this.btnCopy.TabIndex = 4;
			this.btnCopy.Text = "&Copy";
			this.btnCopy.UseVisualStyleBackColor = true;
			this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
			// 
			// rbRuble
			// 
			this.rbRuble.AutoSize = true;
			this.rbRuble.Checked = true;
			this.rbRuble.Location = new System.Drawing.Point(17, 43);
			this.rbRuble.Name = "rbRuble";
			this.rbRuble.Size = new System.Drawing.Size(55, 17);
			this.rbRuble.TabIndex = 6;
			this.rbRuble.TabStop = true;
			this.rbRuble.Text = "Рубли";
			this.rbRuble.UseVisualStyleBackColor = true;
			this.rbRuble.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
			// 
			// rbDollar
			// 
			this.rbDollar.AutoSize = true;
			this.rbDollar.Location = new System.Drawing.Point(78, 44);
			this.rbDollar.Name = "rbDollar";
			this.rbDollar.Size = new System.Drawing.Size(72, 17);
			this.rbDollar.TabIndex = 6;
			this.rbDollar.Text = "Доллары";
			this.rbDollar.UseVisualStyleBackColor = true;
			this.rbDollar.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
			// 
			// rbEvro
			// 
			this.rbEvro.AutoSize = true;
			this.rbEvro.Location = new System.Drawing.Point(156, 44);
			this.rbEvro.Name = "rbEvro";
			this.rbEvro.Size = new System.Drawing.Size(50, 17);
			this.rbEvro.TabIndex = 6;
			this.rbEvro.Text = "Евро";
			this.rbEvro.UseVisualStyleBackColor = true;
			this.rbEvro.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
			// 
			// rbNo
			// 
			this.rbNo.AutoSize = true;
			this.rbNo.Location = new System.Drawing.Point(212, 44);
			this.rbNo.Name = "rbNo";
			this.rbNo.Size = new System.Drawing.Size(59, 17);
			this.rbNo.TabIndex = 6;
			this.rbNo.Text = "ничего";
			this.rbNo.UseVisualStyleBackColor = true;
			this.rbNo.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
			// 
			// lblError
			// 
			this.lblError.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblError.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblError.ForeColor = System.Drawing.Color.Red;
			this.lblError.Location = new System.Drawing.Point(353, 44);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(127, 61);
			this.lblError.TabIndex = 7;
			this.lblError.Text = "label2";
			this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblError.Visible = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(504, 166);
			this.Controls.Add(this.lblError);
			this.Controls.Add(this.rbNo);
			this.Controls.Add(this.rbEvro);
			this.Controls.Add(this.rbDollar);
			this.Controls.Add(this.rbRuble);
			this.Controls.Add(this.btnCopy);
			this.Controls.Add(this.txtResult);
			this.Controls.Add(this.txtNumber);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Число прописью";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnExit;
		private System.Windows.Forms.TextBox txtNumber;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.Button btnCopy;
		private System.Windows.Forms.RadioButton rbRuble;
		private System.Windows.Forms.RadioButton rbDollar;
		private System.Windows.Forms.RadioButton rbEvro;
		private System.Windows.Forms.RadioButton rbNo;
		private System.Windows.Forms.Label lblError;
	}
}

