﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace RectTrackerNms
{
	public class RectTracker :Component
	{
		#region Handle

		private class Handle
		{
			public Handle(Cursor cursor, TrackedSideEnum trackedSide, int handleSize)
			{
				this.cursor = cursor;
				this.handleSize = handleSize;
				this.TrackedSide = trackedSide;
			}

			#region HandleSize

			private int handleSize;

			public int HandleSize
			{
				get { return handleSize; }
				set { handleSize = value; }
			}

			#endregion

			#region Cursor

			private readonly Cursor cursor;

			public Cursor Cursor
			{
				get
				{
					return cursor;
				}
			}

			#endregion

			#region Rectangle

			private Rectangle handleRectangle;

			public Rectangle HandleRectangle
			{
				get
				{
					return handleRectangle;
				}
			}

			#endregion

			#region TrackedSide

			public TrackedSideEnum TrackedSide { get; private set; }

			#endregion

			public void Paint(Graphics gr, Brush br, Pen pen)
			{
				gr.FillRectangle( br, this.handleRectangle );

				Rectangle rc = this.handleRectangle;
				gr.DrawRectangle(pen, rc);
				rc.Inflate(-1, -1);
				gr.DrawRectangle(pen, rc);
			}

			public void SetPosition(int x, int y)
			{
				int handleHalfSize = handleSize / 2;
				this.handleRectangle = new Rectangle(x - handleHalfSize, y - handleHalfSize, handleSize, handleSize);
			}

			public ProcessMouseDelegate ProcessMouse;

			public delegate void ProcessMouseDelegate(int x, int y);
		}

		#endregion

		private const int cnstFrameBorderWidth = 1;

		private bool bVisible = false;
		private Rectangle rectangle;

		readonly Handle[] handles = new Handle[8];

		public RectTracker()
		{
			InitHandles();
		}

		public RectTracker(Control control)
		{
			this.drawingControl = control;
			InitHandles();
		}

		#region InitHandles

		private void InitHandles()
		{
			handles[0] = new Handle( Cursors.SizeNWSE, TrackedSideEnum.Left | TrackedSideEnum.Top, frameWidth)
			             	{
			             		ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Width += this.rectangle.X - x;
			             		               		this.rectangle.X = x;
			             		               		this.rectangle.Height += this.rectangle.Y - y;
			             		               		this.rectangle.Y = y;
			             		               	}
			             	};
			handles[1] = new Handle(Cursors.SizeNS, TrackedSideEnum.Top, frameWidth)
			             	{
			             		ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Height += this.rectangle.Y - y;
			             		               		this.rectangle.Y = y;
			             		               	}
			             	};
			handles[2] = new Handle(Cursors.SizeNESW, TrackedSideEnum.Right | TrackedSideEnum.Top, frameWidth)
			             	{
								ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Width -= this.rectangle.Right - x;
			             		               		this.rectangle.Height += this.rectangle.Y - y;
			             		               		this.rectangle.Y = y;
			             		               	}
			             	};
			handles[3] = new Handle(Cursors.SizeWE, TrackedSideEnum.Left, frameWidth)
			             	{
								ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Width += this.rectangle.X - x;
			             		               		this.rectangle.X = x;
			             		               	}
			             	};
			handles[4] = new Handle(Cursors.SizeWE, TrackedSideEnum.Right, frameWidth)
			             	{
								ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Width -= this.rectangle.Right - x;
			             		               	}
			             	};
			handles[5] = new Handle(Cursors.SizeNESW, TrackedSideEnum.Left | TrackedSideEnum.Bottom, frameWidth)
			             	{
			             		ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Width += this.rectangle.X - x;
			             		               		this.rectangle.X = x;

			             		               		this.rectangle.Height = y - this.rectangle.Top;
			             		               	}
			             	};
			handles[6] = new Handle(Cursors.SizeNS, TrackedSideEnum.Bottom, frameWidth)
			             	{
			             		ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Height = y - this.rectangle.Top;
			             		               	}
			             	};
			handles[7] = new Handle(Cursors.SizeNWSE, TrackedSideEnum.Right | TrackedSideEnum.Bottom, frameWidth)
			             	{
			             		ProcessMouse = (x, y) =>
			             		               	{
			             		               		this.rectangle.Width -= this.rectangle.Right - x;

			             		               		this.rectangle.Height = y - this.rectangle.Top;
			             		               	}
			             	};
		}

		#endregion

		#region FrameWidth

		private int frameWidth = 6;

		[Browsable(true)]
		public int FrameWidth
		{
			get { return frameWidth; }
			set
			{
				frameWidth = value;
				foreach(Handle handle in handles)
				{
					handle.HandleSize = value;
				}
			}
		}

		#endregion

		#region DrawingControl

		private Control drawingControl;

		[Browsable(true)]
		//[Editor(typeof(ObjectSelectorEditor), typeof(Control))]
		public Control DrawingControl
		{
			get { return drawingControl; }
			set { this.drawingControl = value; }
		}

		#endregion

		#region Colors

		#region SnapLineColorInDrag

		private Color snapLineColorInDrag = Color.DarkGray;

		[Browsable(true)]
		public Color SnapLineColorInDrag
		{
			get { return snapLineColorInDrag; }
			set { snapLineColorInDrag = value; }
		}

		#endregion

		#region SnapLineColor

		private Color snapLineColor = Color.LightGray;

		[Browsable(true)]
		public Color SnapLineColor
		{
			get { return snapLineColor; }
			set { snapLineColor = value; }
		}

		#endregion

		#region FrameBackColor

		private Color frameBackColor = Color.WhiteSmoke;

		[Browsable(true)]
		public Color FrameBackColor
		{
			get { return frameBackColor; }
			set { frameBackColor = value; }
		}

		#endregion

		#region FrameForeColor

		private Color frameForeColor = Color.Black;

		[Browsable(true)]
		public Color FrameForeColor
		{
			get { return frameForeColor; }
			set { frameForeColor = value; }
		}

		#endregion

		#endregion

		#region Rectangle

		[Browsable(false)]
		public Rectangle Rectangle
		{
			get { return rectangle; }
		}

		#endregion

		#region MinTrackingSize

		private Size minTrackingSize = new Size(50, 50);

		[Browsable(true)]
		public Size MinTrackingSize
		{
			get
			{
				return minTrackingSize;
			}
			set
			{
				minTrackingSize = value;
			}
		}

		#endregion

		#region MoveOnlyByContent

		[Browsable(true)]
		public bool MoveByContent { get; set; }

		#endregion

		#region Events

		#region Dragging

		[Flags]
		public enum TrackedSideEnum
		{
			None = 0x00,
			Top = 0x01,
			Bottom = 0x01 << 1,
			Right = 0x01 << 2,
			Left = 0x01 << 3,
			All = Top | Bottom | Right | Left
		}

		#region TrackedEventArgs

		[Serializable]
		public class TrackedEventArgs
		{
			private bool cancel;
			private readonly TrackedSideEnum trackedSide;

			public TrackedEventArgs(bool cancel, TrackedSideEnum trackedSide)
			{
				this.cancel = cancel;
				this.trackedSide = trackedSide;
			}

			public bool Cancel
			{
				get { return cancel; }
				set { this.cancel = value; }
			}

			public TrackedSideEnum TrackedSide
			{
				get { return trackedSide; }
			}
		}

		#endregion

		public delegate void TrackedEventHandler(object sender, TrackedEventArgs e);

		public event TrackedEventHandler Dragging;

		private TrackedEventArgs RaiseDragged(TrackedSideEnum trackedSide)
		{
			if(Dragging != null)
			{
				var args = new TrackedEventArgs( false, trackedSide );
				Dragging( this, args );
				return args;
			}

			return new TrackedEventArgs( false, TrackedSideEnum.None );
		}

		#endregion

		#region DragFinished

		public event EventHandler DragFinished;

		private void RaiseDragFinished()
		{
			if(DragFinished != null)
				DragFinished( this, EventArgs.Empty );
		}

		#endregion

		#endregion

		#region ConstrainedByWindow

		private bool constrainedByWindow = true;

		[Browsable(true)]
		public bool ConstrainedByWindow
		{
			get { return constrainedByWindow; }
			set { constrainedByWindow = value; }
		}

		#endregion

		#region Snapping

		#region SnappedTracking

		private bool snappedTracking = false;

		[Browsable(true)]
		public bool SnappedTracking
		{
			get { return snappedTracking; }
			set { snappedTracking = value; }
		}

		#endregion

		#region VerticalSnaps

		private int[] verticalSnaps = new int[0];

		[Browsable(true)]
		public int[] VerticalSnaps
		{
			get { return verticalSnaps; }
			set { verticalSnaps = value!= null? value.Distinct().ToArray(): new int[0]; }
		}

		#endregion

		#region HorisontalSnaps

		private int[] horisontalSnaps = new int[0];

		[Browsable(true)]
		public int[] HorisontalSnaps
		{
			get { return horisontalSnaps; }
			set { horisontalSnaps = value != null ? value.Distinct().ToArray() : new int[0]; }
		}

		#endregion

		#region SnapDistance

		private int snapDistance = 10;

		[Browsable(true)]
		public int SnapDistance
		{
			get { return snapDistance; }
			set { snapDistance = value; }
		}

		#endregion

		#region DrawSnaps

		private bool drawSnaps = true;

		[Browsable(true)]
		public bool DrawSnaps
		{
			get { return drawSnaps; }
			set { drawSnaps = value; }
		}

		#endregion

		#endregion

		public void Show(Rectangle rect)
		{
			this.rectangle = rect;

			if(bVisible == false)
			{
				SubscribeEvents();
				this.bVisible = true;
			}

			this.drawingControl.Invalidate();
		}

		public void Hide()
		{
			this.bVisible = false;
			UnSubscribeEvents();
			this.drawingControl.Invalidate(this.rectangle);
		}

		private void RecalcHandlePositions(Rectangle rcFrame)
		{
			handles[0].SetPosition( rcFrame.Left, rcFrame.Top );
			handles[1].SetPosition( rcFrame.Left + rcFrame.Width / 2, rcFrame.Top);
			handles[2].SetPosition( rcFrame.Left + rcFrame.Width, rcFrame.Top);
			handles[3].SetPosition( rcFrame.Left, rcFrame.Top + rcFrame.Height/2 );
			handles[4].SetPosition( rcFrame.Left + rcFrame.Width, rcFrame.Top + rcFrame.Height / 2);
			handles[5].SetPosition( rcFrame.Left, rcFrame.Top + rcFrame.Height );
			handles[6].SetPosition( rcFrame.Left + rcFrame.Width/2, rcFrame.Top + rcFrame.Height );
			handles[7].SetPosition( rcFrame.Left + rcFrame.Width, rcFrame.Top + rcFrame.Height );
		}

		public void TryPaint(Graphics gr)
		{
			if(!bVisible)
				return;

			using(Brush borderBrush = new HatchBrush(HatchStyle.Percent50, frameForeColor, frameBackColor))
			using (Pen borderPen = new Pen(borderBrush, cnstFrameBorderWidth))
			using(Brush frameBrush = new HatchBrush(HatchStyle.BackwardDiagonal, frameForeColor, frameBackColor))
			using(Pen framePen = new Pen(frameBrush, frameWidth))
			{
				Rectangle rcFrame = this.rectangle;
				rcFrame.Inflate( -frameWidth / 2, -frameWidth / 2 );

				DrawFrame( gr, framePen, rcFrame );
				DrawFrameBorder( gr, borderPen );
				RecalcHandlePositions(rcFrame);
				DrawHandles(gr);
			}
		}

		public void TryDrawSnapLines(Graphics gr)
		{
			if (this.DrawSnaps && this.SnappedTracking)
			{
				Color foreColor = mouseMoveProcessor != null? snapLineColorInDrag: snapLineColor;
				using(Pen pen = new Pen( foreColor, 1 ))
				{
					pen.DashStyle = DashStyle.Dot;

					int bottom = this.drawingControl.ClientRectangle.Bottom;
					foreach(int verticalSnap in VerticalSnaps)
					{
						gr.DrawLine( pen, verticalSnap, 0, verticalSnap, bottom );
					}

					int right = this.drawingControl.ClientRectangle.Width;
					foreach(int horisontalSnap in HorisontalSnaps)
					{
						gr.DrawLine(pen, 0, horisontalSnap, right, horisontalSnap);
					}
				}
			}
		}

		private void DrawFrame(Graphics gr, Pen framePen, Rectangle rcFrame)
		{
			gr.DrawRectangle(framePen, rcFrame);
		}

		private void DrawHandles(Graphics gr)
		{
			using (Pen solidPen = new Pen(frameForeColor, cnstFrameBorderWidth))
			using (Brush br = new SolidBrush(frameBackColor))
			{
				foreach(Handle handle in handles)
				{
					handle.Paint(gr, br, solidPen);
				}
			}
		}

		private void DrawFrameBorder(Graphics gr, Pen borderPen)
		{
			gr.DrawRectangle(borderPen, GetOuterFrameBorder());

			Rectangle rcInnerBorder = GetInnerFrameBorder();
			gr.DrawRectangle(borderPen, rcInnerBorder);
		}

		private Rectangle GetOuterFrameBorder()
		{
			return this.rectangle;
		}

		private Rectangle GetInnerFrameBorder()
		{
			Rectangle rcInnerBorder = this.rectangle;
			rcInnerBorder.Inflate(-frameWidth, -frameWidth);
			return rcInnerBorder;
		}

		private void OnMouseMove(object sender, MouseEventArgs e)
		{
			if(this.drawingControl.Capture)
			{
				if(mouseMoveProcessor != null)
				{
					Rectangle prewRect = this.rectangle;

					mouseMoveProcessor( e.X, e.Y );

					if (this.SnappedTracking)
						DoSnapping(ref this.rectangle);

					if(this.rectangle.Height < minTrackingSize.Height)
					{
						this.rectangle.Height = minTrackingSize.Height;
						this.rectangle.Y = prewRect.Y;
					}

					if(this.rectangle.Width < minTrackingSize.Width)
					{
						this.rectangle.Width = minTrackingSize.Width;
						this.rectangle.X = prewRect.X;
					}

					if(ConstrainedByWindow && !IsContainsInWindow(this.rectangle))
					{
						this.rectangle = prewRect;
						return;
					}

					var notifyResult = RaiseDragged(this.trackedSide);
					if (notifyResult.Cancel)
						this.rectangle = prewRect;

					this.drawingControl.Invalidate();
				}
			}
			else
				SetCursor( e.X, e.Y );
		}

		private void DoSnapping(ref Rectangle rect)
		{
			if ((Control.ModifierKeys & Keys.Control) == Keys.Control)
				return;

			if (trackedSide == TrackedSideEnum.All)
			{
				int? snapLeft = FindSnapPosition( rect.X, VerticalSnaps );
				if(snapLeft.HasValue)
					rect.X = snapLeft.Value;

				int? snapY = FindSnapPosition( rect.Y, HorisontalSnaps );
				if(snapY.HasValue)
					rect.Y = snapY.Value;

				int? snapRight = FindSnapPosition(rect.Right, VerticalSnaps);
				if (snapRight.HasValue)
					rect.X = snapRight.Value - rect.Width;

				int? snapBottom = FindSnapPosition(rect.Bottom, HorisontalSnaps);
				if (snapBottom.HasValue)
					rect.Y = snapBottom.Value - rect.Height;

				return;
			}

			if((trackedSide & TrackedSideEnum.Left) == TrackedSideEnum.Left)
			{
				int? snapX = FindSnapPosition(rect.X, VerticalSnaps);
				if (snapX.HasValue)
				{
					int dX = rect.X - snapX.Value;
					rect.X = snapX.Value;
					rect.Width += dX;
				}
			}

			if((trackedSide & TrackedSideEnum.Top) == TrackedSideEnum.Top)
			{
				int? snapY = FindSnapPosition(rect.Y, HorisontalSnaps);
				if (snapY.HasValue)
				{
					int dY = rect.Y - snapY.Value;
					rect.Y = snapY.Value;
					rect.Height += dY;
				}
			}

			if((trackedSide & TrackedSideEnum.Right) == TrackedSideEnum.Right)
			{
				int? snapX = FindSnapPosition(rect.Right, VerticalSnaps);
				if(snapX.HasValue)
					rect.Width = snapX.Value - rect.Left;
			}

			if((trackedSide & TrackedSideEnum.Bottom) == TrackedSideEnum.Bottom)
			{
				int? snapY = FindSnapPosition(rect.Bottom, HorisontalSnaps);
				if(snapY.HasValue)
					rect.Height = snapY.Value - rect.Top;
			}
		}

		private int? FindSnapPosition(int side, IEnumerable<int> snaps)
		{
			var seq = from sn in snaps
			          where Math.Abs( side - sn ) <= SnapDistance
			          select sn;

			int[] snap = seq.ToArray();
			return snap.Length > 0? snap[0]: (int?)null;
		}

		public bool IsContainsInWindow(Rectangle rect)
		{
			return this.drawingControl.ClientRectangle.Contains(rect);
		}

		private bool IsCursorAboveRightOrBottomEdge(int x, int y)
		{
			return x == this.rectangle.Right
			       || y == this.rectangle.Bottom;
		}

		private void OnMouseDown(object sender, MouseEventArgs e)
		{
			Handle draggedHandle = GetHandleUnderMouse(e.X, e.Y);
			if(draggedHandle != null && !IsCursorAboveRightOrBottomEdge(e.X, e.Y))
			{
				mouseMoveProcessor = draggedHandle.ProcessMouse;
				trackedSide = draggedHandle.TrackedSide;
				this.drawingControl.Capture = true;
			}
			else
			{
				if(IsMouseInMoveArea(e.X, e.Y))
				{
					int dX = e.X - this.rectangle.X;
					int dY = e.Y - this.rectangle.Y;

					mouseMoveProcessor = (x, y) =>
					                     	{
					                     		this.rectangle.X = x - dX;
					                     		this.rectangle.Y = y - dY;
					                     	};
					trackedSide = TrackedSideEnum.All;
				}
			}
		}

		void OnMouseUp(object sender, MouseEventArgs e)
		{
			if (mouseMoveProcessor != null)
			{
				this.drawingControl.Capture = false;
				mouseMoveProcessor = null;
				RaiseDragFinished();
				this.drawingControl.Invalidate();
			}
		}

		private void SetCursor(int x, int y)
		{
			Handle underMouse = GetHandleUnderMouse( x, y );

			Cursor cursorToSet = underMouse != null && !IsCursorAboveRightOrBottomEdge(x, y)
			                     	? underMouse.Cursor
			                     	: IsMouseInMoveArea( x, y ) ? Cursors.SizeAll : Cursors.Default;

			if(!(this.drawingControl.Cursor == cursorToSet))
				this.drawingControl.Cursor = cursorToSet;
		}

		private Handle.ProcessMouseDelegate mouseMoveProcessor;
		private TrackedSideEnum trackedSide;

		private Handle GetHandleUnderMouse(int x, int y)
		{
			return this.handles
				.Where(el => el.HandleRectangle.Contains(x, y))
				.FirstOrDefault();
		}

		private bool IsMouseInMoveArea(int x, int y)
		{
			if(MoveByContent)
				return GetOuterFrameBorder().Contains( x, y );
			else
				return GetOuterFrameBorder().Contains(x, y)
					   && !GetInnerFrameBorder().Contains(x, y);
		}

		private void SubscribeEvents()
		{
			this.drawingControl.MouseMove += OnMouseMove;
			this.drawingControl.MouseDown += OnMouseDown;
			this.drawingControl.MouseUp += OnMouseUp;
		}

		private void UnSubscribeEvents()
		{
			this.drawingControl.MouseMove -= OnMouseMove;
			this.drawingControl.MouseDown -= OnMouseDown;
			this.drawingControl.MouseUp -= OnMouseUp;
		}
	}
}
