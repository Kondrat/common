﻿namespace RectTrackerNms
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnClose = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.txt = new System.Windows.Forms.TextBox();
			this.pnlSrc = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.pnlDrawing = new RectTrackerNms.DrawingPanel();
			this.tracker = new RectTrackerNms.RectTracker();
			this.pnlSrc.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(585, 12);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 45);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "Exit";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(585, 109);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 53);
			this.button1.TabIndex = 2;
			this.button1.Text = "Invalidate";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txt
			// 
			this.txt.Dock = System.Windows.Forms.DockStyle.Top;
			this.txt.Location = new System.Drawing.Point(3, 3);
			this.txt.Name = "txt";
			this.txt.Size = new System.Drawing.Size(67, 20);
			this.txt.TabIndex = 3;
			this.txt.Text = "txt";
			// 
			// pnlSrc
			// 
			this.pnlSrc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pnlSrc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlSrc.Controls.Add(this.txt);
			this.pnlSrc.Location = new System.Drawing.Point(585, 191);
			this.pnlSrc.Name = "pnlSrc";
			this.pnlSrc.Padding = new System.Windows.Forms.Padding(3);
			this.pnlSrc.Size = new System.Drawing.Size(75, 72);
			this.pnlSrc.TabIndex = 4;
			this.pnlSrc.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlSrc_MouseMove);
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.pnlDrawing);
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(567, 418);
			this.panel1.TabIndex = 5;
			// 
			// pnlDrawing
			// 
			this.pnlDrawing.AllowDrop = true;
			this.pnlDrawing.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlDrawing.Location = new System.Drawing.Point(0, 0);
			this.pnlDrawing.Name = "pnlDrawing";
			this.pnlDrawing.Size = new System.Drawing.Size(563, 414);
			this.pnlDrawing.TabIndex = 1;
			this.pnlDrawing.SizeChanged += new System.EventHandler(this.pnlDrawing_SizeChanged);
			this.pnlDrawing.DragDrop += new System.Windows.Forms.DragEventHandler(this.pnlDrawing_DragDrop);
			this.pnlDrawing.DragOver += new System.Windows.Forms.DragEventHandler(this.pnlDrawing_DragOver);
			this.pnlDrawing.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlDrawing_Paint);
			this.pnlDrawing.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDrawing_MouseDown);
			// 
			// tracker
			// 
			this.tracker.ConstrainedByWindow = true;
			this.tracker.DrawingControl = this.pnlDrawing;
			this.tracker.DrawSnaps = true;
			this.tracker.FrameBackColor = System.Drawing.Color.Yellow;
			this.tracker.FrameForeColor = System.Drawing.Color.Blue;
			this.tracker.FrameWidth = 5;
			this.tracker.HorisontalSnaps = new int[0];
			this.tracker.MinTrackingSize = new System.Drawing.Size(100, 50);
			this.tracker.MoveByContent = true;
			this.tracker.SnapDistance = 10;
			this.tracker.SnapLineColor = System.Drawing.SystemColors.MenuBar;
			this.tracker.SnapLineColorInDrag = System.Drawing.SystemColors.MenuHighlight;
			this.tracker.SnappedTracking = true;
			this.tracker.VerticalSnaps = new int[0];
			this.tracker.Dragging += new RectTrackerNms.RectTracker.TrackedEventHandler(this.tracker_Dragging);
			this.tracker.DragFinished += new System.EventHandler(this.tracker_DragFinished);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(672, 442);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.pnlSrc);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.btnClose);
			this.MinimumSize = new System.Drawing.Size(410, 340);
			this.Name = "MainForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.pnlSrc.ResumeLayout(false);
			this.pnlSrc.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnClose;
		private DrawingPanel pnlDrawing;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txt;
		private System.Windows.Forms.Panel pnlSrc;
		private RectTracker tracker;
		private System.Windows.Forms.Panel panel1;
	}
}

