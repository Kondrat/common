﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace RectTrackerNms
{
	public partial class MainForm :Form
	{
		readonly List<Shape> lstShapes = new List<Shape>();
		private Shape selectedShape;

		private Bitmap surface;

		public MainForm()
		{
			InitializeComponent();
			SetSnaps();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void CreateSurface()
		{
			if(this.surface != null)
				this.surface.Dispose();

			this.surface = new Bitmap(pnlDrawing.ClientRectangle.Width, pnlDrawing.ClientRectangle.Height);
		}

		private void pnlDrawing_Paint(object sender, PaintEventArgs e)
		{
			using(Graphics grSurface = pnlDrawing.CreateGraphics())
			using(Graphics grToDraw = Graphics.FromImage( this.surface ))
			{
				using(Brush br = new SolidBrush(SystemColors.Window))
				{
					grToDraw.FillRectangle(br, pnlDrawing.ClientRectangle);
				}

				this.tracker.TryDrawSnapLines(grToDraw);

				PaintRectangles(grToDraw);

				if(pnlDrawing.Focused)
					this.tracker.TryPaint(grToDraw);

				grSurface.DrawImageUnscaled(this.surface, 0, 0);
			}
		}

		private void PaintRectangles(Graphics gr)
		{
			foreach(Shape shape in lstShapes)
			{
				shape.Paint( gr );
			}
		}

		private void pnlDrawing_MouseDown(object sender, MouseEventArgs e)
		{
			selectedShape = GetShapeUnderMouse( e );

			if(selectedShape != null)
				tracker.Show(selectedShape.BoundRect);
			else
				tracker.Hide();
		}

		void tracker_DragFinished(object sender, EventArgs e)
		{
			SetSnaps();
		}

		private void SetSnaps()
		{
			List<int> verticalSnaps = new List<int>();
			List<int> horisontalSnaps = new List<int>();

			verticalSnaps.Add(pnlDrawing.ClientRectangle.Left);
			verticalSnaps.Add(pnlDrawing.ClientRectangle.Right);

			horisontalSnaps.Add(pnlDrawing.ClientRectangle.Top);
			horisontalSnaps.Add(pnlDrawing.ClientRectangle.Bottom);

			foreach (Shape shape in lstShapes)
			{
				Rectangle shapeBound = shape.BoundRect;

				verticalSnaps.Add(shapeBound.Left);
				verticalSnaps.Add(shapeBound.Right);

				horisontalSnaps.Add(shapeBound.Top);
				horisontalSnaps.Add(shapeBound.Bottom);
			}

			tracker.VerticalSnaps = verticalSnaps.ToArray();
			tracker.HorisontalSnaps = horisontalSnaps.ToArray();
		}

		void tracker_Dragging(object sender, RectTracker.TrackedEventArgs e)
		{
			if(selectedShape != null)
			{
				e.Cancel = HasCollision(tracker.Rectangle, true);
				if(e.Cancel)
					return;

				selectedShape.BoundRect = tracker.Rectangle;
				pnlDrawing.Invalidate();
			}
		}

		private bool HasCollision(Rectangle rectangle, bool bSkipSelectedShape)
		{
			IEnumerable<Shape> seq = lstShapes;

			if(bSkipSelectedShape)
				seq = seq.Where( el => !object.ReferenceEquals( el, selectedShape ) );

			bool bHasIntersection = seq.Any( el => el.BoundRect.IntersectsWith(rectangle));

			if(bHasIntersection)
				return true;

			return false;
		}

		private Shape GetShapeUnderMouse(MouseEventArgs e)
		{
			return lstShapes
				.Where( el => el.BoundRect.Contains( e.X, e.Y ) )
				.FirstOrDefault();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			pnlDrawing.Invalidate();
		}

		private void pnlDrawing_SizeChanged(object sender, EventArgs e)
		{
			CreateSurface();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			CreateSurface();
		}

		private void pnlSrc_MouseMove(object sender, MouseEventArgs e)
		{
			if ((Control.MouseButtons & MouseButtons.Left) == MouseButtons.Left)
			{
				string text = txt.Text.Trim();
				if(text.Length > 0)
				{
					ShapeRectangle shape = new ShapeRectangle( 0, 0, 10, 10 ) {Text = text};
					pnlSrc.DoDragDrop( shape, DragDropEffects.Move );
				}
			}
		}

		private void pnlDrawing_DragOver(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(ShapeRectangle)))
			{
				var rc = GetRectangleForDropped( e );
				if(HasCollision( rc, false ) ||  tracker.ConstrainedByWindow && !tracker.IsContainsInWindow(rc))
					e.Effect = DragDropEffects.None;
				else
					e.Effect = DragDropEffects.Move;
			}
			else
				e.Effect = DragDropEffects.None;
		}

		private void pnlDrawing_DragDrop(object sender, DragEventArgs e)
		{
			ShapeRectangle dropped = e.Data.GetData( typeof(ShapeRectangle) ) as ShapeRectangle;
			if(dropped != null)
			{
				dropped.BoundRect = GetRectangleForDropped( e );

				lstShapes.Add(dropped);
				SetSnaps();

				this.selectedShape = dropped;
				tracker.Show(selectedShape.BoundRect);

				pnlDrawing.Focus();
				pnlDrawing.Invalidate();
			}
		}

		private Rectangle GetRectangleForDropped(DragEventArgs e)
		{
			Rectangle rc = new Rectangle();
			rc.X = e.X;
			rc.Y = e.Y;
			rc.Width = tracker.MinTrackingSize.Width;
			rc.Height = tracker.MinTrackingSize.Height;
			return pnlDrawing.RectangleToClient(rc);
		}
	}
}
