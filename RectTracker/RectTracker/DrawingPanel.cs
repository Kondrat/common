﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RectTrackerNms
{
	public sealed class DrawingPanel: Control
	{
		public DrawingPanel()
		{
			this.TabStop = true;
		}

		protected override void OnResize(EventArgs eventargs)
		{
			base.OnResize(eventargs);
			this.Invalidate();
		}

		protected override void OnLeave(EventArgs e)
		{
			base.OnLeave(e);
			Invalidate();
		}

		protected override void OnEnter(EventArgs e)
		{
			base.OnEnter(e);
			Invalidate();
		}

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			if (DesignMode)
			{
				base.OnPaintBackground( pevent );
				using(Brush brush = new SolidBrush(Color.DarkGray))
				{
					pevent.Graphics.FillRectangle(brush, ClientRectangle);
				}
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			this.Focus();
			base.OnMouseDown(e);
		}


	}
}
