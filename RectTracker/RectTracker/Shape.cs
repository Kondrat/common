﻿using System;
using System.Drawing;

namespace RectTrackerNms
{
	abstract class Shape
	{
		protected abstract void OnPaint(Graphics gr);

		public void Paint(Graphics gr)
		{
			var state = gr.Save();
			try
			{
				OnPaint( gr );
			}
			finally
			{
				gr.Restore( state );
			}
		}


		public abstract Rectangle BoundRect { get; set; }
	}


	internal class ShapeRectangle: Shape
	{
		private Rectangle bound;

		readonly Font font = new Font(FontFamily.GenericSerif, 10);
		private Brush br = new SolidBrush( Color.DarkMagenta );

		private readonly Color borderColor = Color.DarkViolet;
		private readonly Color groundColor = Color.YellowGreen;

		public ShapeRectangle(int x, int y, int width, int height)
		{
			this.bound = new Rectangle(x, y, width, height);
		}

		#region Text

		private string text;

		public string Text
		{
			get { return text; }
			set { text = value; }
		}

		#endregion

		protected override void OnPaint(Graphics gr)
		{
			PaintGround( gr );
			PaintFrame( gr );
			PaintText( gr );
		}

		private void PaintText(Graphics gr)
		{
			RectangleF rc = new RectangleF( BoundRect.X, BoundRect.Y, BoundRect.Width, BoundRect.Height);
			StringFormat fmt = StringFormat.GenericTypographic;
			fmt.Alignment = StringAlignment.Center;
			fmt.LineAlignment = StringAlignment.Center;
			gr.DrawString( this.text, font, br, rc, new StringFormat( fmt ) );
		}

		private void PaintFrame(Graphics gr)
		{
			gr.SetClip( this.bound );
			using(Pen pen = new Pen(this.borderColor, 4))
			{
				gr.DrawRectangle( pen, this.bound);
			}
		}

		private void PaintGround(Graphics gr)
		{
			using(Brush br = new SolidBrush(this.groundColor))
			{
				gr.FillRectangle( br, this.bound);
			}
		}

		public override Rectangle BoundRect
		{
			get
			{
				return this.bound;
			}
			set
			{
				this.bound = value;
			}
		}
	}
}
