﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Halo
{
    internal class HaloPainter
    {
        public static void DrawGlowTextInRectangleCenter(PaintEventArgs e, string text, Font font, RectangleF rectangle)
        {
            var sf = StringFormat.GenericTypographic;

            var textSize = e.Graphics.MeasureString(text, font, new PointF(0,0), sf);

            //Create a bitmap in a fixed ratio to the original drawing area.
            using (Bitmap bmp = new Bitmap((int)textSize.Width / 5 , (int)textSize.Height /5 ))
            {
                //Create a GraphicsPath object. 
                using (GraphicsPath pth = new GraphicsPath())
                {
                    //Add the string in the chosen style. 
                    pth.AddString(text, 
                        font.FontFamily, 
                        (int)font.Style, 
                        font.SizeInPoints,
                        new Point(3, 0), 
                        sf);

                    //Get the graphics object for the image. 
                    using (Graphics grBmp = Graphics.FromImage(bmp))
                    {
                        //Choose an appropriate smoothing mode for the halo. 
                        grBmp.SmoothingMode = SmoothingMode.AntiAlias;
                        //Transform the graphics object so that the same half may be used for both halo and text output. 
                        grBmp.ScaleTransform( 1/5f, 1/5f);
                        //Using a suitable pen...
                        using (Pen pen = new Pen(Color.Gray, 3))
                        {
                            //Draw around the outline of the path
                            grBmp.DrawPath(pen, pth);
                        }
                        //and then fill in for good measure. 
                        //g.FillPath(Brushes.Gray, pth);
                    }

                    //this just shifts the effect a little bit so that the edge isn't cut off in the demonstration
                    //e.Graphics.Transform = new Matrix(1, 0, 0, 1, 0, 0);
                    e.Graphics.ResetTransform();
                    //setup the smoothing mode for path drawing
                    e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    //and the interpolation mode for the expansion of the halo bitmap
                    e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    //expand the halo making the edges nice and fuzzy. 

                    var pthRect = pth.GetBounds();
                    float posX = (rectangle.Width - pthRect.Width) / 2;
                    float posY = (rectangle.Height - pthRect.Height) / 2;

                    e.Graphics.TranslateTransform( posX, posY);
                    e.Graphics.DrawImage(bmp, 
                        new Rectangle(-1, -2, (int)textSize.Width-1, (int)textSize.Height-2), 
                        0, 0, bmp.Width, bmp.Height, 
                        GraphicsUnit.Pixel
                        );
                    //Redraw the original text
                    e.Graphics.FillPath(SystemBrushes.WindowText, pth);
                    //e.Graphics.DrawString(text, font, SystemBrushes.WindowText, pos);
                }
            }
        }

        public static void DrawGlowRect(PaintEventArgs e, RectangleF rc)
        {
            var save = e.Graphics.Save();

            //Create a bitmap in a fixed ratio to the original drawing area.
            using (Bitmap bm = new Bitmap((int)rc.Width / 5 + 8, (int)rc.Height / 5 + 8))
            {
                //Create a GraphicsPath object. 
                using (GraphicsPath pth = new GraphicsPath())
                {
                    var rcGlow = rc;
                    rcGlow.Offset(-rc.Left + 5, -rc.Top + 5);
                    pth.AddRectangles(new[] { rcGlow });

                    //Add the string in the chosen style. 
                    //pth.AddString("Text Halo", new FontFamily("Verdana"), (int)FontStyle.Regular, 100, new Point(20, 20), StringFormat.GenericTypographic);
                    //Get the graphics object for the image. 
                    using (Graphics g = Graphics.FromImage(bm))
                    {
                        //Create a matrix that shrinks the drawing output by the fixed ratio. 
                        Matrix mx = new Matrix(1.0f / 5, 0, 0, 1.0f / 5, -(1.0f / 5), -(1.0f / 5));
                        //Choose an appropriate smoothing mode for the halo. 
                        g.SmoothingMode = SmoothingMode.AntiAlias;
                        //Transform the graphics object so that the same half may be used for both halo and text output. 
                        g.Transform = mx;
                        //Using a suitable pen...
                        using (Pen pen = new Pen(Color.Gray, 2))
                        {
                            //Draw around the outline of the path
                            g.DrawPath(pen, pth);
                        }
                        //and then fill in for good measure. 
                        //g.FillPath(Brushes.Blue, pth);
                    }
                    //this just shifts the effect a little bit so that the edge isn't cut off in the demonstration
                    e.Graphics.Transform = new Matrix(1, 0, 0, 1, 0, 0);
                    //setup the smoothing mode for path drawing
                    e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                    //and the interpolation mode for the expansion of the halo bitmap
                    e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    //expand the halo making the edges nice and fuzzy. 

                    Rectangle rcI = new Rectangle(
                        (int)rc.Left - 4, 
                        (int)rc.Top - 4, 
                        (int)rc.Width + 8,
                        (int)rc.Height + 8
                        );

                    e.Graphics.DrawImage(bm, rcI, 0, 0, bm.Width - 6, bm.Height - 6, GraphicsUnit.Pixel);
                    //Redraw the original text
                    //e.Graphics.FillPath(Brushes.Black, pth);
                }
            }


            e.Graphics.DrawRectangles(Pens.DarkBlue, new[] { rc });
            e.Graphics.FillRectangle(Brushes.Gray, rc);

            e.Graphics.Restore(save);
        }
    }
}
