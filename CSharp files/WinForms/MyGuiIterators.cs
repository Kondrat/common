﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GPS.Gui.Shared
{
	public static class MyGuiIterators
	{
		public static IEnumerable<TreeNode> TreeNodesIterator(TreeNodeCollection nodes)
		{
			foreach(TreeNode node in nodes)
			{
				yield return node;
				foreach(TreeNode childNode in TreeNodesIterator(node.Nodes))
				{
					yield return childNode;
				}
			}
		}
	}
}