using System;
using System.Windows.Forms;

public class SetCursor :IDisposable
{
	readonly Control ctrl;
	Cursor oldCursor;
	//Cursor cursorToSet;

	public SetCursor( Control ctrl) 
		:this(ctrl, Cursors.WaitCursor)
	{
		// Empty
	}

	public SetCursor( Control ctrl, Cursor cursor) 
	{
		this.ctrl = ctrl;
		SetupCursor( cursor );
	}

	private void SetupCursor(Cursor cursor)
	{
		Form form = this.ctrl.FindForm();
		if( form != null)
		{
			form.Invoke( (EventHandler)delegate
			                           {
			                           	this.oldCursor = form.Cursor;
			                           	form.Cursor = cursor;
			                           }
				);
		}
	}

	#region IDisposable Members

	public void Dispose()
	{
		if(this.ctrl != null)
			SetupCursor( this.oldCursor );

		GC.SuppressFinalize(this);
	}

	#endregion

	#region Factory's

	public static SetCursor Wait( Control ctrl)
	{
		return new SetCursor( ctrl);
	}

	public static SetCursor New( Control ctrl, Cursor cursor)
	{
		return new SetCursor( ctrl, cursor);
	}

	#endregion
}
