﻿using System;
using System.Windows.Forms;


public static class ControlExtension
{
	public static void Invoke(this Control ctrl, Action action)
	{
		ctrl.Invoke(
			(EventHandler)delegate
			              	{
								action();
			              	}
			);
	}
	public static IAsyncResult InvokeAsync(this Control ctrl, Action action)
	{
		return ctrl.BeginInvoke(
			(EventHandler)delegate
			              	{
								action();
			              	}
			);
	}
}

