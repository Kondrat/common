using System;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Common
{
	public class ApplicationSingleton
	{
		static Mutex mutex;
		public static bool IsInstanceExists( string uniqueName)
		{
			bool createdNew;
			mutex = new Mutex(false, uniqueName, out createdNew);
			return !createdNew;
		}

		public static void TryActivateExistingInstance( string uniqueName)
		{
			bool createdNew;
			Mutex mutex = new Mutex(false, uniqueName, out createdNew);
			if (!createdNew)
			{
				//�������� ��� ������ �������� (�������� ����� ��� ���������� '.exe')
				string processName = Process.GetCurrentProcess().MainModule.ModuleName;
				processName = processName.Substring(0, processName.IndexOf(".exe"));

				Process currentProcess = Process.GetCurrentProcess();

				//���������� ��� �������� � ������� ������
				foreach(Process process in Process.GetProcessesByName(processName))
				{
					//������� ��������� ��� �� ����������
					if (process.Id == currentProcess.Id)
						continue;

					//����� ���� ������ ���������� � ���������� ������
					//������������ �����. ��������� ���-�� ��� ��� '���' ����
					if (process.MainModule.FileName.ToLower() != currentProcess.MainModule.FileName.ToLower())
						continue;

					ActivateWindow( process.MainWindowHandle );
					return;
				}
			}
		}

		public static void ActivateWindow( IntPtr hWnd )
		{
			//������������ �������� ���� ����������
			WinApi.Windows.SetForegroundWindow( hWnd);
			//SetActiveWindow( hWnd);
//			WinApi.Windows.ShowWindow( hWnd, 5 ); // ������������� ����
		}


	}

	namespace WinApi
	{
		public class Windows
		{
			[DllImport("User32.dll")]
			public static extern int SetForegroundWindow(IntPtr hWnd);

			[DllImport("User32.dll")]
			public static extern int SetActiveWindow(IntPtr hWnd);

			[DllImport("User32.dll")]
			public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow );
		}
	}
}
