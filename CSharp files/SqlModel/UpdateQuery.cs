using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// ������������ ��������� Update ...
	/// </summary>
	internal class UpdateQuery: QueryBase
	{
		#region Constructor's

		/// <summary>
		/// �������� ��������� DELETE
		/// </summary>
		public UpdateQuery( Table tableSource, Where @where)
		{
			TableSource = tableSource;
			TableToUpdate = tableSource;

			Where = @where;
		}

		#endregion

		public readonly List<UpdateField> Fields = new List<UpdateField>();

		public Table TableToUpdate { get; set; }

		public bool IsSupportFromKeywordInUpdate { get; set; }

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder("UPDATE ");
			sb.Append( IsSupportFromKeywordInUpdate ? TableToUpdate : TableSource );

			sb.Append( Environment.NewLine )
				.Append( "SET " + CreateUpdateList(Fields) );

			if( IsSupportFromKeywordInUpdate && TableSource.GetTableCount() > 1)
				sb.Append(Environment.NewLine + "FROM " + TableSource);

			if(Where != null && Where.IsNotEmpty)
				sb.Append( Environment.NewLine + " WHERE " + Where );

			return sb.ToString();
		}

		private string CreateUpdateList(List<UpdateField> fields)
		{
			return fields
				.Select( fld => fld.ToString() )
				.Aggregate( (acc, str) => acc + ", "+ str  )
				;
		}
	}
}