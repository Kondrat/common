using System;

namespace SSD_Viewer.Core.Db.SqlModel
{
	internal class QueryBase
	{
		/// <summary>
		/// ��������� �������� (�������, �����������, ���������), �� �������� ������������ �������
		/// </summary>
		public ITableSource TableSource { get; set;}

		/// <summary>
		/// ������� WHERE
		/// </summary>
		public Where Where{ get; set;}

		/// <summary>
		/// ���������� ������� � ����������� ������
		/// </summary>
		/// <param name="leftKey">���� ����� �������</param>
		/// <param name="rightTable">������ �������</param>
		/// <param name="rightKeyName">���� ������ ������� (��� ����)</param>
		public void AddTableToJoin( TableField leftKey, Table rightTable, string rightKeyName)
		{
			AddTableToJoin( leftKey, rightTable, new TableField( rightTable, rightKeyName ));
		}

		/// <summary>
		/// ���������� ������� � ����������� ������
		/// </summary>
		/// <param name="leftKey">���� ����� �������</param>
		/// <param name="rightTable">������ �������</param>
		/// <param name="rightKey">���� ������ �������</param>
		public void AddTableToJoin(TableField leftKey, Table rightTable, TableField rightKey)
		{
			// ���������, ��������� �� ��� ����������� � ���������� ������� � �����������.
			if(TableSource.IsUse(rightTable.Name))
			{
				// �� ����� �������� ������������ �������
				return;
			}

			IJoinSource oldSource = TableSource as IJoinSource;
			if(oldSource == null)
				throw new InvalidOperationException( "��� ����������� ����� ����������� ������ ��� ������� ��� ������ �����������, �� �� ���������." );
			else
			{
				IJoinSource tableJoin = TableJoin.TryCreate(oldSource, leftKey, rightTable, rightKey);
				TableSource = tableJoin;
			}
		}
	}
}