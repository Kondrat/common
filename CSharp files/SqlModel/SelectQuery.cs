﻿using System;
using System.Diagnostics;
using System.Text;

namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// Представляет выражение SELECT ... FROM ... WHERE ... и т.д.
	/// </summary>
	internal class SelectQuery :QueryBase //, ITableSource
	{
		#region Constructor's

		/// <summary>
		/// Создание выражения SELECT
		/// </summary>
		/// <param name="table">Табличный источник, из которого будет производится выборка</param>
		public SelectQuery(ITableSource table)
			:this(null, table)
		{
			// Empty
		}

		/// <summary>
		/// Создание выражения SELECT
		/// </summary>
		public SelectQuery(SelectPredicate selectPredicate, SelectList selectList, ITableSource tableSource)
		{
			Debug.Assert(selectPredicate != null, "selectPredicate != null");
			SelectPredicate = selectPredicate;

			SelectList = selectList;
			TableSource = tableSource;

			GroupBy = new GroupBy();
			OrderBy = new OrderByList();
			Where = new Where();
		}

		/// <summary>
		/// Создание выражения SELECT
		/// </summary>
		public SelectQuery(SelectList selectList, ITableSource tableSource) 
			:this( SelectPredicate.CreateAllPredicate(), selectList, tableSource)
		{
			// Empty
		}

		#endregion

		/// <summary>
		/// Предикаты ALL, DISTINCT, TOP и др.
		/// </summary>
		public SelectPredicate SelectPredicate { get; set;}

		/// <summary>
		/// Список полей, попадающих в результат выборки
		/// </summary>
		public SelectList SelectList { get; set;}

		public GroupBy GroupBy {get; set; }

		/// <summary>
		/// Условие GROUP BY
		/// </summary>
		public Having Having {get; set; }

		/// <summary>
		/// Условие ORDER BY
		/// </summary>
		public OrderByList OrderBy {get; set; }

		public override string ToString()
		{
			return CreateSqlText(false);
		}

		private string CreateSqlText(bool isSubSelect)
		{
			StringBuilder sb = new StringBuilder("SELECT ");
			sb.Append( (SelectPredicate != null && SelectPredicate.ToString().Length > 0)? SelectPredicate + " ": string.Empty);
			sb.Append( Environment.NewLine + " " + SelectList.ToString() );

			SelectQuery subSelect = TableSource as SelectQuery;
			if(subSelect != null)
				sb.Append(Environment.NewLine + " FROM (" + subSelect.CreateSqlText(true) + ")");
			else
				sb.Append( Environment.NewLine + " FROM " + TableSource );

			if(Where != null && Where.IsNotEmpty)
				sb.Append( Environment.NewLine + " WHERE " + Where );

			if(GroupBy != null && GroupBy.IsNotEmpty)
			{
				sb.Append(Environment.NewLine + " GROUP BY " + GroupBy);

				if(Having != null && Having.IsNotEmpty)
					sb.Append( Environment.NewLine + " HAVING " + Having );
			}

			if( !isSubSelect && OrderBy != null && OrderBy.IsNotEmpty)
				sb.Append(Environment.NewLine + " ORDER BY " + OrderBy);

			return sb.ToString();
		}
	}
}
