namespace SSD_Viewer.Core.Db.SqlModel
{
	/*
	 * HAVING ������������ ��� ����������� ������� �� ����������� ��������� ������.
	 */

	/// <summary>
	/// ������� HAVING
	/// </summary>
	internal class Having
	{
		private readonly string expression;

		/// <summary>
		/// �������� �������
		/// </summary>
		/// <param name="expression">����������� ��������� ��� �������</param>
		public Having(string expression)
		{
			this.expression = expression;
		}

		public bool IsNotEmpty
		{
			get
			{
				return !string.IsNullOrEmpty( expression );
			}
		}

		public override string ToString()
		{
			return expression;
		}
	}
}