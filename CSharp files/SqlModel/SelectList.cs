using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SSD_Viewer.Core.Db.SqlModel
{
	#region Select List

	/// <summary>
	/// ������ �����, ���������� � ��������� �������
	/// </summary>
	internal class SelectList
	{
		readonly List<Field> fields = new List<Field>();

		/// <summary>
		/// ���������� ���� � ������
		/// </summary>
		/// <param name="field"></param>
		public void AddField(Field field)
		{
			bReturnAllColumns = false;
			fields.Add( field );
		}

		public override string ToString()
		{
			if(bReturnAllColumns)
				return "*";
			else
			{
				var textParts = fields.Select( fld => fld.ToString() ).ToArray();
				return string.Join( ", ", textParts );
			}
		}

		#region IsReturnAllColumns

		private bool bReturnAllColumns = true;

		/// <summary>
		/// TRUE, ���� ����� ������� ��� ���� ( select * from ... )
		/// </summary>
		public bool IsReturnAllColumns
		{
			get { return bReturnAllColumns; }
		}

		#endregion
	}

	#endregion

	#region Field's

	#region Field

	/// <summary>
	/// ���� SQL-�������
	/// </summary>
	internal abstract class Field
	{
		/// <summary>
		/// ���������� ������ �� ���� ��� ������������� � ����������. ��� ���������� ���� ������� ����������� ����� ��� ��� alias, ���� - ������ ����� ���, ��������� alias. ��� ������������ ���� ���������� ������, ���������� ���������.
		/// </summary>
		public abstract string GetFieldRef();

		/// <summary>
		/// ��������� ����
		/// </summary>
		public string Alias
		{
			get; protected set;
		}
	}

	#endregion

	#region Calculated Field

	/// <summary>
	/// ������������ ����������� ����
	/// </summary>
	internal class CalculatedField :Field
	{
		#region Constructor's

		/// <summary>
		/// �������� ������������ ����
		/// </summary>
		/// <param name="expr">���������, �� �������� ����������� ����</param>
		public CalculatedField(string expr)
		{
			expression = expr;
		}

		public CalculatedField(string expr, string alias)
		{
			expression = expr;
			Alias = alias;
		}

		#endregion

		/// <summary>
		/// ������������� ���������, �� �������� ����������� ����
		/// </summary>
		/// <param name="expr">���������, �� �������� ����������� ����</param>
		public void SetExpression(string expr)
		{
			expression = expr;
		}

		private string expression = string.Empty;

		public override string GetFieldRef()
		{
			return expression;
		}

		public override string ToString()
		{
			return string.IsNullOrEmpty( Alias)? expression: expression + " as " + Alias;
		}
	}

	#endregion

	#region Table Field

	/// <summary>
	/// ���� �������
	/// </summary>
	internal class TableField :Field
	{
		public TableField(Table table, string name)
		{
			Debug.Assert(table != null, "table != null");
			Table = table;

			Debug.Assert(name != null, "name != null");
			Name = name;
		}

		public TableField(Table table, string name, string alias)
		{
			Debug.Assert(table != null, "table != null");
			Table = table;

			Debug.Assert(name != null, "name != null");
			Name = name;

			Debug.Assert(alias != null, "alias != null");
			Alias = alias;
		}

		public override string ToString()
		{
			return
				Table.GetTableAliasOrName() + 
				"." +
				(string.IsNullOrEmpty(Alias) ? Helper.Quote(Name) : Helper.Quote(Name) + " AS " + Helper.Quote( Alias));
		}

		public override string GetFieldRef()
		{
			return Table.GetTableAliasOrName() + "." + Helper.Quote( Name);
		}

		public Table Table
		{
			get; private set;
		}

		public string Name
		{
			get; private set;
		}
	}

	#endregion

	#endregion
}