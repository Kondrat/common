﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SSD_Viewer.Core.Db.SqlModel
{

	#region Direction

	/// <summary>
	/// Направление сортировки
	/// </summary>
	internal class Direction
	{
		private Direction(string direction)
		{
			Debug.Assert(direction != null, "direction != null");
			this.direction = direction;
		}

		// Направление сортировки
		private readonly string direction;

		public override string ToString()
		{
			return direction;
		}

		/// <summary>
		/// Сортировка по возрастанию
		/// </summary>
		public static readonly Direction Asc = new Direction( "ASC" );

		/// <summary>
		/// Сортировка по убыванию
		/// </summary>
		public static readonly Direction Desc = new Direction( "DESC" );
	}

	#endregion

	#region OrderBy

	/// <summary>
	/// Условие сортировки
	/// </summary>
	internal class OrderBy
	{
		/// <summary>
		/// Создание условия. Направление по умолчанию - по возрастанию.
		/// </summary>
		/// <param name="field">Поле, по которому сортируется результат</param>
		public OrderBy(Field field)
			:this( field, Direction.Asc)
		{
			// Empty
		}

		/// <summary>
		/// Создание условия.
		/// </summary>
		/// <param name="field">Поле, по которому сортируется результат</param>
		/// <param name="direction">Направление сортировки</param>
		public OrderBy(Field field, Direction direction)
		{
			Debug.Assert(field != null, "field != null");
			Field = field;

			Debug.Assert(direction != null, "direction != null");
			Direction = direction;
		}

		public readonly Field Field;
		public readonly Direction Direction;
	}

	#endregion

	#region OrderByList

	/// <summary>
	/// Список условий сортировки
	/// </summary>
	internal class OrderByList
	{
		readonly List<OrderBy> items = new List<OrderBy>();

		/// <summary>
		/// Добавление условия сортировки
		/// </summary>
		/// <param name="orderBy">Условие сортировки</param>
		public void Add( OrderBy orderBy)
		{
			Debug.Assert(orderBy != null, "orderBy != null");
			items.Add( orderBy );
		}

		/// <summary>
		/// Добавление условия сортировки
		/// </summary>
		/// <param name="field">Поле, по которому сортируется результат</param>
		/// <param name="direction">Направление сортировки</param>
		public void Add( Field field, Direction direction)
		{
			Add( new OrderBy( field, direction ));
		}

		/// <summary>
		/// Добавление условия сортировки. Направление по умолчанию - по возрастанию.
		/// </summary>
		/// <param name="field">Поле, по которому сортируется результат</param>
		public void Add( Field field)
		{
			Add( new OrderBy( field ) );
		}

		/// <summary>
		/// Вставка условия сортировки в начало списка 
		/// </summary>
		/// <param name="orderBy">Условие сортировки</param>
		public void InsertAtStart(OrderBy orderBy)
		{
			Debug.Assert(orderBy != null, "orderBy != null");
			items.Insert( 0, orderBy);
		}

		/// <summary>
		/// Вставка условия сортировки в начало списка 
		/// </summary>
		/// <param name="field">Поле, по которому сортируется результат</param>
		/// <param name="direction">Направление сортировки</param>
		public void InsertAtStart( Field field, Direction direction)
		{
			InsertAtStart( new OrderBy( field, direction ));
		}


		public bool IsNotEmpty
		{
			get
			{
				return items.Count > 0;
			}
		}

		public override string ToString()
		{
			var parts = items.Select( ob => ob.Field.GetFieldRef() + " " + ob.Direction.ToString() ).ToArray();
			return string.Join( ", ", parts );
		}
	}

	#endregion
}
