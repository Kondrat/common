﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSD_Viewer.Core.Db.SqlModel
{
	internal class UpdateField
	{
		public UpdateField(TableField field, string value)
		{
			Field = field;
			Value = value;
		}

		public TableField Field { get; private set; }

		public string Value { get; private set; }

		public override string ToString()
		{
			return Field + " = " + Value;
		}
	}
}
