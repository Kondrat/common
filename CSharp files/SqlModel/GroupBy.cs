﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// Условия сортировки в запросе
	/// </summary>
	internal class GroupBy
	{
		readonly List<Field> fields = new List<Field>();

		public void AddField(Field field)
		{
			Debug.Assert(field != null, "field != null");
			fields.Add( field );
		}

		/// <summary>
		/// true, если присутствуют условия сортировки
		/// </summary>
		public bool IsNotEmpty
		{
			get
			{
				return fields.Count > 0;
			}
		}

		/// <summary>
		/// Доступ к условиям сортировки по индексу
		/// </summary>
		/// <param name="idx">Индекс</param>
		/// <returns>Условие сортировки</returns>
		public Field this[int idx]
		{
			get
			{
				return this.fields[idx];
			}
		}

		public override string ToString()
		{
			var parts = fields.Select( fld => fld.ToString() ).ToArray();
			return string.Join( ", ", parts );
		}
	}
}
