﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// Вспомогательный класс
	/// </summary>
	static class Helper
	{
		/// <summary>
		/// Окружение строкового токена квадратными скобками
		/// </summary>
		/// <param name="token">Строка, которую нужно окружить скобками</param>
		/// <returns>Результат обработки</returns>
		public static string Quote(string token)
		{
			return string.Format("[{0}]", token);
		}
	}
}
