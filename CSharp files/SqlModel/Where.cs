﻿namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// Условие фильтрации WHERE
	/// </summary>
	internal class Where
	{
		private string expression;

		public Where()
		{
			expression = string.Empty;
		}

		public bool IsNotEmpty
		{
			get
			{
				return !string.IsNullOrEmpty( expression );
			}
		}

		/// <summary>
		/// Добавление условия в выражение WHERE
		/// </summary>
		/// <param name="expr"></param>
		public void AddWhere(string expr)
		{
		    expr = "(" + expr + ")";

			if(string.IsNullOrEmpty( expression ))
				expression = string.Format( "{0}", expr );
			else
				expression = string.Format( "{0} AND {1}", expression, expr );
		}

		public override string ToString()
		{
			return expression;
		}
	}
}
