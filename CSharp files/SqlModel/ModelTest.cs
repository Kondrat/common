﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace SSD_Viewer.Core.Db.SqlModel
{
	[TestFixture]
	public class ModelTest
	{
//        [Test]
//        public void SelectSubSelectTest()
//        {
//            Assert.Ignore();

//            Table table1 = new Table( "table1", "tbl1" );
//            TableField fld11 = new TableField( table1, "field11" );
//            TableField fld12 = new TableField( table1, "field12" );

//            QueryToBuild subSelect = new QueryToBuild(table1);
//            subSelect.SelectList.AddField(fld11);
//            subSelect.SelectList.AddField(fld12);
//            subSelect.OrderBy.Add( fld11 );
//            subSelect.Where.AddWhere( "field12 > 10" );

//            TableField fld21 = new TableField(table1, "field21");
//            TableField fld22 = new TableField(table1, "field22");

//            QueryToBuild select = new QueryToBuild(subSelect);
//            select.SelectList.AddField( fld21 );
//            select.SelectList.AddField( fld22 );
//            select.OrderBy.Add( fld21 );
//            select.Where.AddWhere("field21 > 100");

//            //TableJoin tableJoin1 = null;
//            //TableJoin tableJoin = new TableJoin( tableJoin1, new TableField( table1, "id" ), table1, "id" );

//            Console.WriteLine(select);
//            Assert.AreEqual(@"SELECT 
// tbl1.field21, tbl1.field22
// FROM (SELECT 
// tbl1.field11, tbl1.field12
// FROM table1 AS tbl1
// WHERE (field12 > 10))
// WHERE (field21 > 100)
// ORDER BY tbl1.field21 ASC", select.ToString());
//        }

		[Test]
		public void SelectOrderByTest()
		{
			SelectList selectList = new SelectList();

			Table table1 = new Table("table1");
			Table table2 = new Table("table2", "tbl2");

			var field1 = new TableField(table1, "column1");
			var field2 = new TableField(table2, "column2", "col2");

			selectList.AddField(field1);
			selectList.AddField(field2);

			var tableJoin = TableJoin.TryCreate(table1, "id", table2, "id");
			SelectQuery query = new SelectQuery(selectList, tableJoin);
			query.OrderBy.Add( field1 );
			query.OrderBy.Add( field2, Direction.Desc );

			Console.WriteLine(query);
			Assert.AreEqual(@"SELECT 
 [table1].[column1], [tbl2].[column2] AS [col2]
 FROM [table1] INNER JOIN [table2] AS [tbl2] ON [table1].[id] = [tbl2].[id]
 ORDER BY [table1].[column1] ASC, [tbl2].[column2] DESC", query.ToString());
		}


		[Test]
		public void OrderByTest()
		{
			Table table1 = new Table( "table1" );
			Table table2 = new Table( "table2", "tbl2" );

			OrderByList orderBy = new OrderByList();
			orderBy.Add( new TableField( table1, "column1" ) );
			orderBy.Add(new TableField(table2, "column2"), Direction.Desc);
			orderBy.Add(new TableField(table2, "column3", "col3"), Direction.Asc);

			Console.WriteLine( orderBy);
			Assert.AreEqual("[table1].[column1] ASC, [tbl2].[column2] DESC, [tbl2].[column3] ASC", orderBy.ToString());
		}

		[Test]
		public void GroupByTest()
		{
			Table table1 = new Table( "table1" );
			Table table2 = new Table( "table2", "tbl2" );

			GroupBy groupBy = new GroupBy();
			groupBy.AddField( new TableField( table1, "column1" ) );
			groupBy.AddField( new TableField( table2, "column2" ) );
			groupBy.AddField( new TableField( table2, "column3", "col3" ) );

			Console.WriteLine( groupBy);
			Assert.AreEqual("[table1].[column1], [tbl2].[column2], [tbl2].[column3] AS [col3]", groupBy.ToString());
		}

		[Test]
		public void SelectTest()
		{
			SelectList selectList = new SelectList();

			Table table = new Table( "table1" );

			selectList.AddField( new TableField( table, "column1") );
			selectList.AddField( new TableField( table, "column2") );

			SelectQuery query = new SelectQuery( SelectPredicate.CreateTopPercentPredicate( 10 ), selectList, table );
			Console.WriteLine( query);
			Assert.AreEqual(@"SELECT TOP 10 PERCENT 
 [table1].[column1], [table1].[column2]
 FROM [table1]", query.ToString());
		}

		[Test]
		public void SelectAliasedTest()
		{
			SelectList selectList = new SelectList();

			Table table = new Table( "table1", "tbl1" );

			selectList.AddField( new TableField( table, "column1", "col1") );
			selectList.AddField( new TableField( table, "column2", "col2") );

			SelectQuery query = new SelectQuery(selectList, table);
			Console.WriteLine(query);
			Assert.AreEqual(@"SELECT 
 [tbl1].[column1] AS [col1], [tbl1].[column2] AS [col2]
 FROM [table1] AS [tbl1]", query.ToString());
		}

		[Test]
		public void SelectJoinedTest()
		{
			SelectList selectList = new SelectList();

			Table table1 = new Table( "table1", "tbl1");
			var fldId = new TableField( table1, "id", "id_alias");
			Table table2 = new Table( "table2", "");
			IJoinSource tableJoin = TableJoin.TryCreate(table1, fldId, 
			                                      table2, new TableField( table2, "id" ));

			selectList.AddField( fldId );
			selectList.AddField( new TableField( table1, "column1", "col1") );
			selectList.AddField( new TableField( table2, "column2", "") );

			SelectQuery query = new SelectQuery(selectList, tableJoin);
			Console.WriteLine(query);
			Assert.AreEqual(@"SELECT 
 [tbl1].[id] AS [id_alias], [tbl1].[column1] AS [col1], [table2].[column2]
 FROM [table1] AS [tbl1] INNER JOIN [table2] ON [tbl1].[id] = [table2].[id]", query.ToString());
		}

		[Test]
		public void SelectJoinedTest2()
		{
			SelectList selectList = new SelectList();

			Table table1 = new Table( "table1", "tbl1");
			var fldId = new TableField( table1, "id", "id_alias");
			Table table2 = new Table( "table2", "");

			selectList.AddField( fldId );
			selectList.AddField( new TableField( table1, "column1", "col1") );
			selectList.AddField( new TableField( table2, "column2", "") );

			SelectQuery query = new SelectQuery(selectList, table1);
			query.AddTableToJoin( fldId, table2, "id" );

			Console.WriteLine(query);
			Assert.AreEqual(@"SELECT 
 [tbl1].[id] AS [id_alias], [tbl1].[column1] AS [col1], [table2].[column2]
 FROM [table1] AS [tbl1] INNER JOIN [table2] ON [tbl1].[id] = [table2].[id]", query.ToString());
		}

		[Test]
		public void SelectJoinedTwiceTest()
		{
			SelectList selectList = new SelectList();

			Table table1 = new Table( "table1", "tbl1");
			var fldId = new TableField( table1, "id", "id_alias");
			Table table2 = new Table( "table2", "");
			Table table3 = new Table( "table3", "");
			IJoinSource tableJoin1 = TableJoin.TryCreate(table1, fldId, 
			                                       table2, new TableField( table2, "id" ));

			IJoinSource tableJoin2 = TableJoin.TryCreate(tableJoin1, fldId,
			                                       table3, new TableField(table3, "id"));

			selectList.AddField( fldId );
			selectList.AddField( new TableField( table1, "column1", "col1") );
			selectList.AddField( new TableField( table2, "column2", "") );
			selectList.AddField( new TableField( table3, "column3", "") );

			SelectQuery query = new SelectQuery(selectList, tableJoin2);
			Console.WriteLine(query);
			Assert.AreEqual(@"SELECT 
 [tbl1].[id] AS [id_alias], [tbl1].[column1] AS [col1], [table2].[column2], [table3].[column3]
 FROM ([table1] AS [tbl1] INNER JOIN [table2] ON [tbl1].[id] = [table2].[id]) INNER JOIN [table3] ON [tbl1].[id] = [table3].[id]", query.ToString());
		}
	}
}
