using System;
using System.Diagnostics;

namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// ��������� ���������. �������� �� ��������, ������� ����� ������� ���������� ��������� ������.
	/// </summary>
	internal interface ITableSource
	{
		/// <summary>
		/// ���������, ������������ �� ��������� ������� � ��������� ������. ��� Table - true, ���� ��� ������� ��������� � ���������, ��� TableJoin true, ���� ��������� ������� ��������� � �����������.
		/// </summary>
		/// <param name="tableName">��� ����������� �������</param>
		bool IsUse(string tableName);

		/// <summary>
		/// ���������� ���-�� ������, ������������ � ��������� ���������
		/// </summary>
		/// <returns></returns>
		int GetTableCount();
	}

	/// <summary>
	/// ��������� ���������. �������� �� ��������, ������� ����� ���� ������������ ��� ������������ ����������� (JOIN).
	/// </summary>
	internal interface IJoinSource :ITableSource
	{
		// Empty
	}

	/// <summary>
	/// ������� � ��
	/// </summary>
	internal class Table :IJoinSource
	{
		/// <summary>
		/// �������� ������� � ��������� � �����
		/// </summary>
		/// <param name="name">��� �������</param>
		public Table(string name)
		{
			Debug.Assert(name != null, "name != null");
			this.Name = name;
		}

		/// <summary>
		/// �������� ������� � ��������� � ����� � ����������
		/// </summary>
		/// <param name="name">��� �������</param>
		/// <param name="alias">��������� �������</param>
		public Table(string name, string alias)
			: this(name)
		{
			this.Alias = alias;
		}

		/// <summary>
		/// ��� �������
		/// </summary>
		public string Name
		{
			get; private set;
		}

		/// <summary>
		/// ��������� �������
		/// </summary>
		public string Alias
		{
			get; private set;
		}

		public override string ToString()
		{
			return string.IsNullOrEmpty(Alias) ? Helper.Quote( Name) : Helper.Quote( Name) + " AS " + Helper.Quote( Alias);
		}

		public bool IsUse(string tableName)
		{
			return tableName == this.Name;
		}

		public int GetTableCount()
		{
			return 1;
		}

		public string GetTableAliasOrName()
		{
			return string.IsNullOrEmpty( this.Alias )? Helper.Quote( this.Name): Helper.Quote( this.Alias);
		}
	}

	/// <summary>
	/// ��� ����������� ������ (LEFT JOIN, RIGHT JOIN � ��.)
	/// </summary>
	internal class JoinType
	{
		internal JoinType(string sqlText)
		{
			SqlText = sqlText;
		}

		public string SqlText
		{
			get; private set;
		}

		public override string ToString()
		{
			return SqlText;
		}

		#region Static fields

		public static readonly JoinType InnerJoin = new JoinType( "INNER JOIN" );
		public static readonly JoinType LeftJoin = new JoinType( "LEFT JOIN" );
		public static readonly JoinType RightJoin = new JoinType( "RIGHT JOIN" );

		#endregion
	}

	/// <summary>
	/// �������� ��������� ������ ��� ����������� ������
	/// </summary>
	internal class JoinOperator
	{
		internal JoinOperator(string sqlText)
		{
			SqlText = sqlText;
		}

		/// <summary>
		/// �������� ��������� � �������� SQL
		/// </summary>
		public string SqlText
		{
			get;
			private set;
		}

		public override string ToString()
		{
			return SqlText;
		}

		#region Static fields

		public static readonly JoinType Equal = new JoinType("=");
		public static readonly JoinType NotEqual = new JoinType("<>");
		public static readonly JoinType Great = new JoinType(">");
		public static readonly JoinType GreatEqual = new JoinType(">=");
		public static readonly JoinType Less = new JoinType("<");
		public static readonly JoinType LessEqual = new JoinType("<=");

		#endregion
	}

	/// <summary>
	/// ������������ ��������� JOIN
	/// </summary>
	internal class TableJoin :IJoinSource
	{
		#region Creator's

		public static IJoinSource TryCreate(JoinType joinType, JoinType joinOperator, IJoinSource left, TableField leftKey, Table right, string rightKeyName)
		{
			return TryCreate(joinType, joinOperator, left, leftKey, right, new TableField(right, rightKeyName));
		}

		public static IJoinSource TryCreate(IJoinSource left, TableField leftKey, Table right, TableField rightKey)
		{
			return TryCreate( JoinType.InnerJoin, SqlModel.JoinOperator.Equal, left, leftKey, right, rightKey );
		}

		public static IJoinSource TryCreate(IJoinSource left, TableField leftKey, Table right, string rightKeyName)
		{
			return TryCreate(JoinType.InnerJoin, SqlModel.JoinOperator.Equal, left, leftKey, right, new TableField(right, rightKeyName));
		}

		public static IJoinSource TryCreate(JoinType joinType, IJoinSource left, TableField leftKey, Table right, TableField rightKey)
		{
			return TryCreate( joinType, SqlModel.JoinOperator.Equal, left, leftKey, right, rightKey );
		}

		public static IJoinSource TryCreate(JoinType joinType, Table left, string leftKeyName, Table right, string rightKeyName)
		{
			return TryCreate(joinType, SqlModel.JoinOperator.Equal, left, new TableField(left, leftKeyName), right, new TableField(right, rightKeyName));
		}

		public static IJoinSource TryCreate(Table left, string leftKeyName, Table right, string rightKeyName)
		{
			return TryCreate( JoinType.InnerJoin, SqlModel.JoinOperator.Equal, left, new TableField( left, leftKeyName ), right, new TableField( right, rightKeyName ) );
		}

		public static IJoinSource TryCreate(JoinType joinType, JoinType joinOperator, IJoinSource left, TableField leftKey, Table right, TableField rightKey)
		{
			if(left.IsUse(right.Name))
				//throw new ArgumentException("���������� �������� ������� � ����������� ������.");
				return left;
			else
				return new TableJoin( joinType, joinOperator, left, leftKey, right, rightKey );
		}

		#endregion


		private TableJoin(JoinType joinType, JoinType joinOperator, IJoinSource left, TableField leftKey, Table right, TableField rightKey)
		{
			if(left.IsUse(right.Name))
				throw new ArgumentException("���������� �������� ������� � ����������� ������.");

			JoinType = joinType;
			JoinOperator = joinOperator;
			Left = left;
			LeftKey = leftKey;
			Right = right;
			RightKey = rightKey;
		}

		/// <summary>
		/// ��� �����������
		/// </summary>
		public JoinType JoinType{ get; private set;}
		/// <summary>
		/// �������� ��������� ������
		/// </summary>
		public JoinType JoinOperator{ get; private set;}

		/// <summary>
		/// ��������� �������� �����
		/// </summary>
		public IJoinSource Left{ get; private set;}
		/// <summary>
		/// ����� ����
		/// </summary>
		public TableField LeftKey{ get; private set;}

		/// <summary>
		/// ������� ������
		/// </summary>
		public Table Right{ get; private set;}
		/// <summary>
		/// ������ ����
		/// </summary>
		public TableField RightKey{ get; private set;}

		public override string ToString()
		{
			string leftJoinSource = Left.ToString();
			if(Left is TableJoin)
				leftJoinSource = "(" + leftJoinSource + ")";

			return string.Format( 
				"{0} {1} {2} ON {3} {4} {5}",
				leftJoinSource,
				JoinType,
				Right,
				LeftKey.Table.GetTableAliasOrName() + "." + Helper.Quote( LeftKey.Name),
				JoinOperator,
				RightKey.Table.GetTableAliasOrName() + "." + Helper.Quote( RightKey.Name)
				);
		}

		public bool IsUse(string tableName)
		{
			return this.Left.IsUse( tableName ) || this.Right.IsUse( tableName );
		}

		public int GetTableCount()
		{
			return this.Left.GetTableCount() + this.Right.GetTableCount();
		}
	}
}