using System.Diagnostics;

namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// ��������� ALL, DISTINCT, TOP � �.�.
	/// </summary>
	internal class SelectPredicate
	{
		private readonly string sqlText;

		private SelectPredicate(string sqlText)
		{
			Debug.Assert(sqlText != null, "sqlText != null");
			this.sqlText = sqlText;
		}

		public override string ToString()
		{
			return sqlText;
		}

		#region Factory

		public static SelectPredicate CreateAllPredicate()
		{
			return new SelectPredicate(string.Empty); // ALL - by default
		}

		public static SelectPredicate CreateDistinctPredicate()
		{
			return new SelectPredicate( "DISTINCT" );
		}

		public static SelectPredicate CreateDistinctRowPredicate()
		{
			return new SelectPredicate("DISTINCTROW");
		}

		public static SelectPredicate CreateTopPredicate(int count)
		{
			return new SelectPredicate("TOP " + count);
		}

		public static SelectPredicate CreateTopPercentPredicate(int count)
		{
			return new SelectPredicate("TOP " + count + " PERCENT");
		}

		#endregion
	}
}