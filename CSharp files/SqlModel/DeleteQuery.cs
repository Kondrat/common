using System;
using System.Text;
using SSD_Viewer.Core.Db.SqlModel;

namespace SSD_Viewer.Core.Db.SqlModel
{
	/// <summary>
	/// ������������ ��������� DELETE FROM ... WHERE ...
	/// </summary>
	internal class DeleteQuery: QueryBase
	{
		#region Constructor's

		/// <summary>
		/// �������� ��������� DELETE
		/// </summary>
		public DeleteQuery( ITableSource tableSource, Where @where)
		{
			TableSource = tableSource;

			Where = @where;
		}

		#endregion

		public string TableToDeleteRows { get; set; }

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder("DELETE " + TableToDeleteRows);
			sb.Append( Environment.NewLine )
				.Append( "FROM " + TableSource );

			if(Where != null && Where.IsNotEmpty)
				sb.Append( Environment.NewLine + " WHERE " + Where );

			return sb.ToString();
		}
	}
}