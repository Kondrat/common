﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Csv
{
	public class CsvReader
	{
		private readonly CsvDefines csvDefines;
		private readonly TextReader rdr = null;

		bool bIsFirstLine = true;

		public CsvReader(TextReader reader, CsvDefines csvDefines)
		{
			this.csvDefines = csvDefines;
			rdr = reader;
			bIsFirstLine = true;
		}


		public IEnumerable<string[]> Iterator
		{
			get
			{
				string[] line = null;
				while((line = ReadLine()) != null)
					yield return line;
			}
		}

		#region Read CSV

		private string[] ReadLine()
		{
			StringBuilder sbField = new StringBuilder();
			List<string> line = new List<string>();

			bool bIsVerbatimMode = false;
			bool bIsQuoteInVerbatimModeBeReaded = false;
			bool bIsLineSeparatorReaded = false;

			while(true)
			{
				int iSymb = rdr.Read();
				if(iSymb == -1)
				{
					if (bIsVerbatimMode && !bIsQuoteInVerbatimModeBeReaded)
						throw new ArgumentException("Неожиданный конец файла.");

					if(bIsFirstLine && line.Count == 0 && sbField.Length == 0)
					{
						bIsFirstLine = false;
						return new[] {string.Empty};
					}

					if (line.Count > 0 || sbField.Length > 0)
					{
						AppendField( line, ref sbField );
						bIsFirstLine = false;
						return line.ToArray();
					}

					return null;
				}

				char symb = (char)iSymb;

				if(bIsVerbatimMode &&
					symb != csvDefines.Quote &&
					!bIsQuoteInVerbatimModeBeReaded
					)
				{
					sbField.Append(symb);
				}
				else
				{
					switch(symb)
					{
						case (char)0x0A:
						{
							if(!bIsLineSeparatorReaded)
								bIsVerbatimMode = FinalizeLineReading(bIsVerbatimMode, bIsQuoteInVerbatimModeBeReaded, line, ref sbField);

							bIsLineSeparatorReaded = true;
							bIsFirstLine = false;
							return line.ToArray();
						}

						case (char)0x0D:
						{
							bIsVerbatimMode = FinalizeLineReading( bIsVerbatimMode, bIsQuoteInVerbatimModeBeReaded, line, ref sbField );
							bIsLineSeparatorReaded = true;
						}
							break;

						default:
							if(symb == csvDefines.FieldSeparator)
							{
								if(bIsQuoteInVerbatimModeBeReaded)
								{
									bIsVerbatimMode = false;
									bIsQuoteInVerbatimModeBeReaded = false;
								}

								AppendField(line, ref sbField);
							}
							else
							if(symb == csvDefines.Quote)
							{
								if(!bIsVerbatimMode)
									bIsVerbatimMode = true;
								else
								{
									if(!bIsQuoteInVerbatimModeBeReaded)
										bIsQuoteInVerbatimModeBeReaded = true;
									else
									{
										sbField.Append(csvDefines.Quote);
										bIsQuoteInVerbatimModeBeReaded = false;
									}
								}
							}
							else
								sbField.Append(symb);
							break;
					}
				}
			}
		}

		private bool FinalizeLineReading(bool bIsVerbatimRead, bool bIsQuoteInVerbatimBeReaded, List<string> line, ref StringBuilder sbField)
		{
			if(bIsQuoteInVerbatimBeReaded)
				bIsVerbatimRead = false;

			AppendField(line, ref sbField);
			return bIsVerbatimRead;
		}

		private void AppendField(List<string> line, ref StringBuilder sbField)
		{
			line.Add(sbField.ToString());
			sbField = new StringBuilder();
		}

		#endregion

		#region Static

		public static CsvReader CreateExcelCompatible(TextReader reader)
		{
			return new CsvReader(reader, CsvDefines.ExcelCompatible);
		}

		#endregion
	}
}