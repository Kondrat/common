﻿using System;
using System.IO;
using System.Text;

namespace Common.Csv
{
	public class CsvWriter: IDisposable
	{
		private readonly CsvDefines csvDefines;
		private readonly TextWriter writer = null;

		private readonly char[] quotedSymbols = null;

		//public CsvWriter(string filename, Encoding encoding, CsvDefines csvDefines)
		//    :this(new FileStream( filename, FileMode.Create ), encoding, csvDefines)
		//{
		//    //this.csvDefines = csvDefines;
		//    //int quotedSymbolsLength = 2 + csvDefines.LineSeparator.Length;
		//    //quotedSymbols = new char[quotedSymbolsLength];
		//    //quotedSymbols[0] = csvDefines.FieldSeparator;
		//    //quotedSymbols[1] = csvDefines.Quote;
		//    //quotedSymbols[2] = csvDefines.LineSeparator[0];
		//    //if(csvDefines.LineSeparator.Length > 1)
		//    //    quotedSymbols[3] = csvDefines.LineSeparator[1];

		//    //writer = new StreamWriter( filename, false, encoding );
		//}

		public CsvWriter(TextWriter wrt, CsvDefines csvDefines)
		{
			this.csvDefines = csvDefines;
			int quotedSymbolsLength = 2 + csvDefines.LineSeparator.Length;
			quotedSymbols = new char[quotedSymbolsLength];
			quotedSymbols[0] = csvDefines.FieldSeparator;
			quotedSymbols[1] = csvDefines.Quote;
			quotedSymbols[2] = csvDefines.LineSeparator[0];
			if(csvDefines.LineSeparator.Length > 1)
				quotedSymbols[3] = csvDefines.LineSeparator[1];

			this.writer = wrt;
		}

		//public CsvWriter(Stream ostr, Encoding encoding, CsvDefines csvDefines)
		//{
		//    this.csvDefines = csvDefines;
		//    int quotedSymbolsLength = 2 + csvDefines.LineSeparator.Length;
		//    quotedSymbols = new char[quotedSymbolsLength];
		//    quotedSymbols[0] = csvDefines.FieldSeparator;
		//    quotedSymbols[1] = csvDefines.Quote;
		//    quotedSymbols[2] = csvDefines.LineSeparator[0];
		//    if(csvDefines.LineSeparator.Length > 1)
		//        quotedSymbols[3] = csvDefines.LineSeparator[1];

		//    this.writer = new StreamWriter( ostr, encoding );
		//}

		public void Close()
		{
			writer.Close();
		}

		public void WriteLine(params string[] parts)
		{
			//if(columnCount == cnst_UnknownColumnsCount)
			//    columnCount = parts.Length;

			StringBuilder sbLine = new StringBuilder();
			string quoteString = new string( csvDefines.Quote, 1 );
			string doubleQuoteString = new string( csvDefines.Quote, 2 );

			for(int idx = 0; idx < parts.Length; idx++ )
			{
				string part = parts[idx];

				if(idx > 0)
					sbLine.Append(csvDefines.FieldSeparator);

				if(part.IndexOfAny(quotedSymbols) != -1)
					sbLine.AppendFormat("{0}{1}{0}", csvDefines.Quote, part.Replace(quoteString, doubleQuoteString));
				else
					sbLine.Append(part.Replace(quoteString, doubleQuoteString));
			}

			writer.Write( sbLine );
			writer.Write( csvDefines.LineSeparator );
		}

		#region Dispose

		private bool bDisposed = false;
		public void Dispose()
		{
			if(! bDisposed)
			{
				Close();
				bDisposed = true;
			}
		}

		#endregion


		#region Static

		public static CsvWriter CreateExcelCompatible(TextWriter wrt)
		{
			return new CsvWriter( wrt, CsvDefines.ExcelCompatible );
		}

		#endregion
	}
}