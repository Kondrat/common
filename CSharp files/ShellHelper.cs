﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace PaymentDbf
{
	static class ShellHelper
	{
		public static Icon GetIconFromShell(string ext, bool isLargeIcon)
		{
			SHFILEINFO fileInfo = new SHFILEINFO();
			if(!isLargeIcon)
				SHGetFileInfo(ext, FileAttributes.FILE_ATTRIBUTE_NORMAL, out fileInfo,
					(uint)20, ShellFileInfoFlags.SHGFI_USEFILEATTRIBUTES | ShellFileInfoFlags.SHGFI_ICON | ShellFileInfoFlags.SHGFI_SMALLICON);
			else
				SHGetFileInfo(ext, FileAttributes.FILE_ATTRIBUTE_NORMAL, out fileInfo,
					(uint)20, ShellFileInfoFlags.SHGFI_USEFILEATTRIBUTES | ShellFileInfoFlags.SHGFI_ICON);
			if(fileInfo.hIcon == (IntPtr)0)
				return null;
			return Icon.FromHandle(fileInfo.hIcon);
		}

		private struct SHFILEINFO
		{
			public IntPtr hIcon;             // дескриптор системной иконки.
			public int iIcon;                // индекс системной иконки в коллекции системных иконок.
			public uint dwAttributes;        // атрибуты файла.
			public string szDisplayName;     // отображаемое имя файла.
			public string szTypeName;        // наименование типа файла.
		}

		private enum FileAttributes
		{
			FILE_ATTRIBUTE_DIRECTORY = 0x00000010,     // каталог.
			FILE_ATTRIBUTE_NORMAL = 0x00000080      // файл.
		}

		private enum ShellFileInfoFlags
		{
			SHGFI_ATTRIBUTES = 0x00000800,
			SHGFI_ATTR_SPECIFIED = 0x00020000,
			SHGFI_DISPLAYNAME = 0x00000200,     // возвращать отображаемое имя файла.
			SHGFI_EXETYPE = 0x00002000,
			SHGFI_ICON = 0x00000100,     // возвращать иконку.
			SHGFI_ICONLOCATION = 0x00001000,
			SHGFI_LARGEICON = 0x00000000,
			SHGFI_LINKOVERLAY = 0x00008000,
			SHGFI_OPENICON = 0x00000002,
			SHGFI_PIDL = 0x00000008,
			SHGFI_SELECTED = 0x00010000,
			SHGFI_SHELLICONSIZE = 0x00000004,
			SHGFI_SMALLICON = 0x00000001,     // возвращать маленькую иконку файла.
			SHGFI_SYSICONINDEX = 0x00004000,     // возвращать индекс в коллекции системных иконок.
			SHGFI_TYPENAME = 0x00000400,     // возвращать наименование типа файла.
			SHGFI_USEFILEATTRIBUTES = 0x00000010
		}

		[DllImport("Shell32.dll")]
		private static extern IntPtr SHGetFileInfo(string drivePath, FileAttributes attributes, out SHFILEINFO fileInfo,
						uint countBytesFileInfo, ShellFileInfoFlags flags);

	}
}
