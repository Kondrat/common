﻿using System;
using System.Reflection;
using System.Text;

[Flags]
public enum ObjectReviewMakerFlags
{
	ShowProperties = 0x01,
	ShowFields = (0x01 << 1),
	OnlyOneLevel = (0x01 << 2)
}

internal static class ObjectReviewMaker
{
	public static string GenerateDataReview(object @object, ObjectReviewMakerFlags flags)
	{
		// Сделать процедурку так, чтобы учитывалась ситуация, при которой в свойствах объекта
		//	может быть вложеный объект; его свойства нужно показать аналогичным образом.

		var sb = new StringBuilder();
		ProcessInternal( @object, string.Empty, sb, flags );

		return sb.ToString();
	}

	private static void ProcessInternal(object obj, string prevLevelNames, StringBuilder sb, ObjectReviewMakerFlags flags)
	{
		if(obj == default(object))
			return;

		if((flags & ObjectReviewMakerFlags.ShowProperties) == ObjectReviewMakerFlags.ShowProperties)
		{
			Show<PropertyInfo>( obj, prevLevelNames,
			                    type => type.GetProperties( BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public ),
			                    pi => pi.PropertyType,
			                    pi => pi.GetValue( obj, null ),
			                    flags,
			                    sb
				);
		}

		if((flags & ObjectReviewMakerFlags.ShowFields) == ObjectReviewMakerFlags.ShowFields)
		{
			Show<FieldInfo>( obj, prevLevelNames,
			                 type => type.GetFields( BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public ),
			                 fi => fi.FieldType,
			                 fi => fi.GetValue( obj ),
			                 flags,
			                 sb
				);
		}
	}

	private static void Show<TMemberInfo>(object obj, string prevLevelNames,
	                                      Func<Type, TMemberInfo[]> membersAccessor,
	                                      Func<TMemberInfo, Type> memberTypeAccessor,
	                                      Func<TMemberInfo, object> memberValueAccessor,
	                                      ObjectReviewMakerFlags flags, StringBuilder sb)
		where TMemberInfo: MemberInfo
	{
		TMemberInfo[] members = membersAccessor( obj.GetType() );

		foreach(var mi in members)
		{
			string currentPropFullName =
				string.IsNullOrEmpty( prevLevelNames )? mi.Name: prevLevelNames + "." + mi.Name;

			object value = memberValueAccessor( mi );

			if(memberTypeAccessor( mi ).IsArray && value != null)
			{
				WriteArray( (Array)value, currentPropFullName, sb );
			}
			else if(value != null && IsDataObject( memberTypeAccessor( mi ) ))
			{
				WriteNestedDataObject( value, currentPropFullName, flags, sb );
			}
			else
				WriteScalarValue( value, currentPropFullName, sb );
		}
	}

	private static void WriteNestedDataObject(object value, string currentPropFullName,
	                                          ObjectReviewMakerFlags flags, StringBuilder sb)
	{
		if((flags & ObjectReviewMakerFlags.OnlyOneLevel) != ObjectReviewMakerFlags.OnlyOneLevel)
			ProcessInternal( value, currentPropFullName, sb, flags );
	}

	private static void WriteArray(Array arr, string currentPropFullName, StringBuilder sb)
	{
		sb.AppendFormat( "{0}: [", currentPropFullName );
		sb.Append( Environment.NewLine );
		var alignSpaces = new string( ' ', currentPropFullName.Length );

		for(int idx = 0; idx < arr.Length; idx++)
		{
			sb.AppendFormat( "{0}  {1}{2}",
			                 alignSpaces,
			                 FormatValue( arr.GetValue( idx ) ),
			                 idx < arr.Length - 1? ",": string.Empty
				);

			sb.Append( Environment.NewLine );
		}
		sb.AppendFormat( "{0}  ]", alignSpaces );
		sb.Append( Environment.NewLine );
	}

	private static void WriteScalarValue(object value, string currentPropFullName, StringBuilder sb)
	{
		sb.AppendFormat( "{0}: {1}",
		                 currentPropFullName,
		                 FormatValue( value ) );
		sb.Append( Environment.NewLine );
	}

	private static string FormatValue(object value)
	{
		if(value == null)
			return "<NULL>";

		return value.GetType() == typeof(string)? "\"" + value.ToString() + "\"": value.ToString();
	}

	private static bool IsDataObject(Type type)
	{
		if(type.IsPrimitive || type == typeof(string) || type.IsValueType)
			return false;

		return true;
	}
}