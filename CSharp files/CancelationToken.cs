using System;
using System.Threading;

namespace KadrPhotoUpdater2016.Core
{
	internal class CancelationToken
	{
		private volatile bool _bNeedCancel;

		private volatile bool _isCanceled;

		private readonly ManualResetEvent _canceledWait = new ManualResetEvent(false);

		public EventWaitHandle Cancel()
		{
			_bNeedCancel = true;
			return _canceledWait;
		}

		public bool IsNeedCancel
		{
			get { return _bNeedCancel; }
		}

		public void SetCanceled()
		{
			_canceledWait.Set();
			_isCanceled = true;
			RaiseCanceled();
		}

		public bool IsCanceled
		{
			get { return _isCanceled; }
		}

		public event EventHandler Canceled;

		private void RaiseCanceled()
		{
			if(Canceled != null)
				Canceled(this, EventArgs.Empty);
		}
	}
}