﻿using System;

public class Algorithms
{
	[Serializable]
	public delegate void RepeatAlgDelegate(int idx);

	public static void Repeat(int count, RepeatAlgDelegate action)
	{
		if (action == null)
			throw new ArgumentNullException("action");

		for (int index = 0; index < count; index++)
		{
			action(index);
		}
	}

	public static void Repeat(int count, Action action)
	{
		if (action == null)
			throw new ArgumentNullException("action");

		for (int index = 0; index < count; index++)
		{
			action();
		}
	}
}
