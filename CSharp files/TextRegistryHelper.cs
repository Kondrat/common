﻿using System;
using System.Diagnostics;

namespace Model
{
	internal static class TextRegistryHelper
	{
		#region static data

		private static readonly char[] s_statementDelimeters = new char[]
			{
				'.', '!', '?', '…'
			};

		#endregion

		#region FormatAllCapital

		public static string FormatAllCapital(string text)
		{
			return FormatAllCapital( text, 0, text.Length );
		}

		public static string FormatAllCapital(string text, int start, int length)
		{
			Debug.Assert( start >= 0, "start >= 0" );
			return ProcessCharInternal( text, start, length, char.ToUpper );
		}

		#endregion

		#region FormatAllLower

		public static string FormatAllLower(string text)
		{
			return FormatAllLower( text, 0, text.Length );
		}

		public static string FormatAllLower(string text, int start, int length)
		{
			Debug.Assert( start >= 0, "start >= 0" );
			return ProcessCharInternal( text, start, length, char.ToLower );
		}

		#endregion

		#region FormatWordFromCapital

		public static string FormatWordFromCapital(string text)
		{
			return FormatWordFromCapital( text, 0, text.Length );
		}

		public static string FormatWordFromCapital(string text, int start, int length)
		{
			Debug.Assert( start >= 0, "start >= 0" );

			bool bIsPrewSymbolIsWordDelimeter = start > 0? IsWordDelimeter( text[start - 1] ): true;
			return ProcessCharInternal( text, start, length,
			                            ch =>
			                            {
			                            	if(bIsPrewSymbolIsWordDelimeter)
			                            	{
			                            		bIsPrewSymbolIsWordDelimeter = IsWordDelimeter( ch );
			                            		return char.ToUpper( ch );
			                            	}

			                            	bIsPrewSymbolIsWordDelimeter = IsWordDelimeter( ch );
			                            	return char.ToLower( ch );
			                            }
				);
		}

		#endregion

		#region FormatAsStatement

		public static string FormatAsStatement(string text)
		{
			return FormatAsStatement( text, 0, text.Length );
		}

		public static string FormatAsStatement(string text, int start, int length)
		{
			Debug.Assert( start >= 0, "start >= 0" );

			bool bIsPrewSymbolIsStatementDelimeter = IsAtStatementStart( start, text );
			return ProcessCharInternal( text, start, length,
			                            ch =>
			                            {
			                            	if(IsEmptyChar( ch ))
			                            		return ch;

			                            	if(bIsPrewSymbolIsStatementDelimeter)
			                            	{
			                            		bIsPrewSymbolIsStatementDelimeter = false;
			                            		return char.ToUpper( ch );
			                            	}

			                            	bIsPrewSymbolIsStatementDelimeter = IsStatementDelimeter( ch );
			                            	return char.ToLower( ch );
			                            }
				);
		}

		#endregion

		#region Internals

		private static string ProcessCharInternal(string text, int start, int length, Func<char, char> charProcessor)
		{
			char[] chars = text.ToCharArray();

			int startBound = Math.Max( 0, start );
			int endBound = Math.Min( length + startBound, chars.Length );

			for(int idx = startBound; idx < endBound; idx++)
				chars[idx] = charProcessor( chars[idx] );

			return new string( chars );
		}

		private static bool IsWordDelimeter(char ch)
		{
			return
				char.IsPunctuation( ch )
				|| char.IsWhiteSpace( ch )
				|| IsStatementDelimeter( ch )
				;
		}

		private static bool IsStatementDelimeter(char ch)
		{
			return Array.IndexOf( s_statementDelimeters, ch ) > -1;
		}

		private static bool IsAtStatementStart(int pos, string text)
		{
			do
			{
				pos--;
			} while(pos > 0 && IsEmptyChar( text[pos] ));

			return pos <= 0? true: IsStatementDelimeter( text[pos] );
		}

		private static bool IsEmptyChar(char ch)
		{
			return char.IsWhiteSpace( ch ) || ch == '-';
		}

		#endregion
	}
}