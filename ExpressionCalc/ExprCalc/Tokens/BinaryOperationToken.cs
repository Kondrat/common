using System.Diagnostics;

namespace ExprCalc.Tokens
{
	[DebuggerDisplay("Binary Op = {StrValue}, OperationPriority = {OperationPriority}")]
	class BinaryOperationToken :StringToken
	{
		public BinaryOperationToken(string value): base( value )
		{
			// Empty
		}

		public override bool IsNumber
		{
			get
			{
				return false;
			}
		}

		public override bool IsBinaryOperation
		{
			get
			{
				return true;
			}
		}

		public override bool IsBrace
		{
			get
			{
				return false;
			}
		}

		public override int OperationPriority
		{
			get
			{
				return Calculator.cnstOperationList.IndexOf( StrValue[0] );
			}
		}
	}
}