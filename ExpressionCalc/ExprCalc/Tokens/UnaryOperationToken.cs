using System.Diagnostics;

namespace ExprCalc.Tokens
{
	[DebuggerDisplay("Unary Op = {StrValue}")]
	class UnaryOperationToken :StringToken
	{
		public UnaryOperationToken(string value): base( value )
		{
			// Empty
		}

		public override bool IsNumber
		{
			get
			{
				return false;
			}
		}

		public override bool IsBinaryOperation
		{
			get
			{
				return false;
			}
		}

		public override bool IsUnaryOperation
		{
			get
			{
				return true;
			}
		}

		public override bool IsBrace
		{
			get
			{
				return false;
			}
		}

		public override int OperationPriority
		{
			get
			{
				return -1;
			}
		}
	}
}