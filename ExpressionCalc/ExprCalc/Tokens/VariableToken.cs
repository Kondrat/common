namespace ExprCalc.Tokens
{
	class VariableToken :StringToken
	{
		public VariableToken(string value)
			: base(value)
		{
			// Empty
		}

		public override bool IsNumber
		{
			get
			{
				return false;
			}
		}

		public override bool IsBinaryOperation
		{
			get
			{
				return false;
			}
		}

		public override bool IsBrace
		{
			get
			{
				return false;
			}
		}

		public override bool IsVariable
		{
			get
			{
				return true;
			}
		}
	}
}