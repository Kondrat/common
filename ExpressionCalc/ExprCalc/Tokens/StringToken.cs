using System.Diagnostics;

namespace ExprCalc.Tokens
{
	abstract class StringToken: Token
	{
		protected StringToken(string value)
		{
			Debug.Assert(!string.IsNullOrEmpty(value), "!string.IsNullOrEmpty( strValue )");
			this.strValue = value;
		}

		#region StrValue

		private readonly string strValue;

		public override string StrValue
		{
			get
			{
				return strValue;
			}
		}

		#endregion

		public override string ForPostfixView
		{
			get
			{
				return strValue;
			}
		}

	}
}