using System;
using System.Diagnostics;

namespace ExprCalc.Tokens
{
	[DebuggerDisplay("Number = {StrValue}")]
	internal class NumericToken :Token
	{
		public NumericToken(double value)
		{
			DblValue = value;
		}

		public override string StrValue
		{
			get
			{
				return DblValue.ToString( Calculator.NumberFormat );
			}
		}

		public override bool IsNumber
		{
			get
			{
				return true;
			}
		}

		public override bool IsBinaryOperation
		{
			get
			{
				return false;
			}
		}

		public override bool IsBrace
		{
			get
			{
				return false;
			}
		}

		public override string ForPostfixView
		{
			get
			{
				return StrValue;
			}
		}

		public double DblValue
		{
			get; private set;
		}

		#region operators

		public static NumericToken operator+(NumericToken arg1, NumericToken arg2)
		{
			return new NumericToken( arg1.DblValue + arg2.DblValue );
		}

		public static NumericToken operator-(NumericToken arg1, NumericToken arg2)
		{
			return new NumericToken(arg1.DblValue - arg2.DblValue);
		}

		public static NumericToken operator*(NumericToken arg1, NumericToken arg2)
		{
			return new NumericToken(arg1.DblValue * arg2.DblValue);
		}

		public static NumericToken operator/(NumericToken arg1, NumericToken arg2)
		{
			const double cnstDelta = 0.000000000001;
			const string cnstDivideByZero = "������� ������� �� 0.";

			if((Math.Abs( arg2.DblValue ) - 0.0f) < cnstDelta)
				throw new ArgumentException( cnstDivideByZero );

			return new NumericToken(arg1.DblValue / arg2.DblValue);
		}

		#endregion
	}
}