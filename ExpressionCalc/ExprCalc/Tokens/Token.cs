﻿using System;
using System.Diagnostics;

namespace ExprCalc.Tokens
{
	[DebuggerDisplay("StrValue = {StrValue}")]
	internal abstract class Token
	{
		public abstract string StrValue{ get;}

		public abstract bool IsNumber
		{ get;
		}

		public virtual bool IsVariable
		{
			get
			{
				return false;
			}
		}

		public virtual bool IsFunction
		{
			get
			{
				return false;
			}
		}

		public virtual bool IsArgSeparator
		{
			get
			{
				return false;
			}
		}

		public virtual bool IsUnaryOperation
		{
			get
			{
				return false;
			}
		}

		public abstract bool IsBinaryOperation
		{ get;
		}

		public abstract bool IsBrace
		{ get;
		}

		public virtual int OperationPriority
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		public virtual bool IsOpenBrace
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		public abstract string ForPostfixView
		{ 
			get;
		}
	}
}