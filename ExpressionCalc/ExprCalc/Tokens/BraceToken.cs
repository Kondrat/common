using System.Diagnostics;

namespace ExprCalc.Tokens
{
	[DebuggerDisplay("Brace = {StrValue}")]
	class BraceToken :StringToken
	{
		public BraceToken(string value): base( value )
		{
			// Empty
		}

		public override bool IsNumber
		{
			get
			{
				return false;
			}
		}

		public override bool IsBinaryOperation
		{
			get
			{
				return false;
			}
		}

		public override bool IsBrace
		{
			get
			{
				return true;
			}
		}

		public override bool IsOpenBrace
		{
			get
			{
				return StrValue[0] == Calculator.cnstOpenBrace;
			}
		}
	}
}