using System.Diagnostics;

namespace ExprCalc.Tokens
{
	[DebuggerDisplay("Separator = {StrValue}")]
	class ArgSeparatorToken :StringToken
	{
		public ArgSeparatorToken(string value)
			: base(value)
		{
			// Empty
		}

		public override bool IsNumber
		{
			get
			{
				return false;
			}
		}

		public override bool IsBinaryOperation
		{
			get
			{
				return false;
			}
		}

		public override bool IsBrace
		{
			get
			{
				return false;
			}
		}

		public override bool IsArgSeparator
		{
			get
			{
				return true;
			}
		}
	}
}