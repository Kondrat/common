using System.Diagnostics;

namespace ExprCalc.Tokens
{
	[DebuggerDisplay("Function = {StrValue}")]
	class FunctionToken :StringToken
	{
		public FunctionToken(string value)
			: base(value)
		{
			// Empty
		}

		public override bool IsNumber
		{
			get
			{
				return false;
			}
		}

		public override bool IsBinaryOperation
		{
			get
			{
				return false;
			}
		}

		public override bool IsBrace
		{
			get
			{
				return false;
			}
		}

		public override bool IsVariable
		{
			get
			{
				return false;
			}
		}

		public override bool IsFunction
		{
			get
			{
				return true;
			}
		}

		public override int OperationPriority
		{
			get
			{
				return -1; // Минимальный
			}
		}

		public override string ForPostfixView
		{
			get
			{
				return StrValue + "()";
			}
		}
	}
}