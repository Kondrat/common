﻿namespace ExprCalc
{
	public interface IVariableResolver
	{
		double this[string variableName]
		{
			get;
		}
	}
}
