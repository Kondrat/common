﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ExprCalc.Compilation;
using ExprCalc.Interpretation;
using ExprCalc.Tokens;

namespace ExprCalc
{
	public class Calculator
	{
		private Func<Func<string, double>, double> compiledExpr = null;

		internal static readonly Random Rnd = new Random((int)DateTime.Now.Ticks);

		private readonly List<Token> postfixTokens = null;

		public const string cnstOperationList = "+-*/^"; // В порядке возрастания приоритета
		public const char cnstOpenBrace = '('; 
		public const char cnstCloseBrace = ')'; 
		public const char cnstArgSeparator = ','; 

		public Calculator(string expression)
		{
			List<Token> tokens = new Tokenizer( expression ).Tokenize();
			postfixTokens = new PostfixTransform(tokens).Transform();
		}

		public string ExpressionInPostfixForm
		{
			get
			{
				StringBuilder sb = new StringBuilder();
				foreach(Token token in postfixTokens)
				{
					sb.AppendFormat( "{0} ", token.ForPostfixView );
				}

				return sb.ToString().TrimEnd();
			}
		}

		public string[] GetVariablesList()
		{
			return postfixTokens
				.Where( tok => tok.IsVariable )
				.Select( tok => tok.StrValue )
				.Distinct()
				.ToArray();
		}

		public void Compile()
		{
			ExprCompiler compiler = new ExprCompiler();
			compiledExpr = compiler.Compile( postfixTokens );
		}

		public double Evaluate(IVariableResolver context)
		{
			Func<string, double> variableResolver = name => context[name];
			return EvaluateInner( variableResolver );
		}

		public double Evaluate(Func<string, double> variableResolver)
		{
			return EvaluateInner(variableResolver);
		}

		public double Evaluate()
		{
			return EvaluateInner(name =>
								  {
									  string msg = string.Format("Не могу определить значение переменной '{0}'. Не указан контекст/метод для определения переменных.", name);
									  throw new ArgumentException(msg);
								  }
				);
		}

		private double EvaluateInner(Func<string, double> variableResolver)
		{
			try
			{
				return compiledExpr != null ?
						compiledExpr(varName => variableResolver(varName)) :
						new StackInterpreter(variableResolver).Evaluate(postfixTokens);
			}
			catch(InvalidProgramException) // В случае компиляции в MSIL.
			{
				throw new ArgumentException(StackInterpreter.cnstErrorInExpression);
			}
		}

		#region Globalization

		internal static string DecimalSeparator
		{
			get
			{
				return NumberFormat.NumberDecimalSeparator;
			}
		}

		public static NumberFormatInfo NumberFormat
		{
			get
			{
				return CultureInfo.CurrentCulture.NumberFormat;
			}
		}

		#endregion
	}
}