﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace ExprCalc.Compilation
{
	class FunctionsCompiler
	{
		private readonly ILGenerator gen;

		public FunctionsCompiler(ILGenerator gen)
		{
			this.gen = gen;
		}

		public void Compile(string fname)
		{
			MethodInfo mi = MapFunctionToMethodInfo( fname.ToLowerInvariant() );
			gen.EmitCall( OpCodes.Call, mi, null );
			return;
		}

		private MethodInfo MapFunctionToMethodInfo(string fname)
		{
			MethodInfo mi = null;
			switch(fname)
			{
				case Functions.cnstMsec:
					mi = typeof(FunctionsCompiler).GetMethod("MsecImpl");
					break;
				case Functions.cnstTicks:
					mi = typeof(FunctionsCompiler).GetMethod("TicksImpl");
					break;
				case Functions.cnstPow:
					mi = typeof(Math).GetMethod( "Pow" );
					break;
				case Functions.cnstSin:
					mi = typeof(Math).GetMethod( "Sin" );
					break;
				case Functions.cnstCos:
					mi = typeof(Math).GetMethod( "Cos" );
					break;
				case Functions.cnstRnd:
					mi = typeof(FunctionsCompiler).GetMethod( "RndImpl");
					break;
				default:
					throw new ArgumentException( string.Format( "Неизвестная функция: '{0}'.", fname ) );
			}

			return mi;
		}

		#region Реализация функций для использования в компиляторе

		public static double RndImpl(double maxVal)
		{
			lock(Calculator.Rnd)
			{
				return Calculator.Rnd.Next(0, (int)maxVal + 1);
			}
		}

		public static double MsecImpl()
		{
			return DateTime.Now.Millisecond;
		}

		public static double TicksImpl()
		{
			return DateTime.Now.Ticks;
		}

		#endregion
	}
}
