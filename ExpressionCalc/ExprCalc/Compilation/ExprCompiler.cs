﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using ExprCalc.Tokens;

namespace ExprCalc.Compilation
{
	class ExprCompiler
	{
		public Func<Func<string, double>, double> Compile(List<Token> postfixTokens)
		{
			Type[] methodArgs = { typeof(Func<string, double>) };
			DynamicMethod evaluator = new DynamicMethod(
				"CompiledEvaluator",
				typeof(double),
				methodArgs,
				typeof(ExprCompiler).Module);

			GenerateILCode( evaluator, postfixTokens );

			Func<Func<string, double>, double> evaluatorRef = (Func<Func<string, double>, double>)evaluator.CreateDelegate(typeof(Func<Func<string, double>, double>));
			return evaluatorRef;
		}

		private void GenerateILCode(DynamicMethod evaluator, List<Token> postfixTokens)
		{
			var variableNames = postfixTokens
				.Where( tok => tok.IsVariable )
				.Select( tok => tok.StrValue.ToLowerInvariant() )
				.Distinct()
				.ToArray()
				;

			evaluator.InitLocals = true;

			ILGenerator gen = evaluator.GetILGenerator();
			var varsInfo = DeclareVariables(gen, variableNames);
			GenerateResolveVariables(gen, varsInfo);
			gen.Emit(OpCodes.Nop);
			GenerateBody( gen, postfixTokens, varsInfo );

			gen.Emit( OpCodes.Ret );
		}

		private void GenerateBody(ILGenerator gen, List<Token> postfixTokens, IDictionary<string, LocalBuilder> varsInfo)
		{
			foreach(Token token in postfixTokens)
			{
				if(token.IsNumber)
				{
					NumericToken ntok = (NumericToken)token;
					gen.Emit( OpCodes.Ldc_R8, ntok.DblValue );
				}
				else if(token.IsUnaryOperation)
				{
					Debug.Assert(token.StrValue == "-" || token.StrValue == "+");
					
					if (token.StrValue == "-")
						gen.Emit(OpCodes.Neg);
				}
				else if(token.IsVariable)
				{
					VariableToken vtok = (VariableToken)token;
					gen.Emit( OpCodes.Ldloc, varsInfo[vtok.StrValue.ToLowerInvariant()].LocalIndex );
				}
				else if(token.IsBinaryOperation)
				{
					BinaryOperationToken otok = (BinaryOperationToken)token;

					if(otok.StrValue == "^")
					{
						FunctionsCompiler functionsCompiler = new FunctionsCompiler(gen);
						functionsCompiler.Compile(Functions.cnstPow);
					}
					else
						gen.Emit( MapOperationToOpCode(otok.StrValue));
				}
				else if(token.IsFunction)
				{
					FunctionToken ftok = (FunctionToken)token;
					FunctionsCompiler functionsCompiler = new FunctionsCompiler(gen);
					functionsCompiler.Compile( ftok.StrValue );
				}
			}
		}

		private OpCode MapOperationToOpCode(string opSymbol)
		{
			OpCode op;
			switch(opSymbol)
			{
				case "+":
					op = OpCodes.Add;
					break;
				case "-":
					op = OpCodes.Sub;
					break;
				case "*":
					op = OpCodes.Mul;
					break;
				case "/":
					op = OpCodes.Div;
					break;
				default:
					throw new ArgumentException(string.Format("Неизвестная операция: {0}.", opSymbol));
			}
			return op;
		}

		private IDictionary<string, LocalBuilder> DeclareVariables(ILGenerator gen, string[] variables)
		{
			var dict = new SortedList<string, LocalBuilder>(variables.Length);
			foreach(string @var in variables)
			{
				dict.Add( @var, gen.DeclareLocal( typeof(double) ));
			}
			return dict;
		}

		private void GenerateResolveVariables(ILGenerator gen, IDictionary<string, LocalBuilder> variables)
		{
			MethodInfo resolverFuncInfo = typeof(ExprCompiler).GetMethod("ResolveVariableImpl");

			foreach(var pair in variables)
			{
				gen.Emit( OpCodes.Ldarg_0);	// первый аргумент компилируемого метода (resolver)
				gen.Emit( OpCodes.Ldstr, pair.Key );
				gen.EmitCall( OpCodes.Call, resolverFuncInfo, null );
				gen.Emit( OpCodes.Stloc, pair.Value.LocalIndex );
			}
		}

		public static double ResolveVariableImpl( Func<string, double> resolver, string name)
		{
			// Этот метод вызывается из скомпилированного кода для получения значения переменной по её имени. 
			//	Нужен этот метод, чтобы не генерировать вручную обработку исключений.
			try
			{
				return resolver( name );
			}
			catch(Exception)
			{
				string msg = string.Format("Не указано значение переменной: '{0}'", name);
				throw new ArgumentException( msg );
			}
		}
	}
}