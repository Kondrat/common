using System;
using System.Collections.Generic;
using ExprCalc.Tokens;

namespace ExprCalc
{
	internal class PostfixTransform
	{
		private readonly List<Token> tokens;
		private readonly Stack<Token> stack = new Stack<Token>();

		public PostfixTransform(List<Token> tokens)
		{
			this.tokens = tokens;
		}

		public List<Token> Transform()
		{
			stack.Clear();
			var postfixTokens = new List<Token>(tokens.Count);

			foreach(Token token in tokens)
			{
				if(token.IsNumber || token.IsVariable)
					postfixTokens.Add( token );
				else if(token.IsUnaryOperation)
					stack.Push( token );
				else if(token.IsFunction)
					stack.Push( token );
				else if(token.IsArgSeparator)
				{
					while(stack.Count > 0 && !(stack.Peek().IsBrace && stack.Peek().IsOpenBrace))
						postfixTokens.Add( stack.Pop() );

					if(stack.Count == 0)
						throw new ArgumentException( "Несогласованность скобок." );
				}
				else if(token.IsBinaryOperation)
				{
					if(stack.Count > 0)
					{
						Token top = stack.Peek();
						//if(top.IsBinaryOperation && token.OperationPriority < top.OperationPriority)
						if( top.IsUnaryOperation || (top.IsBinaryOperation && token.OperationPriority < top.OperationPriority))
						{
							do
							{
								postfixTokens.Add( stack.Pop() );
							} while(
								stack.Count > 0 &&
								!stack.Peek().IsBrace &&
								stack.Peek().OperationPriority >= token.OperationPriority
								);
						}
					}

					stack.Push( token );
				}
				else if(token.IsBrace)
				{
					if(token.IsOpenBrace)
						stack.Push( token );
					else
					{
						while(true)
						{
							if(stack.Count == 0)
								throw new ArgumentException( "Несогласованность скобок." );

							Token top = stack.Pop();
							if(top.IsBrace && top.IsOpenBrace)
							{
								if(stack.Count > 0 && (stack.Peek().IsFunction || stack.Peek().IsBinaryOperation ))
									postfixTokens.Add( stack.Pop() );

								break;
							}
							else
								postfixTokens.Add( top );
						}
					}
				}
			}

			while(stack.Count > 0)
			{
				Token top = stack.Pop();

				if( !top.IsUnaryOperation && !top.IsBinaryOperation)
					throw new ArgumentException( "Несогласованность скобок." );

				postfixTokens.Add( top );
			}

			return postfixTokens;
		}
	}
}