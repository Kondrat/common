﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ExprCalc.Tokens;

namespace ExprCalc.Interpretation
{
	class StackInterpreter
	{
		public const string cnstErrorInExpression = "Ошибка в выражении.";
		private readonly Helper helper;

		public StackInterpreter(Func<string, double> variableResolver)
		{
			helper = new Helper( variableResolver);
		}

		public double Evaluate(List<Token> postfixTokens)
		{
			Stack<Token> stack = new Stack<Token>();

			foreach(Token token in postfixTokens)
			{
				Debug.Assert(
					token.IsBinaryOperation || token.IsUnaryOperation || token.IsNumber || token.IsVariable || token.IsFunction,
					"token.IsBinaryOperation || token.IsUnaryOperation || token.IsNumber || token.IsVariable || token.IsFunction"
					);

				if(token.IsNumber || token.IsVariable)
					stack.Push( token );
				else if(token.IsFunction)
					ProcessFunction( token, stack );
				else if(token.IsUnaryOperation)
				{
					Debug.Assert(token.StrValue == "-" || token.StrValue == "+");

					if (stack.Count < 1)
						throw new ArgumentException( cnstErrorInExpression );

					if(token.StrValue == "-")
					{
						NumericToken tArg = helper.GetArgumentFromStack(stack);
						NumericToken tRes = new NumericToken( - tArg.DblValue );
						stack.Push(tRes);
					}
				}
				else if(token.IsBinaryOperation)
				{
					// Только бинарные операции
					if(stack.Count < 2)
						throw new ArgumentException( cnstErrorInExpression );

					NumericToken tArg2 = helper.GetArgumentFromStack(stack);
					NumericToken tArg1 = helper.GetArgumentFromStack(stack);

					var op = OperationSymbolToFunction( token.StrValue );
					NumericToken tRes = op( tArg1, tArg2 );
					stack.Push( tRes);
				}
			}

			if(stack.Count != 1)
				throw new ArgumentException(cnstErrorInExpression);

			Token last = stack.Pop();

			if(last.IsNumber)
				return ((NumericToken)last).DblValue;

			if(last.IsVariable)
				return helper.ResolveVariable( last ).DblValue;

			throw new ArgumentException(cnstErrorInExpression);
		}

		private Func<NumericToken, NumericToken, NumericToken> OperationSymbolToFunction(string opSymbol)
		{
			Func<NumericToken, NumericToken, NumericToken> op;
			switch(opSymbol)
			{
				case "+":
					op = (arg1, arg2) => arg1 + arg2;
					break;
				case "-":
					op = (arg1, arg2) => arg1 - arg2;
					break;
				case "*":
					op = (arg1, arg2) => arg1 * arg2;
					break;
				case "/":
					op = (arg1, arg2) => arg1 / arg2;
					break;
				case "^":
					op = (arg1, arg2) =>
					     {
					     	double dRes = Math.Pow( arg1.DblValue, arg2.DblValue);
					     	return new NumericToken( dRes);
					     };
					break;
				default:
					throw new ArgumentException( string.Format( "Неизвестная операция: {0}.", opSymbol ) );
			}
			return op;
		}

		private void ProcessFunction(Token token, Stack<Token> stack)
		{
			new FunctionsInterpreterImpl( helper ).ProcessFunction( token, stack );
		}
	}
}