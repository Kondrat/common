﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ExprCalc.Tokens;

namespace ExprCalc.Interpretation
{
	class Helper
	{
		private readonly Func<string, double> variableResolver;

		public Helper(Func<string, double> variableResolver)
		{
			Debug.Assert(variableResolver != null, "variableResolver != null");
			this.variableResolver = variableResolver;
		}

		public NumericToken ResolveVariable(Token token)
		{
			try
			{
				string variableName = token.StrValue.ToLowerInvariant();
				double dValue = variableResolver(variableName);
				return new NumericToken( dValue);
			}
			catch(Exception)
			{
				string msg = string.Format("Не указано значение переменной: '{0}'", token.StrValue);
				throw new ArgumentException(msg);
			}
		}

		public NumericToken GetArgumentFromStack(Stack<Token> stack)
		{
			Token top = stack.Pop();
			if(top.IsVariable)
				return ResolveVariable(top);

			Debug.Assert(top is NumericToken, "top is NumericToken");
			return (NumericToken)top;
		}
	}
}