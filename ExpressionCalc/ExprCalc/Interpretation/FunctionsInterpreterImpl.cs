﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ExprCalc.Tokens;

namespace ExprCalc.Interpretation
{
	class FunctionsInterpreterImpl
	{
		readonly Helper helper;

		public FunctionsInterpreterImpl(Helper helper)
		{
			this.helper = helper;
		}

		public void ProcessFunction(Token token, Stack<Token> stack)
		{
			Debug.Assert(token.IsFunction, "token.IsFunction");

			NumericToken res = null;
			FunctionsInterpreterImpl functions = new FunctionsInterpreterImpl(helper);
			switch(token.StrValue.ToLowerInvariant())
			{
				case Functions.cnstMsec:
					res = functions.MSec();
					break;
				case Functions.cnstPow:
					res = functions.Pow(stack);
					break;
				case Functions.cnstSin:
					res = functions.Sin(stack);
					break;
				case Functions.cnstCos:
					res = functions.Cos(stack);
					break;
				case Functions.cnstRnd:
					res = functions.Rnd(stack);
					break;
				case Functions.cnstTicks:
					res = functions.Tiks(stack);
					break;
				default:
					throw new ArgumentException(string.Format("Неизвестная функция: '{0}'.", token.StrValue));
			}

			stack.Push(res);
		}

		private NumericToken Tiks(Stack<Token> stack)
		{
			return new NumericToken( DateTime.Now.Ticks );
		}

		private NumericToken Pow(Stack<Token> stack)
		{
			return BinaryDoubleFunc( stack, (a1, a2) => Math.Pow( a1, a2 ) );
		}

		private NumericToken Sin(Stack<Token> stack)
		{
			return UnaryDoubleFunc( stack, val => Math.Sin( val ) );
		}

		private NumericToken MSec()
		{
			return new NumericToken( DateTime.Now.Millisecond);
		}

		private NumericToken Cos(Stack<Token> stack)
		{
			return UnaryDoubleFunc( stack, val => Math.Cos( val ) );
		}

		private NumericToken Rnd(Stack<Token> stack)
		{
			return UnaryIntFunc( stack, val =>
			                            {
											lock(Calculator.Rnd)
			                            	{
			                            		return Calculator.Rnd.Next( 0, val+1);
			                            	}
			                            } 
				);
		}

		#region Internals

		private NumericToken BinaryDoubleFunc(Stack<Token> stack, Func<double, double, double> func)
		{
			CheckArgumentCount( stack, 2 );

			NumericToken arg2 = helper.GetArgumentFromStack( stack );
			NumericToken arg1 = helper.GetArgumentFromStack( stack );

			double fRes = func( arg1.DblValue, arg2.DblValue);
			return new NumericToken(fRes);
		}

		private NumericToken UnaryDoubleFunc(Stack<Token> stack, Func<double, double> func)
		{
			CheckArgumentCount(stack, 1);
			NumericToken arg = helper.GetArgumentFromStack(stack);
			double fRes = func(arg.DblValue);
			return new NumericToken(fRes);
		}

		private NumericToken UnaryIntFunc(Stack<Token> stack, Func<int, int> func)
		{
			CheckArgumentCount(stack, 1);
			NumericToken arg = helper.GetArgumentFromStack(stack);
			int iRes = func( (int)arg.DblValue);
			return new NumericToken(iRes);
		}

		private void CheckArgumentCount(Stack<Token> stack, int count)
		{
			if(stack.Count < count)
				throw new ArgumentException( string.Format( "Для функции требуется {0} аргументов.", count ) );
		}

		#endregion
	}
}