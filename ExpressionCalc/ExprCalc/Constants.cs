﻿using System;
using System.Collections.Generic;

namespace ExprCalc
{
	static class Constants
	{
		public const string cnstPi = "pi";
		public const string cnstE = "e";

		static readonly IDictionary<string, double > s_constants = new SortedList<string, double>();

		static Constants()
		{
			s_constants.Add( cnstPi, Math.PI );
			s_constants.Add( cnstE, Math.E );
		}

		public static bool IsConstant(string name)
		{
			return s_constants.ContainsKey( name.ToLowerInvariant() );
		}

		public static double GetConstantValue(string name)
		{
			return s_constants[name.ToLowerInvariant()];
		}
	}
}
