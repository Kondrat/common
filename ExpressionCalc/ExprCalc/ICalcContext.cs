﻿namespace ExprCalc
{
	public interface ICalcContext
	{
		double this[string variableName]
		{
			get;
		}
	}
}
