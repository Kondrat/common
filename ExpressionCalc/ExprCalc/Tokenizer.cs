﻿using System;
using System.Collections.Generic;
using System.Text;
using ExprCalc.Tokens;

namespace ExprCalc
{
	class Tokenizer
	{
		private readonly string expression;

		public Tokenizer(string expression)
		{
			this.expression = expression;
		}

		private delegate Token TokenFactory(string value);

		public List<Token> Tokenize()
		{
			List<Token> tokens = new List<Token>();

			int pos = 0;
			while(pos < expression.Length)
			{
				SkipWhitespaces(ref pos);

				if(pos > expression.Length -1)
					break;

				Token token;
				if(IsAtNumber(expression[pos]))
				{
					token = ReadMultiSymbolToken(ref pos,
					                             ch => IsDecimalNumber( ch ), 
					                             val=> new NumericToken( double.Parse( val, Calculator.NumberFormat ))
						);
				}
				else if(IsAtUnaryOperation( pos, expression))
				{
					token = ReadOneSymbolToken(ref pos, 
					                           val => new UnaryOperationToken( val )
						);
				}
				else if(IsAtBinaryOperation( expression[pos] ))
				{
					token = ReadOneSymbolToken(ref pos, 
					                           val => new BinaryOperationToken( val )
						);
				}
				else if(IsAtBrace( expression[pos] ))
				{
					token = ReadOneSymbolToken(ref pos, 
					                           val => new BraceToken( val )
						);
				}
				else if(IsAtArgSeparator( expression[pos] ))
				{
					token = ReadOneSymbolToken(ref pos, 
					                           val => new ArgSeparatorToken( val )
						);
				}
				else if(IsAtFunction( pos, expression )) // Проверка должна выполняться перед проверкой на переменную!
				{
					token = ReadMultiSymbolToken( ref pos,
					                              ch => char.IsLetterOrDigit( ch ),
					                              val => new FunctionToken( val )
						);
				}
				else if(IsAtVariable( expression[pos] ))
				{
					token = ReadMultiSymbolToken( ref pos,
					                              ch => char.IsLetterOrDigit( ch ),
					                              val => new VariableToken( val )
						);

					if(Constants.IsConstant(token.StrValue))
						token = new NumericToken( Constants.GetConstantValue( token.StrValue) );
				}
				else
					throw new ArgumentException( string.Format( "Неожиданный символ в позиции {0}: {1}", pos, expression[pos] ) );

				tokens.Add( token );
			}

			return tokens;
		}

		#region Reading

		private bool IsDecimalNumber(char ch)
		{
			return char.IsDigit( ch ) || Calculator.DecimalSeparator.IndexOf( ch ) != -1;
		}

		private Token ReadMultiSymbolToken(ref int pos, Predicate<char> pred, TokenFactory tokenFactory)
		{
			StringBuilder sb = new StringBuilder();

			while( pos < expression.Length && pred(expression[pos]))
			{
				sb.Append(expression[pos]);
				pos++;
			}

			return tokenFactory(sb.ToString());
		}

		private Token ReadOneSymbolToken(ref int pos, TokenFactory tokenFactory)
		{
			// Операции все односимвольные
			return tokenFactory(expression[pos++].ToString());
		}

		#endregion

		#region Testing

		private static bool IsAtNumber(char firstSymbolOfToken)
		{
			return char.IsDigit(firstSymbolOfToken);
		}

		private static bool IsAtVariable(char firstSymbolOfToken)
		{
			// Переменная начинается с буквы
			return char.IsLetter(firstSymbolOfToken);
		}

		private static bool IsAtArgSeparator(char firstSymbolOfToken)
		{
			// Переменная начинается с буквы
			return firstSymbolOfToken == Calculator.cnstArgSeparator;
		}

		private static bool IsAtFunction(int pos, string expression)
		{
			if(!char.IsLetter( expression[pos]))
				return false;

			while(++pos < expression.Length )
			{
				char ch = expression[pos];
				if(ch == Calculator.cnstOpenBrace)
					return true;
				if(!char.IsLetterOrDigit(ch))
					return false;
			}

			return false;
		}

		private static bool IsAtUnaryOperation(int pos, string expression)
		{
			// Унарная операция только '-' и '+'.
			if (expression[pos] != '-' && expression[pos] != '+')
				return false;

			pos--;
			SkipWhitespacesBack( ref pos, expression );
			if(pos >= 0)
			{
				char ch = expression[pos];
				if(IsAtArgSeparator( ch ) || IsAtBinaryOperation( ch ) || ch == Calculator.cnstOpenBrace)
					return true;
				else
					return false;
			}
			return true;
		}

		private static bool IsAtBinaryOperation(char firstSymbolOfToken)
		{
			return Calculator.cnstOperationList.IndexOf(firstSymbolOfToken) != -1;
		}

		private static bool IsAtBrace(char firstSymbolOfToken)
		{
			return firstSymbolOfToken == Calculator.cnstOpenBrace || firstSymbolOfToken == Calculator.cnstCloseBrace;
		}

		#endregion

		void SkipWhitespaces(ref int pos)
		{
			while(pos < expression.Length && char.IsWhiteSpace(expression[pos]))
				pos++;
		}

		static void SkipWhitespacesBack(ref int pos, string expression)
		{
			while(pos >= 0 && char.IsWhiteSpace(expression[pos]))
				pos--;
		}
	}
}