﻿namespace ExprCalc
{
	static class Functions
	{
		// После добавления имени функции здесь нужно добавить её реализацию в FunctionsInterpreterImpl
		//	и компиляцию в FunctionsCompiler.

		public const string cnstMsec = "msec";
		public const string cnstPow = "pow";
		public const string cnstSin = "sin";
		public const string cnstCos = "cos";
		public const string cnstRnd = "rnd";
		public const string cnstTicks = "ticks";
	}
}
