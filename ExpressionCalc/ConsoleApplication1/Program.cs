﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExprCalc;

namespace ConsoleApplication1
{
	class Program
	{
		static void Main(string[] args)
		{
			string expr = "20765,64 * 89 + 14688,64 * 89";

			Calculator calculator = new Calculator( expr );
			//calculator.Compile();
			double result = calculator.Evaluate();

			Console.WriteLine(expr);
			Console.WriteLine(result);
		}
	}
}
