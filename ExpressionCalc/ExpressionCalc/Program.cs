﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ExprCalc;

namespace ExpressionCalcTest
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
		}

		public static float Formula(Func<string, float> variableResolver)
		{
			float x = variableResolver("x");
			float res = (float)(Math.Pow(x, 2) + x);
			//float res = (float)(Math.Pow( 1 + 1, Math.Sin( 3 ) ) * (2 + x));
			return DateTime.Now.Millisecond;
		}
	}
}