﻿namespace ExpressionCalcTest
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnClose = new System.Windows.Forms.Button();
			this.txtExpression = new System.Windows.Forms.TextBox();
			this.btnCalc = new System.Windows.Forms.Button();
			this.lblRes = new System.Windows.Forms.Label();
			this.txtPostfix = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtX = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtY = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtZ = new System.Windows.Forms.TextBox();
			this.cbUseCompiler = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(410, 12);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 5;
			this.btnClose.Text = "C&lose";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// txtExpression
			// 
			this.txtExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtExpression.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtExpression.Location = new System.Drawing.Point(12, 14);
			this.txtExpression.Name = "txtExpression";
			this.txtExpression.Size = new System.Drawing.Size(392, 24);
			this.txtExpression.TabIndex = 1;
			this.txtExpression.Text = "9 / (-3) / 4";
			// 
			// btnCalc
			// 
			this.btnCalc.Location = new System.Drawing.Point(12, 44);
			this.btnCalc.Name = "btnCalc";
			this.btnCalc.Size = new System.Drawing.Size(185, 23);
			this.btnCalc.TabIndex = 0;
			this.btnCalc.Text = "&Calc";
			this.btnCalc.UseVisualStyleBackColor = true;
			this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
			// 
			// lblRes
			// 
			this.lblRes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblRes.Location = new System.Drawing.Point(156, 127);
			this.lblRes.Name = "lblRes";
			this.lblRes.Size = new System.Drawing.Size(329, 24);
			this.lblRes.TabIndex = 10;
			this.lblRes.Text = "Result";
			// 
			// txtPostfix
			// 
			this.txtPostfix.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtPostfix.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtPostfix.Location = new System.Drawing.Point(12, 73);
			this.txtPostfix.Name = "txtPostfix";
			this.txtPostfix.ReadOnly = true;
			this.txtPostfix.Size = new System.Drawing.Size(392, 24);
			this.txtPostfix.TabIndex = 6;
			this.txtPostfix.TabStop = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 107);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(17, 13);
			this.label1.TabIndex = 7;
			this.label1.Text = "X:";
			// 
			// txtX
			// 
			this.txtX.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtX.Location = new System.Drawing.Point(35, 103);
			this.txtX.Name = "txtX";
			this.txtX.Size = new System.Drawing.Size(115, 22);
			this.txtX.TabIndex = 2;
			this.txtX.Text = "1";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 135);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(17, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "Y:";
			// 
			// txtY
			// 
			this.txtY.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtY.Location = new System.Drawing.Point(35, 131);
			this.txtY.Name = "txtY";
			this.txtY.Size = new System.Drawing.Size(115, 22);
			this.txtY.TabIndex = 3;
			this.txtY.Text = "2";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 163);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(17, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Z:";
			// 
			// txtZ
			// 
			this.txtZ.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtZ.Location = new System.Drawing.Point(35, 159);
			this.txtZ.Name = "txtZ";
			this.txtZ.Size = new System.Drawing.Size(115, 22);
			this.txtZ.TabIndex = 4;
			this.txtZ.Text = "3";
			// 
			// cbUseCompiler
			// 
			this.cbUseCompiler.AutoSize = true;
			this.cbUseCompiler.Checked = true;
			this.cbUseCompiler.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbUseCompiler.Location = new System.Drawing.Point(159, 106);
			this.cbUseCompiler.Name = "cbUseCompiler";
			this.cbUseCompiler.Size = new System.Drawing.Size(106, 17);
			this.cbUseCompiler.TabIndex = 11;
			this.cbUseCompiler.Text = "Компилировать";
			this.cbUseCompiler.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AcceptButton = this.btnCalc;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(497, 196);
			this.Controls.Add(this.cbUseCompiler);
			this.Controls.Add(this.txtZ);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtY);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.txtX);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lblRes);
			this.Controls.Add(this.btnCalc);
			this.Controls.Add(this.txtPostfix);
			this.Controls.Add(this.txtExpression);
			this.Controls.Add(this.btnClose);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Вычислитель выражений";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TextBox txtExpression;
		private System.Windows.Forms.Button btnCalc;
		private System.Windows.Forms.Label lblRes;
		private System.Windows.Forms.TextBox txtPostfix;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtX;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtY;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtZ;
		private System.Windows.Forms.CheckBox cbUseCompiler;
	}
}