﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ExprCalc;
using ExprCalc.Tokens;

namespace ExpressionCalcTest
{
	public partial class MainForm :Form, IVariableResolver
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnCalc_Click(object sender, EventArgs e)
		{
			try
			{
				lblRes.Text = string.Empty;
				txtPostfix.Text = string.Empty;

				Calculator calc = new Calculator( txtExpression.Text );
				txtPostfix.Text = calc.ExpressionInPostfixForm;
				if(cbUseCompiler.Checked)
					calc.Compile();

				lblRes.Text = calc.Evaluate(this).ToString(Calculator.NumberFormat);
			}
			catch(Exception ex)
			{
				//MessageBox.Show( ex.Message );
				lblRes.Text = ex.Message;
			}
		}

		public double this[string variableName]
		{
			get
			{
				TextBox txt;
				switch(variableName.ToLowerInvariant())
				{
					case "x":
						txt = txtX;
						break;
					case "y":
						txt = txtY;
						break;
					case "z":
						txt = txtZ;
						break;
					default:
						throw new KeyNotFoundException( variableName );
				}

				return double.Parse( txt.Text, Calculator.NumberFormat );
			}
		}
	}
}