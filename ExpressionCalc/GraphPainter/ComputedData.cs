﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using ExprCalc;

namespace GraphPainter
{
	class ComputedData
	{
		private readonly Calculator calculator = null;

		public ComputedData(string expression, bool bUseCompilation)
		{
			this.calculator =new Calculator( expression);

			if(bUseCompilation)
				this.calculator.Compile();
		}

		public double MinX = 0;
		public double MaxX = 0;
		public double Step = 0.01f;

		public IEnumerable<PointF> DataIterator
		{
			get
			{
				Debug.Assert(MinX < MaxX, "MinX < MaxX");
				Debug.Assert(Step > 0, "Step > 0");

				for(double posX = MinX; posX <= MaxX; posX += Step)
				{
					double dValue = posX;
					double posY = calculator.Evaluate( name =>
					                                   {
					                                   	Debug.Assert( name.ToLowerInvariant() == "x", "name.ToLowerInvariant() == 'x'" );
					                                   	return dValue;
					                                   }
						);
					yield return new PointF((float)posX, (float)posY);
				}
			}
		}

		#region Seq. calculation

		public PointF[] Calculate()
		{
			Debug.Assert(MinX < MaxX, "MinX < MaxX");
			Debug.Assert(Step > 0, "Step > 0");

			int count = (int)Math.Ceiling((MaxX - MinX) / Step);
			List<PointF> res = new List<PointF>(count + 1);

			for(double posX = MinX; posX <= MaxX; posX += Step)
			{
				double dValue = posX;
				double posY = calculator.Evaluate(name => dValue);
				res.Add(new PointF((float)posX, (float)posY));
			}
			return res.ToArray();
		}

		#endregion

		#region Parallel calculation with join results

		struct RangeResult
		{
			public RangeResult(int rangeNum, List<PointF> result)
			{
				RangeNum = rangeNum;
				Result = result;
			}

			public readonly int RangeNum;
			public readonly List<PointF> Result;
		}

		public PointF[] CalculateParallelWithJoin()
		{
			Debug.Assert(MinX < MaxX, "MinX < MaxX");
			Debug.Assert(Step > 0, "Step > 0");

			int parallelizeDegree = Environment.ProcessorCount;
			int remainingJobs = parallelizeDegree;
			double rangeSize = (MaxX - MinX) / parallelizeDegree;

			// Делаем размер диапазона кратным шагу.
			rangeSize = Math.Ceiling(rangeSize / Step) * Step;

			var rangeResults = new List<RangeResult>(parallelizeDegree);

			using(ManualResetEvent ev = new ManualResetEvent(false))
			{
				int rangeNum = 0;
				for(double rangeStart = MinX; rangeNum < parallelizeDegree; rangeStart += rangeSize + Step, rangeNum++)
				{
					double currRangeStart = rangeStart;
					double currRangeEnd = rangeStart + rangeSize;
					int currRangeNum = rangeNum;

					ThreadPool.QueueUserWorkItem(
						delegate
						{
							var res = CalculateRange(currRangeStart, Math.Min( currRangeEnd, MaxX));
							lock(rangeResults)
								rangeResults.Add(new RangeResult(currRangeNum, res));

							if(Interlocked.Decrement( ref remainingJobs ) == 0)
								ev.Set();
						}
						);
				}
				ev.WaitOne();
			}

			PointF[] totalResult = JoinResults( rangeResults );
			return totalResult;
		}

		private PointF[] JoinResults(List<RangeResult> results)
		{
			return results
				.OrderBy( res => res.RangeNum )
				.SelectMany( res => res.Result )
				.ToArray();
		}

		public List<PointF> CalculateRange(double start, double end)
		{
			int count = (int)Math.Ceiling((end - start) / Step);
			List<PointF> res = new List<PointF>(count + 1);

			for(double posX = start; posX <= end; posX += Step)
			{
				double dValue = posX;
				double posY = calculator.Evaluate(name => dValue);
				res.Add(new PointF((float)posX, (float)posY));
			}
			return res;
		}

		#endregion

		#region Parallel calculation with no join results

		public PointF[] CalculateParallelNoJoin()
		{
			Debug.Assert(MinX < MaxX, "MinX < MaxX");
			Debug.Assert(Step > 0, "Step > 0");

			int parallelizeDegree = Environment.ProcessorCount;
			int remainingJobs = parallelizeDegree;
			double rangeSize = (MaxX - MinX) / parallelizeDegree;

			int totalPoints = (int)Math.Floor((MaxX - MinX) / Step) + 1;
			PointF[] result = new PointF[totalPoints];

			// Делаем размер диапазона кратным шагу.
			int pointsInStep = (int)Math.Ceiling(rangeSize / Step);
			rangeSize = pointsInStep * Step;

			using(ManualResetEvent ev = new ManualResetEvent(false))
			{
				int taskNum = 0;
				int startIndex = 0;
				for(double rangeStart = MinX; taskNum < parallelizeDegree; rangeStart += rangeSize, taskNum++, startIndex += pointsInStep)
				{
					double currRangeStart = rangeStart;
					int currStartIndex = startIndex;
					int currPointsToCalc = taskNum < parallelizeDegree-1? pointsInStep: (int)((MaxX - rangeStart) / Step) +1;

					ThreadPool.QueueUserWorkItem(
						delegate
						{
							CalculateRange2(currRangeStart, currStartIndex, currPointsToCalc, result);
							if(Interlocked.Decrement(ref remainingJobs) == 0)
								ev.Set();
						}
						);
				}
				ev.WaitOne();
			}

			return result;
		}

		private void CalculateRange2(double rangeStart, int startIndex, int pointsToCalc, PointF[] result)
		{
			// Этот метод выполняется в другом потоке. В случае, если при вычислении случается исключение, то оно не передаётся в основной поток.
			for(int idx = 0; idx < pointsToCalc; idx++)
			{
				double posX = idx * Step + rangeStart;
				double posY = calculator.Evaluate(name => posX);
				result[ startIndex + idx] = new PointF((float)posX, (float)posY);
			}
		}

		#endregion

		public string[] GetVariablesList()
		{
			return calculator.GetVariablesList();
		}
	}
}
