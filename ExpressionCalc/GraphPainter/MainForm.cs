﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using ExprCalc;

namespace GraphPainter
{
	public partial class MainForm :Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnCalc_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			try
			{
				var data = new ComputedData( txtFormula.Text, cbUseCompilation.Checked );
				data.MinX = float.Parse( txtFrom.Text, Calculator.NumberFormat);
				data.MaxX = float.Parse(txtTo.Text, Calculator.NumberFormat);
				data.Step = float.Parse(txtStep.Text, Calculator.NumberFormat);

				if(
					data.GetVariablesList().Length == 0 ||
					data.GetVariablesList().Length == 1 && data.GetVariablesList()[0].ToLowerInvariant() == "x"
					)
				{
					GC.Collect();
					Stopwatch sw = new Stopwatch();
					sw.Start();
					PointF[] result = cbParallel.Checked? data.CalculateParallelNoJoin(): data.Calculate();
					painter.Data = result;
					sw.Stop();
					lblCalcTime.Text = string.Format( "{0} {1} msec", lblCalcTime.Text.Substring( 0, lblCalcTime.Text.IndexOf( ':' ) + 1 ), sw.ElapsedMilliseconds );
					lblElementsCount.Text = string.Format("{0} {1}", lblElementsCount.Text.Substring(0, lblElementsCount.Text.IndexOf(':') + 1), result.Length);
					this.Cursor = Cursors.Default;
				}
				else
					MessageBox.Show( "Допустимо использование только переменной 'X'." );
			}
			catch(ArgumentException ex)
			{
				MessageBox.Show(ex.Message);
			}
			catch(FormatException ex)
			{
				MessageBox.Show( ex.Message );
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}
	}
}
