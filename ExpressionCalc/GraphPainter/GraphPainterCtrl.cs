﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace GraphPainter
{
	public partial class GraphPainterCtrl :Control
	{
		private RectangleF graphBounds = new RectangleF( -10, 10, 20, 20);
		private RectangleF screenBounds = new RectangleF( -10, 10, 20, 20);
		private float scaleX, scaleY;

		private Bitmap imgCache = null;
		private readonly Timer timerForRefresh = new Timer();

		public GraphPainterCtrl()
		{
			InitializeComponent();

			timerForRefresh.Stop();
			timerForRefresh.Interval = 200;
			timerForRefresh.Tick += timerForRefresh_Tick;
		}

		protected override void OnCreateControl()
		{
			base.OnCreateControl();
			DoubleBuffered = true;
			SetStyle(ControlStyles.ResizeRedraw, true);
		}

		private void timerForRefresh_Tick(object sender, EventArgs e)
		{
			timerForRefresh.Stop();
			imgCache = null;
			Invalidate();
		}

		#region Data

		private IEnumerable<PointF> data = Enumerable.Empty<PointF>();

		public IEnumerable<PointF> Data
		{
			get
			{
				return data;
			}
			set
			{
				imgCache = null;
				data = value ?? Enumerable.Empty<PointF>();
				RefreshBounds();
				Invalidate();
			}
		}

		#endregion

		private void RefreshBounds()
		{
			float minX =0, minY =0, maxX =0, maxY =0;
			foreach(PointF point in data)
			{
				minX = Math.Min( minX, point.X );
				maxX = Math.Max( maxX, point.X );
				minY = Math.Min( minY, point.Y );
				maxY = Math.Max( maxY, point.Y );
			}
			graphBounds = new RectangleF(minX, minY, maxX - minX, maxY - minY);
			if(graphBounds.IsEmpty)
				graphBounds.Inflate( 1, 1 );

			RecalculateScreen();
		}

		private void RecalculateScreen()
		{
			const float cnstOutsidePart = 30f;
			screenBounds = graphBounds;
			screenBounds.Inflate(screenBounds.Width / cnstOutsidePart, screenBounds.Height / cnstOutsidePart);

			scaleX = ClientRectangle.Width / screenBounds.Width;
			scaleY = ClientRectangle.Height / screenBounds.Height;
		}

		protected override void OnResize(EventArgs e)
		{
			if(timerForRefresh.Enabled)
				timerForRefresh.Stop();
			timerForRefresh.Start();

			RecalculateScreen();
			base.OnResize(e);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			Rectangle rcAll = ClientRectangle;
			Graphics gr = e.Graphics;
			gr.InterpolationMode = InterpolationMode.Low;

			if(!DesignMode)
			{
				if(imgCache == null)
					CreateBitmap();

				gr.DrawImage( imgCache, ClientRectangle, new Rectangle(Point.Empty, new Size(imgCache.Width, imgCache.Height)), GraphicsUnit.Pixel );
			}

			DrawBorder(rcAll, gr);
		}

		private void CreateBitmap()
		{
			using(Graphics grSample = CreateGraphics())
			{
				imgCache = new Bitmap(ClientRectangle.Width, ClientRectangle.Height, grSample);

				using(var gr = Graphics.FromImage(imgCache))
				{
					DrawAxes(ClientRectangle, gr);
					DrawGraph(gr);
					DrawBorder(ClientRectangle, gr);
				}
			}
		}

		private void DrawGraph(Graphics gr)
		{
			using(Pen pen = new Pen( Color.Yellow ))
			{
				gr.SmoothingMode = SmoothingMode.AntiAlias;

				var enumerator = data.GetEnumerator();
				if(enumerator.MoveNext())
				{
					PointF from;
					from = enumerator.Current;

					while(enumerator.MoveNext())
					{
						PointF to = enumerator.Current;
						DrawLine( gr, pen, from.X, from.Y, to.X, to.Y );
						from = to;
					}
				}
			}
		}

		private void DrawAxes(Rectangle rcAll, Graphics gr)
		{
			using(Pen pen = new Pen(Color.Green))
			{
				// 0X
				DrawLine( gr, pen,  graphBounds.Left,  0f, graphBounds.Right, 0f);
				// 0Y
				DrawLine( gr, pen, 0f, graphBounds.Top, 0f, graphBounds.Bottom );
			}
		}

		private void DrawLine(Graphics gr, Pen pen, float x1, float y1, float x2, float y2)
		{
			gr.DrawLine(pen, ToScreenX(x1), ToScreenY(y1), ToScreenX(x2), ToScreenY(y2));
		}

		private float ToScreenX(float x)
		{
			return (x - screenBounds.Left) * scaleX;
		}

		private float ToScreenY(float y)
		{
			return ClientRectangle.Height - (y - screenBounds.Top) * scaleY;
		}

		private void DrawBorder(Rectangle rcAll, Graphics gr)
		{
			ControlPaint.DrawBorder3D( gr, rcAll, Border3DStyle.Sunken );
		}
	}
}
