﻿namespace GraphPainter
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnClose = new System.Windows.Forms.Button();
			this.btnCalc = new System.Windows.Forms.Button();
			this.txtFormula = new System.Windows.Forms.TextBox();
			this.txtFrom = new System.Windows.Forms.TextBox();
			this.txtTo = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtStep = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.lblCalcTime = new System.Windows.Forms.Label();
			this.cbUseCompilation = new System.Windows.Forms.CheckBox();
			this.cbParallel = new System.Windows.Forms.CheckBox();
			this.lblElementsCount = new System.Windows.Forms.Label();
			this.painter = new GraphPainter.GraphPainterCtrl();
			this.SuspendLayout();
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(733, 521);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 8;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// btnCalc
			// 
			this.btnCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnCalc.Location = new System.Drawing.Point(431, 484);
			this.btnCalc.Name = "btnCalc";
			this.btnCalc.Size = new System.Drawing.Size(115, 61);
			this.btnCalc.TabIndex = 7;
			this.btnCalc.Text = "Calc";
			this.btnCalc.UseVisualStyleBackColor = true;
			this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
			// 
			// txtFormula
			// 
			this.txtFormula.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtFormula.Location = new System.Drawing.Point(12, 484);
			this.txtFormula.Name = "txtFormula";
			this.txtFormula.Size = new System.Drawing.Size(413, 26);
			this.txtFormula.TabIndex = 0;
			this.txtFormula.Text = "-sin(0,1*(x)) + 0,2*sin(2*x)*cos(pi/e * x)";
			// 
			// txtFrom
			// 
			this.txtFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtFrom.Location = new System.Drawing.Point(38, 524);
			this.txtFrom.Name = "txtFrom";
			this.txtFrom.Size = new System.Drawing.Size(57, 20);
			this.txtFrom.TabIndex = 2;
			this.txtFrom.Text = "-100";
			// 
			// txtTo
			// 
			this.txtTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtTo.Location = new System.Drawing.Point(144, 524);
			this.txtTo.Name = "txtTo";
			this.txtTo.Size = new System.Drawing.Size(57, 20);
			this.txtTo.TabIndex = 4;
			this.txtTo.Text = "100";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(9, 527);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(23, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "От:";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(113, 527);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(25, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "До:";
			// 
			// txtStep
			// 
			this.txtStep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtStep.Location = new System.Drawing.Point(252, 524);
			this.txtStep.Name = "txtStep";
			this.txtStep.Size = new System.Drawing.Size(57, 20);
			this.txtStep.TabIndex = 6;
			this.txtStep.Text = "0,01";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(216, 527);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(30, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Шаг:";
			// 
			// lblCalcTime
			// 
			this.lblCalcTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lblCalcTime.AutoSize = true;
			this.lblCalcTime.Location = new System.Drawing.Point(568, 484);
			this.lblCalcTime.Name = "lblCalcTime";
			this.lblCalcTime.Size = new System.Drawing.Size(89, 13);
			this.lblCalcTime.TabIndex = 1;
			this.lblCalcTime.Text = "Время расчета: ";
			// 
			// cbUseCompilation
			// 
			this.cbUseCompilation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cbUseCompilation.AutoSize = true;
			this.cbUseCompilation.Checked = true;
			this.cbUseCompilation.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbUseCompilation.Location = new System.Drawing.Point(571, 506);
			this.cbUseCompilation.Name = "cbUseCompilation";
			this.cbUseCompilation.Size = new System.Drawing.Size(106, 17);
			this.cbUseCompilation.TabIndex = 12;
			this.cbUseCompilation.Text = "Компилировать";
			this.cbUseCompilation.UseVisualStyleBackColor = true;
			// 
			// cbParallel
			// 
			this.cbParallel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cbParallel.AutoSize = true;
			this.cbParallel.Checked = true;
			this.cbParallel.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbParallel.Location = new System.Drawing.Point(571, 529);
			this.cbParallel.Name = "cbParallel";
			this.cbParallel.Size = new System.Drawing.Size(94, 17);
			this.cbParallel.TabIndex = 12;
			this.cbParallel.Text = "Параллельно";
			this.cbParallel.UseVisualStyleBackColor = true;
			// 
			// lblElementsCount
			// 
			this.lblElementsCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.lblElementsCount.AutoSize = true;
			this.lblElementsCount.Location = new System.Drawing.Point(730, 484);
			this.lblElementsCount.Name = "lblElementsCount";
			this.lblElementsCount.Size = new System.Drawing.Size(46, 13);
			this.lblElementsCount.TabIndex = 1;
			this.lblElementsCount.Text = "Эл-тов: ";
			// 
			// painter
			// 
			this.painter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.painter.BackColor = System.Drawing.Color.Black;
			this.painter.Data = new System.Drawing.PointF[0];
			this.painter.Location = new System.Drawing.Point(12, 12);
			this.painter.Name = "painter";
			this.painter.Size = new System.Drawing.Size(815, 466);
			this.painter.TabIndex = 9;
			this.painter.TabStop = false;
			this.painter.Text = "graphPainterCtrl1";
			// 
			// MainForm
			// 
			this.AcceptButton = this.btnCalc;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(839, 557);
			this.Controls.Add(this.cbParallel);
			this.Controls.Add(this.cbUseCompilation);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.lblElementsCount);
			this.Controls.Add(this.lblCalcTime);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtStep);
			this.Controls.Add(this.txtTo);
			this.Controls.Add(this.txtFrom);
			this.Controls.Add(this.txtFormula);
			this.Controls.Add(this.btnCalc);
			this.Controls.Add(this.painter);
			this.Controls.Add(this.btnClose);
			this.MinimumSize = new System.Drawing.Size(731, 370);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Графики";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnClose;
		private GraphPainterCtrl painter;
		private System.Windows.Forms.Button btnCalc;
		private System.Windows.Forms.TextBox txtFormula;
		private System.Windows.Forms.TextBox txtFrom;
		private System.Windows.Forms.TextBox txtTo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtStep;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblCalcTime;
		private System.Windows.Forms.CheckBox cbUseCompilation;
		private System.Windows.Forms.CheckBox cbParallel;
		private System.Windows.Forms.Label lblElementsCount;
	}
}

