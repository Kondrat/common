﻿using System;
using System.Collections.Generic;

public class SExpression
{
	#region Helper Structure

	struct Part
	{
		public Part(int start, int end)
		{
			this.start = start;
			this.end = end;
		}

		public readonly int start;
		public readonly int end;

		public string String(string source)
		{
			if (start >= end)
				return string.Empty;
			else
				return source.Substring(start, end - start);
		}

		public bool IsEqual( string sourceString, string sampleString, bool bIgnoreCase)
		{
			bool result = true;

			int len = end - start;
			if(sampleString.Length == len)
			{
				int pos = start;

				foreach(char ch in sampleString)
				{
					char ch1 = sourceString[pos++];
					if(bIgnoreCase)
						result = char.ToLower(ch) == char.ToLower(ch1);
					else
						result = ch == ch1;

					if(result == false)
						break;
				}
			}
			else
				result = false;

			return result;
		}
	}

	#endregion

	#region Data Fileds

	private readonly bool bIgnoreCase;

	/// <summary>
	/// Обрабатываемое S-выражение
	/// </summary>
	private readonly string expression = string.Empty;

	/// <summary>
	/// Разбитое на части S-выражение
	/// </summary>
	private string[] expressionParts = null;

	/// <summary>
	/// Разбитый на части запрос
	/// </summary>
	private string[] request = null;

	/// <summary>
	/// Текущий уровень вложенности
	/// </summary>
	private int nestLevel = 0;

	/// <summary>
	/// "Указатели" на найденную подстроку
	/// </summary>
	private Part resultPart;

	#endregion

	#region Interface

	/// <summary>
	/// Создание нового экземпляра парсера
	/// </summary>
	public SExpression(string expression)
	{
		if (expression == null)
			this.expression = string.Empty;
		else
			this.expression = expression;
	}

	/// <summary>
	/// Создание нового экземпляра парсера
	/// </summary>
	public SExpression(string expression, bool bIgnoreCase)
	{
		this.bIgnoreCase = bIgnoreCase;
		if (expression == null)
			this.expression = string.Empty;
		else
			this.expression = expression;
	}

	/// <summary>
	/// Обрабатываемое S-выражение
	/// </summary>
	public string Value
	{
		get
		{
			return expression;
		}
	}

	/// <summary>
	/// Поиск в S-выражении по запросу и выдача в виде строки. S-выражение должно иметь структуру '(key value)', где value - набор атомов или S-выражение. Возвращается (key value).
	/// </summary>
	/// <param name="requestString">Запрос типа 'key.key1.key2'. Допустим подстановочный знак '_'.</param>
	/// <returns>Найденное выражение</returns>
	public string ExprString(string requestString)
	{
		return ProcessForString(requestString, false);
	}

	/// <summary>
	/// Поиск в S-выражении по запросу и выдача в качестве объекта типа SExpression. S-выражение должно иметь структуру '(key value)', где value - набор атомов или S-выражение. Возвращается (key value).
	/// </summary>
	/// <param name="requestString">Запрос типа 'key.key1.key2'. Допустим подстановочный знак '_'.</param>
	/// <returns>Найденное выражение</returns>
	public SExpression ExprObject(string requestString)
	{
		return new SExpression(ExprString(requestString), this.bIgnoreCase);
	}

	/// <summary>
	/// Поиск в S-выражении по запросу и выдача в виде строки. S-выражение должно иметь структуру '(key value)', где value - набор атомов или S-выражение. Возвращается value.
	/// </summary>
	/// <param name="requestString">Запрос типа 'key.key1.key2'. Допустим подстановочный знак '_'.</param>
	/// <returns>Найденное выражение</returns>
	public string ValueString(string requestString)
	{
		return ProcessForString(requestString, true);
	}

	/// <summary>
	/// Поиск в S-выражении по запросу и выдача в качестве объекта типа SExpression. S-выражение должно иметь структуру '(key value)', где value - набор атомов или S-выражение. Возвращается value.
	/// </summary>
	/// <param name="requestString">Запрос типа 'key.key1.key2'. Допустим подстановочный знак '_'.</param>
	/// <returns>Найденное выражение</returns>
	public SExpression ValueObject(string requestString)
	{
		return new SExpression(ValueString(requestString), this.bIgnoreCase);
	}

	#endregion

	#region Initialization

	private void Init(string requestString)
	{
		if (bIgnoreCase)
			requestString = requestString.ToLower();

		request = requestString.Split('.');
		nestLevel = 0;
	}

	#endregion

	#region Parser

	private string ProcessForString(string requestString, bool bExtractValue)
	{
		if (requestString == null)
			requestString = string.Empty;
		else
			requestString = requestString.Trim();

		if (requestString.Length == 0)
			return string.Empty;

		if (Find(requestString))
		{
			if (bExtractValue)
				return ExtractValue(resultPart).String(expression);
			else
				return resultPart.String(expression);
		}
		else
			return string.Empty;
	}

	/// <summary>
	/// Не исчерпан ли лимит вложенности в запросе
	/// </summary>
	private bool CanGoToNest
	{
		get
		{
			return nestLevel < request.Length - 1;
		}
	}

	private bool Find(string requestString)
	{
		Init(requestString);

		int startPos = 0;
		SkipSpaces(ref startPos);
		int lastPos = expression.Length;

		if (expression.Length == 0)
			return false;

		if (expression[startPos] != '(')
			throw new ArgumentException("Expression must begin from '('.");

		// Делаем в цикле на случай наличия нескольких корневых списков
		while (startPos < lastPos - 1)
		{
			int endPos;

			if (expression[startPos] == '(')
				endPos = GoAfterCorrespondedBracket(startPos);
			else
			{
				endPos = startPos;
				SkipAtom(ref endPos);
			}

			bool bRes = ProcessExpression(startPos);
			if (bRes)
				return true;
			else
			{
				startPos = endPos;
				SkipSpaces(ref startPos);
			}
		}

		return false;
	}

	bool ProcessExpression(int exprCurrPos)
	{
		SkipSpaces(ref exprCurrPos);
		int exprStart = exprCurrPos;

		if (expression[exprStart] == '(')
		{
			// S-expression
			int exprEnd = GoAfterCorrespondedBracket(exprStart);
			exprCurrPos++;
			SkipSpaces(ref exprCurrPos);
			Part atom = ReadAtom(ref exprCurrPos);
			string requestAtLevel = request[nestLevel];

			if (atom.IsEqual( expression, requestAtLevel, bIgnoreCase) || requestAtLevel == "_")
			{
				// Совпадение текущеей части запроса текущего уровня и прочитанного атома
				if (CanGoToNest)
				{
					// Если ещё не все части запроса обработаны
					nestLevel++;
					while (exprCurrPos < exprEnd - 1)
					{
						bool bRes = ProcessExpression(exprCurrPos);
						if (bRes)
							return true;
						else
						{
							SkipSpaces(ref exprCurrPos);
							if (expression[exprCurrPos] == '(')
							{
								// Если далее список, то готовимся его разобрать
								exprCurrPos = GoAfterCorrespondedBracket(exprCurrPos);
							}
							else
								return false; // Далее нет уровней вложенности
						}
					}
					nestLevel--;
				}
				else
				{
					// Все части запроса обработаны, полное совпадение. Готовим результат.
					resultPart = new Part(exprStart, exprEnd);
					return true;
				}
			}
		}
		return false;
	}

	private Part ExtractValue(Part part)
	{
		int startPos = part.start;
		startPos++;
		SkipSpaces(ref startPos);
		SkipAtom(ref startPos);
		if (expression[startPos] != '(')
			startPos++;
		return new Part(startPos, part.end - 1);
	}

	bool IsInRange(int pos)
	{
		return pos < expression.Length;
	}

	int GoAfterCorrespondedBracket(int startPos)
	{
		if (expression[startPos] != '(')
			throw new ArgumentException("GoAfterCorrespondedBracket");

		int bracketCounter = 0;
		int pos = startPos;
		while (IsInRange(pos))
		{
			char ch = expression[pos];
			if (ch == '(')
				bracketCounter++;
			if (ch == ')')
				bracketCounter--;
			pos++;

			if (bracketCounter == 0)
				break;
		}

		if (bracketCounter != 0)
			throw new ApplicationException("Can't find corresponded bracket for start pos " + startPos.ToString());

		return pos;
	}

	void SkipSpaces(ref int pos)
	{
		while (IsInRange(pos) && char.IsWhiteSpace(expression[pos]))
			pos++;
	}

	Part ReadAtom(ref int pos)
	{
		int startPos = pos;
		SkipAtom(ref pos);
		return new Part(startPos, pos);
	}

	void SkipAtom(ref int pos)
	{
		while (IsInRange(pos) && char.IsLetterOrDigit(expression[pos]))
			pos++;
	}

	#endregion

	#region Overrides

	public override string ToString()
	{
		return this.expression;
	}

	public override int GetHashCode()
	{
		return expression.GetHashCode();
	}

	#endregion

	#region Clone

	/// <summary>
	/// Клонирование
	/// </summary>
	public SExpression Clone()
	{
		return new SExpression(this.Value, this.bIgnoreCase);
	}

	#endregion

	#region Выделение частей

	/// <summary>
	/// Объект содержит только атом
	/// </summary>
	public bool IsAtom
	{
		get
		{
			foreach (char ch in expression)
			{
				if (ch == '(' || ch == ' ')
					return false;
			}
			return true;
		}
	}

	/// <summary>
	/// Объект содержит пустое выражение
	/// </summary>
	public bool IsEmpty
	{
		get
		{
			int pos = 0;
			SkipSpaces(ref pos);
			return !IsInRange(pos);
		}
	}

	/// <summary>
	/// Составные части выражения (пробел - разделитель, то, что в скобках (список) - как единая часть).
	/// </summary>
	public string[] Parts
	{
		get
		{
			TrySplitExpression();
			return this.expressionParts;
		}
	}

	/// <summary>
	/// Возвращает часть в виде строки по индексу  (пробел - разделитель, то, что в скобках (список) - как единая часть).
	/// </summary>
	public string PartString(int idx)
	{
		return Parts[idx];
	}

	/// <summary>
	/// Возвращает часть в виде объекта по индексу (пробел - разделитель, то, что в скобках (список) - как единая часть).
	/// </summary>
	public SExpression PartExpression(int idx)
	{
		return new SExpression(PartString(idx), this.bIgnoreCase);
	}

	/// <summary>
	/// Количество частей (пробел - разделитель, то, что в скобках (список) - как единая часть).
	/// </summary>
	public int Count
	{
		get
		{
			TrySplitExpression();
			return this.expressionParts.Length;
		}
	}

	/// <summary>
	/// Если список, то возвращает содержимое списка в виде объекта (т.е. без учёта скобок)
	/// </summary>
	public SExpression CleanBrackets()
	{
		SExpression result = null;
		if (expression.Length == 0)
			result = new SExpression(string.Empty, this.bIgnoreCase);
		else
		{
			int start = 0;
			SkipSpaces(ref start);
			if (expression[start] == '(')
			{
				int end = GoAfterCorrespondedBracket(start);
				// Убираем скобки
				start++;
				end--;
				if (start >= end)
					result = new SExpression(string.Empty, this.bIgnoreCase);
				else
					result = new SExpression(expression.Substring(start, end - start), this.bIgnoreCase);
			}
			else
				result = this;
		}
		return result;
	}

	private void TrySplitExpression()
	{
		if (this.expressionParts == null)
			SplitExpressionToParts();
	}

	private void SplitExpressionToParts()
	{
		List<string> parts = new List<string>();
		int end = 0;
		while (IsInRange(end))
		{
			SkipSpaces(ref end);
			int start = end;
			if (IsInRange(end) && expression[end] == '(')
				end = GoAfterCorrespondedBracket(end);
			else
				GoToNextSpace(ref end);

			parts.Add(expression.Substring(start, end - start));
		}

		this.expressionParts = parts.ToArray();
	}

	private void GoToNextSpace(ref int pos)
	{
		while (IsInRange(pos) && (!char.IsWhiteSpace(expression[pos])))
			pos++;
	}

	#endregion

}