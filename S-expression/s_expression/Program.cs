﻿using System;

namespace s_expression
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				//string strExp = "(1 2 3 4 5)";
				//SExpression exp = new SExpression(strExp);
				//SExpression exp2 = exp.CleanBrackets();

				//Console.WriteLine(exp2.PartExpression(0).CleanBrackets());

				Test1();
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);
			}
		}

		private static void Test1()
		{
			//string expression = @"(book (file pushkin.ps) qq (title Избранное) (author А.С. Пушкин) (publisher Просвещение) (address (city Москва)) (year 1997) (color 12:44:200) (coord 44:N;55:W;12) (nested (book (file pushkin.ps) qq (title Избранное) (author А.С. Пушкин) (publisher Просвещение) (address (city Москва)) (year 1997) (color 12:44:200) (coord 44:N;55:W;12))qw))";
			//string expression = @"(book(file pushkin.ps)(title Избранное)(author А.С. Пушкин)(publisher Просвещение)(address(city Москва))(year 1997)(color 12:44:200)(coord 44:N;55:W;12) qq)";
			string expression = @"(book (file pushkin.ps) (title Избранное) (author А.С. Пушкин) (publisher Просвещение) (address (city Москва) (street Железобетонная ул.)) (year 1997) (color 12:44:200) (coord 44:N;55:W;12) qq)";
			//string expression = @"((file pushkin.ps) (title Избранное) (author А.С. Пушкин) (publisher Просвещение) (address (city Москва) (street Железобетонная ул.)) (year 1997) (color 12:44:200) (coord 44:N;55:W;12) qq)";
			//string expression = @"(root (node1 (subnode1 value1) (subnode2 value2)) (node2 value of node2))";

			//string expression = "(1 2 3 4 5 6 7 8 9)";

//                string expression = @"
//
//(root 
//	(node1 
//		(subnode1 value2)
//		(subnode2 value2))
//	(node2 value of node2))
//
//(root2
//	(node1 
//		(subnode1 value2_2)
//		(subnode2 value2_2))
//	(node2 value of node2_2))
//
//";

			SExpression expr = new SExpression(expression, true);
			Console.WriteLine(expr.Value);

			//Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			//SExpression expr2 = expr.ValueObject("_.address");
			//Console.WriteLine(expr2.Value);

			//Console.WriteLine(expr2.Count);
			//foreach(string part in expr2.Parts)
			//{
			//    Console.WriteLine(part);
			//}
			//Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");


			//Console.WriteLine(expr.ValueObject("_").PartExpression(1).ValueObject("_").Value);

			Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			Console.WriteLine("~~~~~ Про Пушкина А.С. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			SExpression exprBook = expr.ValueObject("book");
			Console.WriteLine(exprBook.ValueString("Author"));
			Console.WriteLine(exprBook.ValueString("title"));
			Console.WriteLine(exprBook.ValueString("publisher"));
			Console.WriteLine(exprBook.ValueString("address"));
			Console.WriteLine(exprBook.ValueString("address.city"));
			SExpression streetExpr = exprBook.ValueObject("address.street");
			Console.WriteLine(streetExpr.Value);
			foreach(string part in streetExpr.Parts)
			{
				Console.WriteLine("  - " + part);
			}
			Console.WriteLine(exprBook.ValueString("year"));
			Console.WriteLine(exprBook.ValueString("file"));
			Console.WriteLine(exprBook.ValueString("color"));
			Console.WriteLine(exprBook.ValueString("coord"));
			Console.WriteLine(exprBook.ValueString("nested"));
			Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

			Console.WriteLine("Дерево:");
			PrintNode(expr, 0);

			//Write(expr, "_.title");
			//Write(expr, "root2.node2");

			//Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

			//Console.WriteLine(expr.ValueObject("root").ValueObject("node1").ValueObject("subnode2"));
		}

		private static void PrintNode(SExpression expr, int level)
		{
			if(expr.IsAtom)
			{
				Console.WriteLine(new string('\t', level-1) + expr.Value);
			}
			else
			{
				foreach(string part in expr.Parts)
				{
					PrintNode(new SExpression(part).CleanBrackets(), level + 1);
				}
			}
		}

		private static void Write(SExpression sExpression, string request)
		{
			Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			Console.WriteLine(sExpression.ExprString(request));

			Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			Console.WriteLine(sExpression.ValueString(request));
		}
	}
}
