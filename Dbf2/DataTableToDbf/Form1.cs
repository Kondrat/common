using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace DataTableToDbf
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.DataGrid dg;
		private System.Data.DataSet ds;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Data.SqlClient.SqlCommand cmdCount;
		private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
		private System.Data.SqlClient.SqlConnection sqlConnection1;
		private System.Data.SqlClient.SqlDataAdapter daBu;
		private System.Windows.Forms.Button button4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.button1 = new System.Windows.Forms.Button();
			this.dg = new System.Windows.Forms.DataGrid();
			this.ds = new System.Data.DataSet();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.cmdCount = new System.Data.SqlClient.SqlCommand();
			this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
			this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
			this.daBu = new System.Data.SqlClient.SqlDataAdapter();
			this.button4 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(636, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Close";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// dg
			// 
			this.dg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.dg.DataMember = "";
			this.dg.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dg.Location = new System.Drawing.Point(4, 4);
			this.dg.Name = "dg";
			this.dg.ReadOnly = true;
			this.dg.Size = new System.Drawing.Size(624, 428);
			this.dg.TabIndex = 1;
			// 
			// ds
			// 
			this.ds.DataSetName = "NewDataSet";
			this.ds.Locale = new System.Globalization.CultureInfo("ru-RU");
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.Location = new System.Drawing.Point(636, 56);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 2;
			this.button2.Text = "DataTable";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button3.Location = new System.Drawing.Point(636, 96);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 2;
			this.button3.Text = "DataReader";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// cmdCount
			// 
			this.cmdCount.CommandText = "SELECT 10000 AS Expr1";
			this.cmdCount.Connection = this.sqlConnection1;
			// 
			// sqlConnection1
			// 
			this.sqlConnection1.ConnectionString = "Data Source=mbpsql2k;Initial Catalog=ArchiveSystem;Integrated Security=True;Persi" +
				"st Security Info=False;Packet Size=4096;Workstation ID=83-DMITRYS";
			this.sqlConnection1.FireInfoMessageEventOnUserErrors = false;
			// 
			// sqlSelectCommand1
			// 
			this.sqlSelectCommand1.CommandText = resources.GetString("sqlSelectCommand1.CommandText");
			this.sqlSelectCommand1.Connection = this.sqlConnection1;
			// 
			// daBu
			// 
			this.daBu.SelectCommand = this.sqlSelectCommand1;
			this.daBu.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "BatchesItems", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("BatchItemID", "BatchItemID"),
                        new System.Data.Common.DataColumnMapping("is_batch", "is_batch"),
                        new System.Data.Common.DataColumnMapping("ProdID", "ProdID"),
                        new System.Data.Common.DataColumnMapping("BatchItemNumber", "BatchItemNumber"),
                        new System.Data.Common.DataColumnMapping("InBatchID", "InBatchID"),
                        new System.Data.Common.DataColumnMapping("CreateOrReceive", "CreateOrReceive"),
                        new System.Data.Common.DataColumnMapping("DateCreate", "DateCreate"),
                        new System.Data.Common.DataColumnMapping("BatchCount", "BatchCount"),
                        new System.Data.Common.DataColumnMapping("BatchCount2", "BatchCount2"),
                        new System.Data.Common.DataColumnMapping("BatchCountFree", "BatchCountFree"),
                        new System.Data.Common.DataColumnMapping("BatchCountFree2", "BatchCountFree2"),
                        new System.Data.Common.DataColumnMapping("BatchItemStatus", "BatchItemStatus"),
                        new System.Data.Common.DataColumnMapping("passportNumber", "passportNumber"),
                        new System.Data.Common.DataColumnMapping("materialDocNumber", "materialDocNumber")})});
			// 
			// button4
			// 
			this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button4.Location = new System.Drawing.Point(636, 140);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 2;
			this.button4.Text = "Direct";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(716, 441);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.dg);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button4);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DataTableToDbf";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.dg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			daBu.Fill( ds);
			dg.DataSource = ds.Tables[0];
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			button2.Enabled = false;

			Dbf.Dbf d = new Dbf.Dbf( ds.Tables[0]);

			//d.CreateColumns( ds.Tables[0]);
			d.CreateColumns( "Bid:N20,0; is_batch:N20,0; ProdID:N20,0; bin:C13; InBatchID:N20,0; cor:N20,0; DateCreate:D; bc1:F20,3; bc2:F20,3; bcf1:F20,3; bcf2:F20,3; bis:N20; passp:C9; mDoc:C8");

			d.Columns[3].Length = 3;

			d.WriteTo( "DataTable.dbf");

			button2.Enabled = true;
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			button3.Enabled = false;

			sqlConnection1.Open();
			using( sqlConnection1)
			{
				int cnt = (int)cmdCount.ExecuteScalar();

				IDataReader rdr = daBu.SelectCommand.ExecuteReader();

				Dbf.Dbf d = new Dbf.Dbf( rdr, cnt);

				//d.CreateColumns( rdr);
				d.CreateColumns( "Bid:N20,0; is_batch:N20,0; ProdID:N20,0; bin:C13; InBatchID:N20,0; cor:N20,0; DateCreate:D; bc1:F20,3; bc2:F20,3; bcf1:F20,3; bcf2:F20,3; bis:N20; passp:C9; mDoc:C8");

				d.WriteTo("DataReader.dbf");

				rdr.Close();

				sqlConnection1.Close();
			}
			button3.Enabled = true;
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			button4.Enabled = false;
			int rowCount = 10000;
			Random rnd = new Random( DateTime.Now.Millisecond);
			string msg = "������ ����� � ����!";

			System.IO.FileStream fs = new System.IO.FileStream( "direct.dbf", System.IO.FileMode.Create, System.IO.FileAccess.Write);
			System.IO.BufferedStream bs = new System.IO.BufferedStream( fs, 3000);

			Dbf.Dbf d = new Dbf.Dbf( "col1:N5; col2:C50; pi:F15,6");
			d.BeginDirectWrite( bs, rowCount);
			for( int i=0; i< rowCount; i++)
			{
				d.DirectWriteRow( rnd.Next( 1000), msg.Substring( 0, rnd.Next(msg.Length)+1), rnd.Next(0,2)==1? Math.PI: Math.E);
			}
			d.EndDirectWrite();

			fs.Close();
			button4.Enabled = true;
		}
	}
}
