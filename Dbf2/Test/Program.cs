﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			//Write();
			Read();
		}

		private static void Read()
		{
			const string filename = @"d:\Work Project 2\Dbf2\&TestData\F9040108_test.dbf";
			//const string filename = @"d:\Work Project 2\Dbf2\&TestData\f9040254.dbf";
			//const string filename = @"d:\Work Project 2\Dbf2\&TestData\res.dbf";

			var reader = Dbf.Dbf.Read( filename );
			using(reader)
			{
				var it1 = reader.GetIterator(false);
				var res1 = it1.ToArray();

				var it2 = reader.GetTypedIterator( row => row[0] as string, true );
				var res2 = it2.ToArray();
			}
		}

		private static void Write()
		{
			var dbf = new Dbf.Dbf("col1:N5; col2:C50; pi:F15,6; logical:L; date:D");
			dbf.FileType = 0x03;

			dbf.BeginDirectWrite(@"d:\Work Project 2\Dbf2\&TestData\res.dbf");

			dbf.DirectWriteRow( 10, "12334", 11.23, false, DateTime.Today );
			dbf.DirectWriteRow( 55, "43210", 22.43, true, DateTime.Today.AddMonths( 2 ) );

			dbf.EndDirectWrite(2);
		}
	}
}
