using System;

namespace Dbf
{
	internal static class Helper
	{
		public static void WriteNullBytes( System.IO.BinaryWriter bw, int count)
		{
			bw.Write( new byte[count] );
		}

		private static System.Text.Encoding cp866 = System.Text.Encoding.GetEncoding(Dbf.cnstDefCodePage);
		public static byte[] WriteStringToBuffer( string str, byte buffSize)
		{
			lock( cp866)
			{
				byte[] res = new byte[buffSize];
				byte[] s = cp866.GetBytes( str);
				Array.Copy( s, res, Math.Min( res.Length, s.Length));
				return res;
			}
		}

		public static void WriteStringBufferToStream( System.IO.BinaryWriter bw, string str, byte buffSize)
		{
			byte[] bytes = WriteStringToBuffer( str, buffSize);
			bw.Write( bytes);
		}
		
		public static byte YearToByte(int year)
		{
			return (byte)(year % 100);
		}
	}
}
