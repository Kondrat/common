using System;
using System.IO;
using System.Text;


namespace Dbf
{
	/// <summary>
	/// ������ ������
	/// </summary>
	public class Row
	{
		readonly object[] data;
		readonly Column[] columns;

		bool isDeleted = false;

		public bool IsDeleted
		{
			get { return this.isDeleted;  }
			set { this.isDeleted = value; }
		}

		internal Row( Column[] columns )
		{
			this.columns = columns;
			data = new object[ this.columns.Length];
		}

		public object[] GetData()
		{
			return this.data;
		}

		[System.Runtime.CompilerServices.IndexerName("Data")]
		public object this[int idx]
		{
			get { return data[idx];}
			set
			{
				if( !IsTypeOfDataValid( value, this.columns[idx].Type))
				{
					string msg = string.Format(
						"��� ������� �� ������������� ���� ������������� ������. colIndex = {0}, �������������: '{1}', �������: '{2}'", 
						idx, 
						value != null? value.GetType().FullName: "<null>", 
						this.columns[idx].Type
						);

					throw new ArgumentException(msg);
				}
				data[idx] = value;
			}
		}

		private bool IsTypeOfDataValid( object value, DbfColumnType type)
		{
			if( IsNull(value))
				return true;

			switch( type)
			{
				case DbfColumnType.C: 
					return value.GetType() == typeof(string);

				case DbfColumnType.D: 
					return value.GetType() == typeof(DateTime);

				case DbfColumnType.F: 
					return value.GetType() == typeof(float) ||
						   value.GetType() == typeof(double);

				case DbfColumnType.L: 
					return value.GetType() == typeof(bool);

				case DbfColumnType.N: 
					return value.GetType() == typeof(int) ||
						   value.GetType() == typeof(short) ||
						   value.GetType() == typeof(ushort) ||
						   value.GetType() == typeof(long) ||
						   value.GetType() == typeof(byte) ||
						   value.GetType() == typeof(sbyte) ||
						   value.GetType() == typeof(uint);
				default:
					throw new ApplicationException();
			}
		}

		internal void Write( System.IO.Stream ostr)
		{
			 // �� ������ ��� NULL, (�������� � DBF ��� � ������� ����������� � ����� ��������� ��������)
			System.IO.StreamWriter wrt = new System.IO.StreamWriter(ostr, System.Text.Encoding.GetEncoding(Dbf.cnstDefCodePage));

			// ������� ��������
			if( this.isDeleted)
				wrt.Write('*');
			else
				wrt.Write(' ');

			for( int i=0; i<this.columns.Length; i++)
			{
				WriteField( this[i], this.columns[i], wrt);
			}
			wrt.Flush();
		}

		private bool IsNull(object fieldData)
		{
			return fieldData == Convert.DBNull || fieldData == null;
		}

		private void WriteField( object fieldData, Column col, System.IO.StreamWriter wrt)
		{
			switch( col.Type)
			{
				case DbfColumnType.C: 
				{
					wrt.Flush();

					string sVal = string.Empty;
					if( ! IsNull(fieldData))
						sVal = fieldData as string;

					if( sVal.Length < col.Length)
						sVal += new string( ' ', col.Length-sVal.Length);

					byte[] buffer = Helper.WriteStringToBuffer( sVal, col.Length);
					wrt.BaseStream.Write( buffer, 0, buffer.Length);
				}
					break;

				case DbfColumnType.D: 
				{
					if(IsNull(fieldData))
					{
						wrt.Write( RawNullValue(col));
						return;
					}

					DateTime dt = (DateTime)fieldData;
					wrt.Write( dt.ToString("yyyyMMdd"));
				}
					break;

				case DbfColumnType.F: 
				{
					if(!IsNull(fieldData))
					{
						double dVal = Convert.ToDouble(fieldData);

						string format = "{0," + col.Length.ToString() + ":0." + new string('0', col.Decimals) + "}";

						System.Globalization.NumberFormatInfo nfi =
							System.Globalization.CultureInfo.CurrentCulture.NumberFormat.Clone() as System.Globalization.NumberFormatInfo;
						nfi.NumberDecimalSeparator = ".";

						string res = string.Format(nfi, format, dVal);
						wrt.Write(res);
					}
					else 
						wrt.Write(RawNullValue(col));
				}
					break;

				case DbfColumnType.L: 
				{
					if( IsNull(fieldData))
					{
						wrt.Write( RawNullValue(col));
						return;
					}
					bool b = (bool)fieldData;
					wrt.Write( b? 'T': 'F');
				}
					break;

				case DbfColumnType.N: 
				{
					if( !IsNull(fieldData))
					{
						long nVal = Convert.ToInt64(fieldData);

						string format = "{0," + col.Length.ToString() + "}";
						string res = string.Format(format, nVal);
						wrt.Write(res);
					}
					else 
						wrt.Write(RawNullValue(col));
				}
					break;
			}
		}

		private static string RawNullValue(Column col)
		{
			return new string(' ', col.Length);
		}

		private static bool IsRawNullValue(string strVal)
		{
			if( strVal == null)
				return true;

			for(int idx = 0; idx < strVal.Length; idx++)
			{
				if(strVal[idx] != ' ')
					return false;
			}

			return true;
		}

		internal static Row Read(Stream istr, DbfHeader header, Encoding encoding)
		{
			var row = new Row(header.Columns);
			byte[] rowBuf = new byte[header.RecordLength];

			istr.Read( rowBuf, 0, header.RecordLength );

			if(rowBuf[0] == '*')
				row.IsDeleted = true;

			for(int colIdx = 0; colIdx < header.Columns.Length; colIdx++)
			{
				Column column = header.Columns[colIdx];
				object oValue = null;

				string strVal = encoding.GetString( rowBuf, column.Offset, column.Length );

				switch(column.Type)
				{
					case DbfColumnType.C:
						oValue = strVal.Trim();
						break;
					case DbfColumnType.N:
					{
						if(IsRawNullValue(strVal))
							oValue = null;
						else
						{
							int iVal = int.Parse(strVal);
							oValue = iVal;
						}
					}
						break;
					case DbfColumnType.L:
					{
						if(IsRawNullValue(strVal))
							oValue = null;
						else
						{
							oValue = rowBuf[column.Offset] == 'T';
						}
					}
						break;
					case DbfColumnType.D:
					{
						if(IsRawNullValue(strVal))
							oValue = null;
						else
						{
							DateTime dtVal = DateTime.ParseExact(strVal, "yyyyMMdd", null);
							oValue = dtVal;
						}
					}
						break;
					case DbfColumnType.F:
					{
						if(IsRawNullValue(strVal))
							oValue = null;
						else
						{
							System.Globalization.NumberFormatInfo nfi =
								System.Globalization.CultureInfo.CurrentCulture.NumberFormat.Clone() as System.Globalization.NumberFormatInfo;
							nfi.NumberDecimalSeparator = ".";

							double dVal = double.Parse(strVal, nfi);
							oValue = dVal;
						}
					}
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				row[colIdx] = oValue;
			}

			return row;
		}
	}
}
