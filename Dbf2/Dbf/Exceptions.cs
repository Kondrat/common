using System;

namespace Dbf
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ColumnCreatedException: Exception
	{
		public ColumnCreatedException()
		{
		}

		public ColumnCreatedException( string message): base(message)
		{
		}

	}
}
