using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Dbf
{
	/// <summary>
	/// ��������� DBF
	/// </summary>
	internal sealed class DbfHeader
	{
		#region FileType

		internal byte fileType = 0x03; /*  FoxPro / dBase IV ��� ���� */

		/// <summary>
		/// ��� �����
		/// </summary>
		public byte FileType
		{
			get
			{
				return fileType;
			}
			set
			{
				fileType = value;
			}
		}

		#endregion

		/// <summary>
		/// ���� ���������� ���������
		/// </summary>
		internal DateTime lastChangeDate = DateTime.Today;
	
		/// <summary>
		/// ���-�� ������� � �����
		/// </summary>
		internal uint recordCount =0;

		/// <summary>
		/// ����� ����� ������ � ������� (������� ������� ��������)
		/// </summary>
		internal ushort RecordLength
		{
			get
			{
				ushort len =1; // ��������� ������� �������� ������
				foreach( Column c in this.columns)
					len += c.Length;
				return len;
			}
		}

		/// <summary>
		/// ������� ���������� �����
		/// </summary>
		internal byte indexFilePrezent = 0; // ��� ���

		private readonly List<Column> columns = new List<Column>();
		/// <summary>
		/// ������� � DBF
		/// </summary>
		internal Column[] Columns
		{
			get
			{
				return this.columns.ToArray();
			}
		}

		internal void AddColumn(Column col)
		{
			if( IsColumnNamePrezent( col.Name))
				throw new ArgumentException("������� � ������ '"+col.Name+"' ��� ������� � ����������� DBF �����.");
			this.columns.Add( col);
		}

		internal void DeleteColumn( int pos)
		{
			this.columns.RemoveAt( pos);
		}

		bool IsColumnNamePrezent(string name)
		{
			foreach( Column c in this.columns)
			{
				if( c.Name == name)
					return true;
			}
			return false;
		}

		const int cnstIgnoredArea1 = 27 - 12 + 1;
		const int cnstIgnoredArea2 = 31 - 29 + 1;
		const byte cnstEndOfMetadata = 0x0D;

		internal void Write( System.IO.Stream ostr)
		{
			System.IO.MemoryStream ms = new System.IO.MemoryStream();
			System.IO.BinaryWriter bw = new System.IO.BinaryWriter( ms);

			// 00
			bw.Write( this.fileType);
	
			// 01-03
			bw.Write( Helper.YearToByte( this.lastChangeDate.Year));
			bw.Write( (byte)this.lastChangeDate.Month);
			bw.Write( (byte)this.lastChangeDate.Day);
			
//			// 04-07
			bw.Write( this.recordCount);
			
//			// 08-09
			Helper.WriteNullBytes( bw, 2);

//			// 10-11
			bw.Write( this.RecordLength);

			// 12-27
			Helper.WriteNullBytes( bw, cnstIgnoredArea1);

			// 28
			bw.Write( this.indexFilePrezent);

			// 29-31
			Helper.WriteNullBytes( bw, cnstIgnoredArea2);

			bw.Flush();

			// 32-n  ������ �����
			foreach( Column c in this.columns)
				c.Write( ms);

			// n+1 ������� ����� ���������
			bw.Write( (byte)cnstEndOfMetadata);

			// ������������ �������� ������ ������
			ms.Position = 8;
			ushort offset = (ushort)(ms.Length);
			bw.Write( offset);

			bw.Flush();
			ms.WriteTo( ostr);
		}

		internal static DbfHeader Read(Stream istr)
		{
			System.IO.BinaryReader br = new BinaryReader( istr );
			DbfHeader header = new DbfHeader();

			// 00
			header.fileType = br.ReadByte();

			// 01-03
			byte year = br.ReadByte();
			while(year > 100)
				year -= 100;

			byte month = br.ReadByte();
			byte day = br.ReadByte();

			string strDate = string.Format( "{0}.{1}.{2}", year, month, day );
			header.lastChangeDate = DateTime.ParseExact(strDate, "y.M.d", null, DateTimeStyles.AssumeLocal);

			// 04-07
			header.recordCount = br.ReadUInt32();

			// 08-09
			br.ReadInt16(); // Skip 2 bytes - header size

			// 10-11
			ushort recordLength = br.ReadUInt16();

			// 12-27
			br.BaseStream.Position += cnstIgnoredArea1; // Skip area

			// 28
			header.indexFilePrezent = br.ReadByte();

			// 29-31
			br.BaseStream.Position += cnstIgnoredArea2;

			// read columns
			while(br.PeekChar() != cnstEndOfMetadata )
			{
				Column column = Column.Read( br );
				header.AddColumn( column );
			}

			// � ��������� ������ �������� ������ � ������� ����� ���� �� �������, ������� ����������� �������� ����.
			CalculateColumnOffsets(header.columns);

			return header;
		}

		private static void CalculateColumnOffsets(IEnumerable<Column> columns)
		{
			ushort offset = 1;
			foreach(Column column in columns)
			{
				column.SetOffset( offset);
				offset += column.Length;
			}
		}
	}
}
