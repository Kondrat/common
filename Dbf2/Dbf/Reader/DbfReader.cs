﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Dbf.Reader
{
	public class DbfReader :IDisposable
	{
		private readonly Stream istr;
		private readonly Encoding encoding;
		private readonly DbfHeader header = null;

		internal DbfReader(Stream istr, Encoding encoding)
		{
			this.istr = istr;
			this.encoding = encoding;

			this.header = DbfHeader.Read( this.istr );
		}

		public Column[] Columns
		{
			get
			{
				return this.header.Columns;
			}
		}

		private void SetPositionToStart()
		{
			istr.Position = /*заголовок*/ 32 + /*описание колонок*/ 32 * this.header.Columns.Length + /*завершающий символ*/1;
		}

		public IEnumerable<Row> GetIterator(bool bSkipDeleted)
		{
			SetPositionToStart();
			int rowCount = 0;

			while(rowCount < this.header.recordCount)
			{
				Row row = Row.Read( this.istr, this.header, this.encoding );

				rowCount++;

				if( bSkipDeleted && row.IsDeleted)
					continue;
				
				yield return row;
			}
		}

		public IEnumerable<TRow> GetTypedIterator<TRow>(Func<Row, TRow> converter, bool bSkipDeleted)
		{
			if(converter == null)
				throw new ArgumentNullException( "converter" );

			foreach(Row row in this.GetIterator( bSkipDeleted ))
			{
				yield return converter( row );
			}
		}

		#region Implementation of IDisposable

		public void Dispose()
		{
			istr.Dispose();
		}

		#endregion
	}
}
