using System;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Dbf.Reader;

namespace Dbf
{



	/// <summary>
	/// ����� ��� ������ ������ DBF
	/// </summary>
	public class Dbf
	{
		internal const int cnstDefCodePage = 866;

		/// <summary>
		/// �������� ������ ��� ��������
		/// </summary>
		internal enum DataSourceType
		{
			DataTable,
			DataReader,
			Direct
		}

		#region Variables

		readonly DataSourceType srcType;

		readonly DataTable srcTable;

		readonly IDataReader srcReader;

		readonly DbfHeader header = new DbfHeader();

		private bool bNeedWriteCountOfRecordsOnEndDirectWrite = false;

		#endregion	// Variables

		#region Constructors

		/// <summary>
		/// �������� ������� �� �������
		/// </summary>
		public Dbf( string columnTemplate)
		{
			this.srcType = DataSourceType.Direct;
			this.CreateColumns( columnTemplate);
		}

		public Dbf( DataTable table)
		{
			this.srcReader = null;
			this.srcTable = table;
			this.srcType = DataSourceType.DataTable;
			this.header.recordCount = Convert.ToUInt32(table.Rows.Count);
			this.CreateColumns( table);
		}

		public Dbf( IDataReader dr, int recordCount)
		{
			this.srcTable = null;
			this.srcReader = dr;
			this.srcType = DataSourceType.DataReader;
			this.header.recordCount = Convert.ToUInt32(recordCount);
			this.CreateColumns( dr);
		}
		#endregion // Consructors

		#region FileType

		public byte FileType
		{
			get
			{
				return header.FileType;
			}
			set
			{
				header.FileType = value;
			}
		}

		#endregion

		#region DirectWrite
		public void BeginDirectWrite( string filename, int recCount)
		{
			FileStream fs = new FileStream( filename, FileMode.Create, FileAccess.Write);
			this.BeginDirectWrite( fs, recCount);
			autoCloseDirectOstr = true;
		}

		public void BeginDirectWrite( string filename)
		{
			FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
			this.BeginDirectWrite( fs);
			autoCloseDirectOstr = true;
		}

		System.IO.Stream directOstr = null;
		bool autoCloseDirectOstr = false;

		public void BeginDirectWrite( System.IO.Stream ostr)
		{
			if(!ostr.CanSeek)
				throw new InvalidOperationException("��� ����������� �������� ���-�� ������� � ����� �������� ������ ����������, ����� �������� ����� ����������� ����������������.");

			bNeedWriteCountOfRecordsOnEndDirectWrite = true;
			BeginDirectWrite(ostr, 0);
		}

		public void BeginDirectWrite( System.IO.Stream ostr, int recCount)
		{
			this.header.recordCount = Convert.ToUInt32(recCount);
			directOstr = ostr;
			autoCloseDirectOstr = false;
			this.header.Write( directOstr);
		}

		private void TestForDirectMode()
		{
			if( this.srcType != DataSourceType.Direct)
				throw new ApplicationException("����� ������ �������� ������ � ���������������� ������.");
		}

		public void DirectWriteRow( params object[] data)
		{
			Row r = this.NewRow( data);
			r.Write( this.directOstr);
		}

		public void EndDirectWrite()
		{
			if(bNeedWriteCountOfRecordsOnEndDirectWrite)
				throw new InvalidOperationException("�.�. � ������ ������ ����� �� ������� ���-�� �������, ���������� ��������������� ������� 'EndDirectWrite(int recCount);'");
			
			EndDirectWrite(0);
		}

		public void EndDirectWrite(int recCount)
		{
			if(directOstr != null)
			{
				WriteEnd(directOstr);

				if(bNeedWriteCountOfRecordsOnEndDirectWrite)
				{
					this.header.recordCount = Convert.ToUInt32(recCount);
					directOstr.Seek(0, SeekOrigin.Begin);
					this.header.Write(directOstr);
				}

				if( autoCloseDirectOstr)
					directOstr.Close();

				directOstr = null;
			}
		}
		#endregion // DirectWrite

		#region Method for write DBF

		public void WriteTo( string filename )
		{
			FileStream fs = new FileStream( filename, FileMode.Create, FileAccess.Write);
			using( fs)
			{
				this.WriteTo( fs);
				fs.Close();
			}
		}

		public void WriteTo( System.IO.Stream ostr )
		{
			this.header.Write( ostr);
			WriteData(ostr);
			WriteEnd(ostr);
		}

		private void WriteEnd( System.IO.Stream ostr)
		{
			ostr.WriteByte( 0x1A );
		}

		#endregion // Method for write DBF

		#region Read

		public static DbfReader Read(Stream istr, Encoding encoding)
		{
			return new DbfReader( istr, encoding );
		}

		public static DbfReader Read(Stream istr)
		{
			return Read( istr, Encoding.GetEncoding( Dbf.cnstDefCodePage ) );
		}

		public static DbfReader Read(string filename)
		{
			FileStream ifs = new FileStream( filename, FileMode.Open, FileAccess.Read);
			return Read(ifs, Encoding.GetEncoding(Dbf.cnstDefCodePage));
		}

		public static DbfReader Read(string filename, Encoding encoding)
		{
			FileStream ifs = new FileStream( filename, FileMode.Open, FileAccess.Read);
			return Read( ifs, encoding );
		}


		#endregion

		#region Initializers
		public void CreateColumns( DataTable table)
		{
			TestForColumnCreated();

			foreach( DataColumn c in table.Columns)
			{
				DbfColumnType type = this.MapNativeDataToDbfData( c.DataType);

				switch( type)
				{
					case DbfColumnType.C:
					{
						int i_maxStringLen = GetMaxStringLengthInDataTable( table, c);
						byte b_maxStringLen = i_maxStringLen>254? (byte)254: (byte)i_maxStringLen;
						if( b_maxStringLen ==0)
							b_maxStringLen =1;
						this.AddStringColumn( c.ColumnName, b_maxStringLen);
					}
						break;
					case DbfColumnType.D:
					{
						this.AddDataColumn( c.ColumnName);
					}
						break;
					case DbfColumnType.F:
					{
						this.AddFloatColumn( c.ColumnName, /*��������*/20, /*�� �����*/3);
					}
						break;
					case DbfColumnType.L:
					{
						this.AddLogicalColumn( c.ColumnName);
					}
						break;
					case DbfColumnType.N:
					{
						this.AddIntegerColumn( c.ColumnName, /*��������*/20);
					}
						break;
				}
			}
		}

		public void CreateColumns( string columnTemplate)
		{
			TestForColumnCreated();

			string[] cols = columnTemplate.Split(';');
			Regex rx = new Regex(@"^(?<name>.+):(?<type>\w)(?<size>\d*)(?:,(?<decimal>\d*)){0,1}$", RegexOptions.IgnoreCase|RegexOptions.Singleline);

			foreach( string ct in cols)
			{
				string templ = ct.Trim();
				Match m = rx.Match( templ);
				string name = m.Groups["name"].Value;
				DbfColumnType type = (DbfColumnType)Enum.Parse( typeof(DbfColumnType), m.Groups["type"].Value);

				switch( type)
				{
					case DbfColumnType.C:
					{
						int i_maxStringLen;
						try
						{
							i_maxStringLen = int.Parse( m.Groups["size"].Value);
						}
						catch( Exception)
						{
							throw new ArgumentException("�������� ������ �������.");
						}

						byte b_maxStringLen = i_maxStringLen>254? (byte)254: (byte)i_maxStringLen;
						if( b_maxStringLen ==0)
							b_maxStringLen =1;
						this.AddStringColumn( name, b_maxStringLen);
					}
						break;
					case DbfColumnType.D:
					{
						this.AddDataColumn( name);
					}
						break;
					case DbfColumnType.F:
					{
						int len, dec;
						try
						{
							len = int.Parse( m.Groups["size"].Value);
							dec = int.Parse( m.Groups["decimal"].Value);
						}
						catch( Exception)
						{
							throw new ArgumentException("�������� ������ �������.");
						}

						if( len <1 || len > 20)
							throw new ArgumentException("����� ������������� ���� �� ������ ��������� 20 ��� ���� ������ 1.");
						
						if( dec <0 || dec > 20)
							throw new ArgumentException("���-�� �������� ����� ������� � ������������ ���� �� ������ ��������� 6 ��� ���� ������ 0.");

						this.AddFloatColumn( name, (byte)len, (byte)dec);
					}
						break;
					case DbfColumnType.L:
					{
						this.AddLogicalColumn( name);
					}
						break;
					case DbfColumnType.N:
					{
						int len;
						try
						{
							len = int.Parse( m.Groups["size"].Value);
						}
						catch( Exception)
						{
							throw new ArgumentException("�������� ������ �������.");
						}

						if( len <1 || len > 20)
							throw new ArgumentException("����� �������������� ���� �� ������ ��������� 20 ��� ���� ������ 1.");

						this.AddIntegerColumn( name, (byte)len);
					}
						break;
				}
			}
		}


		public void CreateColumns( IDataReader rdr)
		{
			TestForColumnCreated();

			DataTable dt = rdr.GetSchemaTable();
			foreach( DataRow dr in dt.Rows)
			{
				DbfColumnType type = MapNativeDataToDbfData( dr["DataType"] as System.Type);
				string colName = dr["ColumnName"] as string;
				int colSize = (int)dr["ColumnSize"];

				switch( type)
				{
					case DbfColumnType.C:
					{
						int i_maxStringLen = colSize;
						byte b_maxStringLen = i_maxStringLen>254? (byte)254: (byte)i_maxStringLen;
						if( b_maxStringLen ==0)
							b_maxStringLen =1;
						this.AddStringColumn( colName, b_maxStringLen);
					}
						break;
					case DbfColumnType.D:
					{
						this.AddDataColumn( colName);
					}
						break;
					case DbfColumnType.F:
					{
						this.AddFloatColumn( colName, 20, 3);
					}
						break;
					case DbfColumnType.L:
					{
						this.AddLogicalColumn( colName);
					}
						break;
					case DbfColumnType.N:
					{
						this.AddIntegerColumn( colName, 20);
					}
						break;
				}
			}
		}


		/// <summary>
		/// ������� � DBF
		/// </summary>
		public Column[] Columns
		{
			get { return this.header.Columns; }
		}

		/// <summary>
		/// ���������� ������� �� �������
		/// </summary>
		/// <param name="index">������ �������</param>
		/// <returns>�������</returns>
		public Column GetColumn(int index)
		{
			return this.header.Columns[index];
		}

		/// <summary>
		/// ���������� ������� �� �����
		/// </summary>
		/// <param name="name">��� �������</param>
		/// <returns>�������</returns>
		public Column GetColumn(string name)
		{
			foreach( Column c in this.header.Columns)
			{
				if( c.Name == name)
					return c;
			}
			return null;
		}

		public void AddStringColumn( string name, byte length)
		{
			Column c = new Column( name, DbfColumnType.C, length, /*ignored*/ 0, header.RecordLength);
			header.AddColumn( c);
		}

		public void AddDataColumn( string name)
		{
			Column c = new Column( name, DbfColumnType.D, /*ignored*/ 0, /*ignored*/ 0, header.RecordLength);
			header.AddColumn( c);
		}

		public void AddLogicalColumn( string name)
		{
			Column c = new Column( name, DbfColumnType.L, /*ignored*/ 0, /*ignored*/ 0, header.RecordLength);
			header.AddColumn( c);
		}

		public void AddIntegerColumn( string name, byte length)
		{
			Column c = new Column( name, DbfColumnType.N, length, /*ignored*/ 0, header.RecordLength);
			header.AddColumn( c);
		}

		public void AddFloatColumn( string name, byte length, byte decimals)
		{
			Column c = new Column( name, DbfColumnType.F, length, decimals, header.RecordLength);
			header.AddColumn( c);
		}

		/// <summary>
		/// �������� �������
		/// </summary>
		/// <param name="index">������ ��������� �������</param>
		public void DeleteColumn( int index)
		{
			this.header.DeleteColumn( index);
		}

		#endregion //Initializers

		#region Helpers
		private void TestForColumnCreated()
		{
			if( header.Columns.Length >0)
				throw new ColumnCreatedException("������� � DBF ��� �������.");
		}

		private DbfColumnType MapNativeDataToDbfData( System.Type type )
		{
			if( type == typeof( string))
				return DbfColumnType.C;

			if( type == typeof(DateTime))
				return DbfColumnType.D;

			if( type == typeof( float) || 
				type == typeof( double)
				)
				return DbfColumnType.F;	
			
			if( type == typeof( bool))
				return DbfColumnType.L;

			if( type == typeof(int) ||
				type == typeof(short) ||
				type == typeof(ushort) ||
				type == typeof(long) ||
				type == typeof(byte) ||
				type == typeof(sbyte) ||
				type == typeof(uint)
				)
				return DbfColumnType.N;

			throw new ArgumentException( "��� ������ '"+type.FullName+"' �� �������������� ��� �������� � DBF.");
		}

	
		private int GetMaxStringLengthInDataTable( DataTable table, DataColumn col)
		{
			int len =0;
			foreach( DataRow dr in table.Rows)
			{
				string s = dr[col] as string;

				if( s != null)
					len = Math.Max( len, s.Length);
				else
					len = 0;
			}
			return len;
		}

		#endregion // Helpers

		/// <summary>
		/// ���������� ����� ������ ������ � ������ ����� �������
		/// </summary>
		/// <returns>����� ������</returns>
		internal Row NewRow(object[] data)
		{
			Row r = new Row( this.header.Columns);

			int i=0;
			foreach( object d in data)
				r[i++] = d;

			return r;
		}

		private void WriteData( System.IO.Stream ostr)
		{
			switch( this.srcType)
			{
				case DataSourceType.DataReader:
					WriteDataReader( ostr);
					break;
				case DataSourceType.DataTable:
					WriteDataTable( ostr);
					break;
				case DataSourceType.Direct:
					throw new ApplicationException("������ � ���������������� ������ �������������� ������� WriteRow().");
			}
		}

		private void WriteDataTable( System.IO.Stream ostr)
		{
			foreach( DataRow dr in this.srcTable.Rows)
			{
				Row r = this.NewRow( dr.ItemArray);
				r.Write( ostr);
			}
		}

		private void WriteDataReader( System.IO.Stream ostr)
		{
			while( this.srcReader.Read())
			{
				object[] data = new object[ this.Columns.Length];
				this.srcReader.GetValues( data );
				Row r = this.NewRow( data );
				r.Write( ostr);
			}
		}
	}
}
