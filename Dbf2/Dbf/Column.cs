using System;
using System.IO;
using System.Text;

namespace Dbf
{
	/// <summary>
	/// ���� ������� � DBF
	/// </summary>
	public enum DbfColumnType 
	{
		/// <summary>
		/// ����������
		/// </summary>
		C,

		/// <summary>
		/// �������� (�����)
		/// </summary>
		N,

		/// <summary>
		/// ����������
		/// </summary>
		L,

		/// <summary>
		/// ����
		/// </summary>
		//		M,

		/// <summary>
		/// ����
		/// </summary>
		D,

		/// <summary>
		/// ������������
		/// </summary>
		F
	}


	/// <summary>
	/// �������� ����
	/// </summary>
	public sealed class Column
	{

		/// <summary>
		/// ������������ ��� �������� ���������� �������.
		/// </summary>
		struct ColumnSizeArgument
		{
			public byte Length;
			public byte Decimals;
		}


		readonly DbfColumnType type;

		/// <summary>
		/// ��� ����
		/// </summary>
		public DbfColumnType Type
		{
			get { return this.type; }
		}

		readonly string name;

		/// <summary>
		/// ������������ ����
		/// </summary>
		public string Name
		{
			get { return this.name; }
		}

		byte length;
		/// <summary>
		/// ����� ����
		/// </summary>
		public byte Length
		{
			get { return this.length; }
			set
			{
				ColumnSizeArgument sz;
				sz.Length = value;
				sz.Decimals = this.decimals;
				TestColumnSize( this.type, ref sz);
				this.length = sz.Length;
			}
		}

		byte decimals;
		public byte Decimals
		{
			get { return this.decimals;}
			set
			{
				ColumnSizeArgument sz;
				sz.Length = this.length;
				sz.Decimals = value;
				TestColumnSize( this.type, ref sz);
				this.decimals = sz.Decimals;
			}
		}

		ushort offset;
		/// <summary>
		/// �������� ���� � ������ ����.
		/// </summary>
		internal ushort Offset
		{
			get { return this.offset;}
		}

		internal void SetOffset(ushort offset)
		{
			this.offset = offset;
		}

		private void TestColumnSize( DbfColumnType type, ref ColumnSizeArgument sz)
		{
			switch( type)
			{
				case DbfColumnType.C:
				{
					if( sz.Length ==0)
						throw new ArgumentException("����� ���� ������ ���� ������ ����.");

					if( length > 254)
						throw new ArgumentException("����� ����������� ���� �� ������ ������������ 254.");

					sz.Decimals = 0;
				}
					break;
				case DbfColumnType.D:
					sz.Length = 8; // ����� ���� ���� - 8 ����
					sz.Decimals = 0;
					break;
				case DbfColumnType.F: case DbfColumnType.N:
				{
					if( sz.Length ==0)
						throw new ArgumentException("����� ���� ������ ���� ������ ����.");

					if( sz.Length > 20)
						throw new ArgumentException("����� �������� ����� �� ������ ������������ 20.");

					if( sz.Decimals > sz.Length + 2)
						throw new ArgumentException("������� ������� ���-�� ������ ����� ���������� �����.");
				}
					break;
				case DbfColumnType.L: 
					sz.Length = 1; // ����� ����������� ���� 
					sz.Decimals = 0;
					break;
				default:
					throw new ArgumentException( "�� ��������������: " + type.ToString() );
			}
		}

		internal Column( string name, DbfColumnType type, byte length, byte decimals, int offset)
		{
			this.type = type;

			name = name.Trim();
			if( string.IsNullOrEmpty( name ))
				throw new ArgumentException("�� ������ ������������ ������� DBF.");
			if( name.Length >10)
				throw new ArgumentException( "����� ������������ ���� �� ������ ��������� 10 ��������: '"+name+"'.");
			this.name = name; //.ToUpper();

			this.decimals = 0;

			ColumnSizeArgument sz;
			sz.Decimals = decimals;
			sz.Length = length;

			this.TestColumnSize( type, ref sz);

            this.length = sz.Length;
			this.decimals = sz.Decimals;

			if( offset < 0)
				throw new ArgumentException("�������� DBF-���� ������ ����.");
			if( offset > ushort.MaxValue)
				throw new ArgumentException("�������� DBF-���� ��������� �������� " + ushort.MaxValue.ToString()+".");
			this.offset = (ushort)offset;
		}

		internal void Write( System.IO.Stream ostr)
		{
			System.IO.BinaryWriter bw = new System.IO.BinaryWriter( ostr);

			// 00-10
			Helper.WriteStringBufferToStream( bw, this.Name, 11); // 11, � �� 10; ��� ����

			// 11
			DbfColumnType type = this.Type;

			//// ������ ����� F �� N ����� ��� ����, ����� DBU.EXE ����� ���������� ����
			//if( type == DbfColumnType.F)
			//    type = DbfColumnType.N;

			Helper.WriteStringBufferToStream( bw, type.ToString(), 1);

			// 12-13
			bw.Write( this.offset);

			// 14-15
			Helper.WriteNullBytes( bw, 15-14+1);

			// 16
			bw.Write( this.length);

			// 17
			bw.Write( this.decimals);

			// 18-32
			Helper.WriteNullBytes( bw, 32-18);

			bw.Flush();
		}

		internal static Column Read(BinaryReader br)
		{
			// 00-11 - ��� ���� � 0x00 �����������
			byte[] nameBuf = br.ReadBytes( 10 );
			string name = Encoding.ASCII.GetString( nameBuf ).TrimEnd('\0');
			br.ReadByte(); // Skip 0x00

			// 11
			char ch = br.ReadChar();
			DbfColumnType type = (DbfColumnType)Enum.Parse( typeof(DbfColumnType), new string( ch, 1 ) );


			// 12-13
			ushort offset = (ushort)br.ReadUInt32();

			// 14-15
			//br.BaseStream.Position += 2;

			// 16
			byte length = br.ReadByte();

			// 17
			byte decimals = br.ReadByte();

			// 18-32
			br.BaseStream.Position += 32 - 18;

			return new Column( name, type, length, decimals, offset );
		}
	}
}
