using System.Collections.Generic;
using System.Diagnostics;

namespace IniFiles
{
	/// <summary>
	/// ��������� �������
	/// </summary>
	[DebuggerDisplay("Name = {Name}")]
	public sealed class Chapter
	{
		private string comment = null;
		public string Comment
		{
			get
			{
				return comment ?? string.Empty;
			}
			set
			{
				comment = value;
			}
		}

		private readonly string chapterName;
		public string Name
		{
			get { return chapterName; }
		}

		private readonly IniFile parent = null;

		private readonly IDictionary<string, Field> storage = new KeyValueStorage<string, Field>();
		private void AddEntry( Field fld)
		{
			if( this.Contains( fld.Name))
				throw new Exceptions.FieldNameExist(fld.Name);

			this.storage.Add( fld.Name, fld);
		}

		internal Chapter( IniFile parent, string name)
		{
			this.parent = parent;

			if( parent.Contains( name))
				throw new Exceptions.ChapterNameExist( name);

			this.chapterName = name;
		}

		public bool Contains( string fldName)
		{
			return storage.ContainsKey( fldName);
		}

		public Field this[string fldName]
		{
			get
			{
				Field fld;
				if( this.Contains( fldName))
					fld = storage[ fldName] as Field;
				else
				{
					fld = new Field( this, fldName);
					this.AddEntry( fld);
				}
				return fld;
			}
		}

		public void Remove( string fldName)
		{
			if( this.Contains( fldName))
				storage.Remove( fldName);
		}

		public string[] FieldNames()
		{
			string[] keys = new string[ storage.Count ];
			storage.Keys.CopyTo( keys, 0);
			return keys;
		}

		public void Clear()
		{
			this.Comment = string.Empty;
			storage.Clear();
		}

		public void Remove()
		{
			if( parent != null)
				parent.Remove( this.Name);
		}
	}
}
