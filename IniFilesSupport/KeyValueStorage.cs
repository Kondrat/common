using System;
using System.Collections.Generic;

namespace IniFiles
{
	class KeyValueStorage<TKey, TValue> : LinkedList<KeyValuePair<TKey, TValue>>, IDictionary<TKey, TValue>
	{
		public bool ContainsKey(TKey key)
		{
			foreach(KeyValuePair<TKey, TValue> pair in this)
			{
				if (pair.Key.Equals(key))
					return true;
			}

			return false;
		}

		public void Add(TKey key, TValue value)
		{
			if(this.ContainsKey( key ))
				throw new ArgumentException("Collection already has item with key= " + key.ToString());
			else
				this.AddLast( new KeyValuePair<TKey, TValue>(key, value));
		}

		public bool Remove(TKey key)
		{
			foreach(var pair in this)
			{
				if(pair.Key.Equals( key ))
				{
					this.Remove( pair );
					return true;
				}
			}

			return false;
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			foreach(var pair in this)
			{
				if(pair.Key.Equals( key ))
				{
					value = pair.Value;
					return true;
				}
			}

			value = default(TValue);
			return false;
		}

		public TValue this[TKey key]
		{
			get
			{
				TValue value;
				if(TryGetValue( key, out value ))
					return value;
				else
					throw new KeyNotFoundException("Not found key = " + key.ToString());
			}
			set 
			{ 
				var pairToAdd = new KeyValuePair<TKey, TValue>( key, value );

				var listNode = this.SeekNodeByKey( key );
				if(listNode != null)
					listNode.Value = pairToAdd;
				else
					this.AddLast( pairToAdd );
			}
		}

		LinkedListNode<KeyValuePair<TKey, TValue>> SeekNodeByKey(TKey key)
		{
			if(this.Count == 0)
				return null;

			LinkedListNode<KeyValuePair<TKey, TValue>> currNode = this.First;
			do
			{
				if(currNode.Value.Key.Equals( key ))
					return currNode;

				currNode = currNode.Next;
			} while(currNode != null);

			return null;
		}

		public ICollection<TKey> Keys
		{
			get
			{
				List<TKey> lst = new List<TKey>(this.Count);
				foreach(var pair in this)
				{
					lst.Add( pair.Key );
				}
				return lst;
			}
		}

		public ICollection<TValue> Values
		{
			get
			{
				List<TValue> lst = new List<TValue>(this.Count);
				foreach(var pair in this)
				{
					lst.Add(pair.Value);
				}
				return lst;
			}
		}
	}
}