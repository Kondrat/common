using System;

namespace IniFiles.Exceptions
{
	/// <summary>
	/// Summary description for Exceptions.
	/// </summary>
	public class FieldNameExist : System.ApplicationException
	{
		public FieldNameExist( string msg) :base(msg)
		{
		}
	}

	public class ChapterNameExist : System.ApplicationException
	{
		public ChapterNameExist( string msg) :base(msg)
		{
		}
	}
}
