using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IniFiles
{
	/// <summary>
	/// �������� ����� ��� ������� � ����������������
	/// </summary>
	public sealed class IniFile
	{
		//private string comment = null;
		//public string Comment
		//{
		//    get
		//    {
		//        return comment ?? string.Empty;
		//    }
		//    set
		//    {
		//        if( value != null)
		//        {
		//            string trimmed = value.Trim();
		//            comment = trimmed == string.Empty? null: trimmed;
		//        }
		//        else
		//            comment = null;
		//    }
		//}

		private readonly IDictionary<string, Chapter> storage = new KeyValueStorage<string, Chapter>();

		public bool Contains( string chapterName)
		{
			return storage.ContainsKey( chapterName);
		}

		public void Remove( string chapterName)
		{
			if( this.Contains( chapterName))
				storage.Remove( chapterName);
		}

		public string[] ChapterNames()
		{
			string[] keys = new string[ storage.Count ];
			storage.Keys.CopyTo( keys, 0);
			return keys;
		}

		private void AddEntry( Chapter chapter)
		{
			if( this.Contains( chapter.Name))
				throw new Exceptions.ChapterNameExist(chapter.Name);

			this.storage.Add( chapter.Name, chapter);
		}

		public Chapter this[string chapterName]
		{
			get
			{
				Chapter chapter;
				if( this.Contains( chapterName))
					chapter = storage[ chapterName];
				else
				{
					chapter = new Chapter( this, chapterName);
					this.AddEntry( chapter);
				}
				return chapter;
			}
		}

		public Field this[ string chapterName, string fieldName]
		{
			get
			{
				Chapter chpt = this[chapterName];
				return chpt[fieldName];
			}
		}

		public void Clear()
		{
			//this.Comment = string.Empty;
			storage.Clear();
		}

		#region READ / WRITE

		#region WRITE

		/*********** WRITE ***********/
		public void WriteTo( Stream ostr)
		{
			StreamWriter wrt = new StreamWriter( ostr, Encoding.UTF8);
			this.WriteTo( wrt);
		}

		public void WriteTo( Stream ostr, Encoding encoding)
		{
			StreamWriter wrt = new StreamWriter( ostr, encoding);
			this.WriteTo( wrt);
		}

		public void WriteTo( TextWriter wrt)
		{
			IniWriter.Write( wrt, this);
			wrt.Flush();
		}

		public void WriteTo( string filename)
		{
			this.WriteTo( filename, Encoding.UTF8);
		}

		public void WriteTo( string filename, Encoding encoding)
		{
			using( StreamWriter wrt = new StreamWriter( filename, false, encoding) )
			{
				this.WriteTo( wrt);
				wrt.Close();
			}
		}

		#endregion

		#region READ

		/*********** READ ************/
		public void ReadFrom(Stream istr)
		{
			StreamReader rdr = new StreamReader( istr, Encoding.UTF8);
			this.ReadFrom( rdr);
		}
		public void ReadFrom( Stream istr, Encoding encoding)
		{
			StreamReader rdr = new StreamReader( istr, encoding, false);
			this.ReadFrom( rdr);
		}
		public void ReadFrom(TextReader rdr)
		{
			IniReader.Read( rdr, this);
		}

		public void ReadFrom( string filename)
		{
			this.ReadFrom( filename, Encoding.UTF8);
		}

		public void ReadFrom( string filename, Encoding encoding)
		{
			using( StreamReader rdr = new StreamReader( filename, encoding, false) )
			{
				this.ReadFrom( rdr);
				rdr.Close();
			}
		}

		#endregion

		#region APPENDREAD

		/*********** APPENDREAD ************/
		public void AppendReadFrom(Stream istr)
		{
			StreamReader rdr = new StreamReader( istr);
			this.AppendReadFrom( rdr);
		}
		public void AppendReadFrom( Stream istr, Encoding encoding)
		{
			StreamReader rdr = new StreamReader( istr, encoding, false);
			this.AppendReadFrom( rdr);
		}
		public void AppendReadFrom(TextReader rdr)
		{
			IniReader.AppendRead( rdr, this);
		}

		public void AppendReadFrom( string filename)
		{
			this.AppendReadFrom( filename, Encoding.UTF8);
		}

		public void AppendReadFrom( string filename, Encoding encoding)
		{
			using( StreamReader rdr = new StreamReader( filename, encoding, false) )
			{
				this.AppendReadFrom( rdr);
				rdr.Close();
			}
		}

		#endregion

		#region STATIC READ

		/*********** STATIC READ ************/
		public static IniFile Read(Stream istr)
		{
			IniFile ini = new IniFile();
			ini.ReadFrom( istr);
			return ini;
		}
		public static IniFile Read( Stream istr, Encoding encoding)
		{
			IniFile ini = new IniFile();
			ini.ReadFrom( istr, encoding);
			return ini;
		}
		public static IniFile Read(TextReader rdr)
		{
			IniFile ini = new IniFile();
			ini.ReadFrom( rdr);
			return ini;
		}

		public static IniFile Read( string filename)
		{
			IniFile ini = new IniFile();
			ini.ReadFrom( filename);
			return ini;
		}

		public static IniFile Read( string filename, Encoding encoding)
		{
			IniFile ini = new IniFile();
			ini.ReadFrom( filename, encoding);
			return ini;
		}

		#endregion

		#endregion

		public IEnumerable<Field> FieldsIterator
		{
			get
			{
				foreach(KeyValuePair<string, Chapter> pair in storage)
				{
					foreach(string fldName in pair.Value.FieldNames())
					{
						yield return pair.Value[fldName];
					}
				}
			}
		}
	}
}
