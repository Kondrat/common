using System;
using System.Diagnostics;

namespace IniFiles
{
	/// <summary>
	/// ���� � �������
	/// </summary>
	[DebuggerDisplay("Chapter= {Chapter.Name}; Name= {Name}, Value= {Value}")]
	public sealed class Field
	{
		private readonly Chapter chapter;

		internal Field( Chapter chapter, string name)
		{
			this.chapter = chapter;

			if( name == null)
				throw new ArgumentException("��� ���� �� ����� ���� ������.");

			string key = name.Trim();
			if( key == string.Empty)
				throw new ArgumentException("��� ���� �� ����� ���� ������.");

			if( chapter.Contains( key))
				throw new Exceptions.FieldNameExist( string.Format("{0}", key) );

			this.fldName = key;
		}


		internal Field( Chapter chapter, string name, string value)
		{
			this.chapter = chapter;
			
			if( fldName == null)
				throw new ArgumentException("��� ���� �� ����� ���� ������.");

			string key = name.Trim();
			if( key == string.Empty)
				throw new ArgumentException("��� ���� �� ����� ���� ������.");

			if( chapter.Contains( key))
				throw new Exceptions.FieldNameExist( string.Format("{0}", key) );

			this.fldName = key;
			this.Value = value;
		}

		// ��������

		private readonly string fldName;
		public string Name
		{
			get { return fldName; }
		}

		private string fldValue = string.Empty;
		public string Value
		{
			get
			{
				return fldValue;
			}
			set
			{
				if( value == null)
					this.fldValue = string.Empty;
				else
					this.fldValue = value;
			}
		}

		private string comment = string.Empty;
		public string Comment
		{
			get
			{
				return comment;
			}
			set
			{
				this.comment = value;
			}
		}

		public void Clear()
		{
			this.Comment = string.Empty;
			this.Value = string.Empty;
		}

		public void Remove()
		{
			if( chapter != null)
				chapter.Remove( this.Name);
		}

		public Chapter Chapter
		{
			get
			{
				return chapter;
			}
		}
	}
}
