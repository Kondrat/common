using System;
using System.IO;
using System.Text;

namespace IniFiles
{
	/// <summary>
	/// ���� INI-������
	/// </summary>
	internal sealed class IniReader
	{
		private static readonly StringBuilder s_bldComments = new StringBuilder();
		private static Chapter s_currentChapter;

		public static void Read( TextReader rdr, IniFile ini)
		{
			ini.Clear();
			AppendRead( rdr, ini);
		}

		public static void AppendRead( TextReader rdr, IniFile ini)
		{
			string line;

			while( (line = rdr.ReadLine()) != null )
			{
				if( IsEmptyString( line))
					continue;

				if( IsComment( line))
				{
					AppendLineToComment( line);
					continue;
				}

				if( IsChapter( line))
				{
					string chapterName = ParseChapterName( line);
					s_currentChapter = ini[chapterName];
					s_currentChapter.Comment = s_bldComments.ToString();
					ClearCommentAccumulator();
					continue;
				}

				if( IsField( line))
				{
					ParsedField parsedFld = ParseField( line);
					string fieldName = parsedFld.Name;

					Field fld = s_currentChapter[fieldName];
					fld.Value = parsedFld.Value;

					fld.Comment = s_bldComments.ToString();
					ClearCommentAccumulator();

					continue;
				}
			}
		}

		private static void ClearCommentAccumulator()
		{
			s_bldComments.Length = 0;
		}

		private static void AppendLineToComment( string line)
		{
			// ���������� ����� � ������� � ������ �����
			string cm = line.Substring( 1, line.Length -1);

			if( s_bldComments.Length >0)
				s_bldComments.Append('\n');// Environment.NewLine);

			s_bldComments.Append( cm);
		}

		private static string ParseChapterName( string line)
		{
			return line.Trim().Substring( 1, line.Length -2);
		}

		private static ParsedField ParseField( string line)
		{
			int delimPos = line.IndexOf('=');
			if( delimPos == -1)
				throw new ArgumentException("�� ������ ����������� '=' � ������ INI-�����.", "line");

			string key = line.Substring( 0, delimPos).Trim();
			string value = line.Substring( delimPos+1, line.Length - delimPos -1);

			if( key == string.Empty)
				throw new ArgumentException("��� ���� �� ����� ���� ������.");

			ParsedField res = new ParsedField();
			res.Name = key;
			res.Value = value;
			return res;
		}

		private static bool IsComment(string line)
		{
			if( line.Length <=0)
				return false;
			if( line[0] == ';')
				return true;
			return false;
		}

		private static bool IsEmptyString(string line)
		{
			return line.Trim().Length == 0;
		}

		private static bool IsChapter(string line)
		{
			if( line.Length <=0)
				return false;

			string line2 = line.Trim();
			if( line2[0]=='[' && line2[line2.Length-1]==']')
				return true;
			return false;
		}

		private static bool IsField(string line)
		{
			if( line.Length <=0)
				return false;

			if( line[0] == ';' || line[0] == '=' )
				return false;

			int delimPos = line.IndexOf('=');
			return delimPos >= 0;
		}
	}

	internal struct ParsedField
	{
		public string Name;
		public string Value;
	}
}
