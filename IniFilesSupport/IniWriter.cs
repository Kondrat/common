using System;
using System.IO;

namespace IniFiles
{
	/// <summary>
	/// ������������ INI-�����
	/// </summary>
	internal sealed class IniWriter
	{
		public static void Write( TextWriter wrt, IniFile ini)
		{
			string[] chapterNames = ini.ChapterNames();

			for(int idx = 0; idx < chapterNames.Length; idx++)
			{
				string chapterName = chapterNames[idx];
				WriteChapter( wrt, ini[chapterName]);

				if(idx != chapterNames.Length-1)
					wrt.WriteLine();
			}
		}

		private static void WriteChapter( TextWriter wrt, Chapter chapter)
		{
			WriteComment( wrt, chapter.Comment);
			wrt.WriteLine( string.Format("[{0}]", chapter.Name));

			string[] fldNames = chapter.FieldNames();
			foreach( string fldName in fldNames)
				WriteField( wrt, chapter[fldName]);
		}

		private static void WriteField( TextWriter wrt, Field field)
		{
			WriteComment( wrt, field.Comment);
			wrt.WriteLine( "{0}={1}", field.Name, field.Value);
		}

		private static void WriteComment( TextWriter wrt, string comment)
		{
			if( comment != null && comment.Trim() != string.Empty)
			{
				string[] comments = comment.Split( Environment.NewLine.ToCharArray() );
				foreach( string cm in comments)
					wrt.WriteLine(";{0}", cm);
			}
		}
	}
}
