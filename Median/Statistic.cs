﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SSD_Viewer.Core
{
	class StatisticaImpl
	{
		public struct Moda
		{
			public Moda(double value, int count)
			{
				Value = value;
				Count = count;
			}

			public readonly double Value;
			public readonly int Count;
		}

		private readonly List<double> data;

		public StatisticaImpl(List<double> data)
		{
			Debug.Assert(data != null);

			if(data.Count == 0)
				throw new ArgumentException("data.Count == 0");

			this.data = data;
			this.data.Sort();
		}


		public List<double> Data
		{
			get { return data; }
		}

		#region MinMax

		public double Min
		{
			get
			{
				return data[0];
			}
		}

		public double Max
		{
			get
			{
				return data[data.Count - 1];
			}
		}

		#endregion

		#region Moda

		//private int? moda = null;
		//public int Moda
		//{
		//    get
		//    {
		//        if(!moda.HasValue)
		//        {
		//            CalculateModa();
		//        }
				
		//        return moda.Value;
		//    }
		//}

		//private void CalculateModa()
		//{
		//    throw new NotImplementedException();
		//}

		#endregion

		#region Average

		private double? average;
		public double Average
		{
			get
			{
				if(!average.HasValue)
					CalculateAverage();
				return average.Value;
				
			}
		}

		private void CalculateAverage()
		{
			double sum = 0d;
			foreach(double val in data)
			{
				sum += val;
			}
			average = sum / data.Count;
		}

		#endregion

		#region Median

		private double? median;
		public double Median
		{
			get
			{
				if(!median.HasValue)
					CalculateMedian();
				return median.Value;
			}
		}

		private void CalculateMedian()
		{
			if(data.Count == 1)
			{
				median = data[0];
				return;
			}

			if(data.Count % 2 == 1)
			{
				int idx = (int)Math.Floor( (decimal)(data.Count-1)/2);
				median = data[idx];
			}
			else
			{
				int idx1 = (int)Math.Floor( (decimal)(data.Count-1)/2);
				int idx2 = idx1 + 1;
				median = (data[idx1] + data[idx2])/2f;
			}
		}

		#endregion

		#region GetMedianParts

		public List<double> GetMedianParts(int partsCount)
		{
			if(data.Count < partsCount)
				return new List<double>();

			List<double> res = new List<double>();

			bool bLastLoop = false;
			int step = data.Count/partsCount;
			int idx = 0;
			while(!bLastLoop)
			{
				int idx2 = idx + step;
				if(data.Count - idx2 < step /*последний шаг*/)
				{
					idx2 = data.Count - 1;
					bLastLoop = true;
				}

				List<double> range = Range(idx, idx2);
				StatisticaImpl statisticaImpl = new StatisticaImpl(range);
				res.Add(statisticaImpl.Median);

				idx = idx2;
			}

			return res;
		}

		#endregion

		public List<Moda> GetModaList()
		{
			IDictionary<int, int> accum = new Dictionary<int, int>();
			foreach(int val in data)
			{
				try
				{
					int count = accum[val];
					accum[val] = count + 1;
				}
				catch(Exception)
				{
					accum[val] = 1;
				}
			}

			List<Moda> res = new List<Moda>(accum.Count);
			foreach(KeyValuePair<int, int> pair in accum)
			{
				res.Add(new Moda(pair.Key, pair.Value));
			}

			res.Sort(
				delegate(Moda m1, Moda m2)
				{
				    int countComp = m1.Count.CompareTo(m2.Count);
				    if(countComp != 0)
				        return -1 * countComp;

				    return m1.Value.CompareTo(m2.Value);
				}
					);

			return res;
		}

		private List<double> Range(int idx1, int idx2)
		{
			return data.GetRange(idx1, idx2 - idx1);
		}
	}
}
