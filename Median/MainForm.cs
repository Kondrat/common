﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Median
{
	public partial class MainForm :Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void btnExit_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnCalc_Click(object sender, EventArgs e)
		{
			try
			{
				this.UseWaitCursor = true;
				txtResult.Clear();

//                txtSrc.Text = @"1
//2
//3
//4
//5
//6";

				List<float> data = ReadData();

				Statistic statistic = new Statistic(data);

				Write("Длина ряда:", data.Count.ToString());
				WriteLn();
				Write("Min: ", statistic.Min.ToString());
				Write("Max: ", statistic.Max.ToString());
				Write("Среднее: ", statistic.Average.ToString());
				Write("Медиана: ", statistic.Median.ToString());

				WriteParts(5, statistic);
				WriteParts(10, statistic);
				WriteParts(20, statistic);

				WriteLn();
				Write("Модальный ряд:", string.Empty);
				StringBuilder sbModa = new StringBuilder();
				foreach(Statistic.Moda moda in statistic.GetModaList())
				{
					sbModa.Append("   " + moda.Value.ToString() + "  : " + moda.Count.ToString() + "\r\n");
				}
				this.txtResult.AppendText(sbModa.ToString());
			}
			catch(Exception ex)
			{
				MessageBox.Show(this, ex.Message, ex.GetType().Name);
			}
			finally
			{
				this.UseWaitCursor = false;
			}
		}

		private void WriteParts(int parts, Statistic statistic)
		{
			WriteLn();
			Write("Распределение по", (100 / parts).ToString() + "%");
			foreach(float medianPart in statistic.GetMedianParts(parts))
			{
				Write(" - ", medianPart.ToString());
			}
		}

		private void WriteLn()
		{
			txtResult.AppendText("\r\n");
		}

		private void Write(string title, string value)
		{
			txtResult.AppendText( string.Format("{0} {1}\r\n", title, value));
		}

		private List<float> ReadData()
		{
			List<float> res = new List<float>();

			StringReader rdr = new StringReader(this.txtSrc.Text);
			string line = null;
			while((line = rdr.ReadLine()) != null)
			{
				try
				{
					float val = float.Parse(line.Replace('.', ','));
					res.Add(val);
				}
				catch(FormatException ex)
				{
					throw new FormatException(ex.Message + ": " + line, ex);
				}
			}

			return res;
		}
	}
}