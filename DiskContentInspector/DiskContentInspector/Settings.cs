﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace DiskContentInspector
{
	static class Settings
	{
		private const string cnstSPathToShares = @"Software\DiskContentInspector";

		public static void WriteShareList(string[] shareList)
		{
			var sharesNode = Registry.CurrentUser.CreateSubKey( cnstSPathToShares);
			using(sharesNode)
			{
				string[] valuesNames = sharesNode.GetValueNames();
				foreach(var valueName in valuesNames)
				{
					sharesNode.DeleteValue( valueName );
				}

				int idx = 0;
				foreach(var sharePath in shareList)
				{
					string key = "share_" + idx.ToString();
					sharesNode.SetValue(key, sharePath);
					idx++;
				}
			}
		}

		public static string[] ReadShareList()
		{
			var sharesNode = Registry.CurrentUser.OpenSubKey(cnstSPathToShares);
			if(sharesNode == null)
				return new string[0];

			using(sharesNode)
			{
				List<string> res = new List<string>();

				string[] valuesNames = sharesNode.GetValueNames();
				foreach(var valueName in valuesNames)
				{
					res.Add( (string)sharesNode.GetValue( valueName ) );
				}

				return res.ToArray();
			}
		}
	}
}
