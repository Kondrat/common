﻿namespace DiskContentInspector
{
	partial class DoublicatesCtrl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.cbFileName = new System.Windows.Forms.CheckBox();
			this.cbFileSize = new System.Windows.Forms.CheckBox();
			this.cbMd5 = new System.Windows.Forms.CheckBox();
			this.btnFind = new System.Windows.Forms.Button();
			this.lv = new System.Windows.Forms.ListView();
			this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colMd5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colFullName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.mnuFile = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mnuOpenFile = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuOpenExplorer = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuCopyFilePath = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuCopyFolderPath = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuCopyFileName = new System.Windows.Forms.ToolStripMenuItem();
			this.btnSaveToCsv = new System.Windows.Forms.Button();
			this.sfd = new System.Windows.Forms.SaveFileDialog();
			this.mnuFile.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(608, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "Doublicates";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbFileName
			// 
			this.cbFileName.AutoSize = true;
			this.cbFileName.Checked = true;
			this.cbFileName.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbFileName.Location = new System.Drawing.Point(3, 31);
			this.cbFileName.Name = "cbFileName";
			this.cbFileName.Size = new System.Drawing.Size(71, 17);
			this.cbFileName.TabIndex = 1;
			this.cbFileName.Text = "File name";
			this.cbFileName.UseVisualStyleBackColor = true;
			this.cbFileName.CheckedChanged += new System.EventHandler(this.cb_CheckedChanged);
			// 
			// cbFileSize
			// 
			this.cbFileSize.AutoSize = true;
			this.cbFileSize.Checked = true;
			this.cbFileSize.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbFileSize.Location = new System.Drawing.Point(81, 32);
			this.cbFileSize.Name = "cbFileSize";
			this.cbFileSize.Size = new System.Drawing.Size(63, 17);
			this.cbFileSize.TabIndex = 2;
			this.cbFileSize.Text = "File size";
			this.cbFileSize.UseVisualStyleBackColor = true;
			this.cbFileSize.CheckedChanged += new System.EventHandler(this.cb_CheckedChanged);
			// 
			// cbMd5
			// 
			this.cbMd5.AutoSize = true;
			this.cbMd5.Location = new System.Drawing.Point(150, 32);
			this.cbMd5.Name = "cbMd5";
			this.cbMd5.Size = new System.Drawing.Size(47, 17);
			this.cbMd5.TabIndex = 3;
			this.cbMd5.Text = "MD5";
			this.cbMd5.UseVisualStyleBackColor = true;
			this.cbMd5.CheckedChanged += new System.EventHandler(this.cb_CheckedChanged);
			// 
			// btnFind
			// 
			this.btnFind.Location = new System.Drawing.Point(205, 28);
			this.btnFind.Name = "btnFind";
			this.btnFind.Size = new System.Drawing.Size(75, 23);
			this.btnFind.TabIndex = 4;
			this.btnFind.Text = "Find";
			this.btnFind.UseVisualStyleBackColor = true;
			this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
			// 
			// lv
			// 
			this.lv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colSize,
            this.colMd5,
            this.colFullName});
			this.lv.ContextMenuStrip = this.mnuFile;
			this.lv.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lv.FullRowSelect = true;
			this.lv.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lv.HideSelection = false;
			this.lv.Location = new System.Drawing.Point(0, 55);
			this.lv.MultiSelect = false;
			this.lv.Name = "lv";
			this.lv.Size = new System.Drawing.Size(608, 198);
			this.lv.TabIndex = 5;
			this.lv.UseCompatibleStateImageBehavior = false;
			this.lv.View = System.Windows.Forms.View.Details;
			this.lv.VirtualMode = true;
			this.lv.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.lst_RetrieveVirtualItem);
			this.lv.DoubleClick += new System.EventHandler(this.lv_DoubleClick);
			// 
			// colName
			// 
			this.colName.Text = "Name";
			// 
			// colSize
			// 
			this.colSize.Text = "Size";
			// 
			// colMd5
			// 
			this.colMd5.Text = "MD5";
			// 
			// colFullName
			// 
			this.colFullName.Text = "Full name";
			// 
			// mnuFile
			// 
			this.mnuFile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpenFile,
            this.mnuOpenExplorer,
            this.toolStripMenuItem1,
            this.mnuCopyFilePath,
            this.mnuCopyFolderPath,
            this.mnuCopyFileName});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Size = new System.Drawing.Size(254, 142);
			this.mnuFile.Opening += new System.ComponentModel.CancelEventHandler(this.mnuFile_Opening);
			// 
			// mnuOpenFile
			// 
			this.mnuOpenFile.Name = "mnuOpenFile";
			this.mnuOpenFile.Size = new System.Drawing.Size(253, 22);
			this.mnuOpenFile.Text = "Открыть файл";
			this.mnuOpenFile.Click += new System.EventHandler(this.mnuOpenFile_Click);
			// 
			// mnuOpenExplorer
			// 
			this.mnuOpenExplorer.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
			this.mnuOpenExplorer.Name = "mnuOpenExplorer";
			this.mnuOpenExplorer.Size = new System.Drawing.Size(253, 22);
			this.mnuOpenExplorer.Text = "Открыть каталог в Explorer";
			this.mnuOpenExplorer.Click += new System.EventHandler(this.mnuOpenExplorer_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(250, 6);
			// 
			// mnuCopyFilePath
			// 
			this.mnuCopyFilePath.Name = "mnuCopyFilePath";
			this.mnuCopyFilePath.Size = new System.Drawing.Size(253, 22);
			this.mnuCopyFilePath.Text = "Скопировать полное имя файла";
			this.mnuCopyFilePath.Click += new System.EventHandler(this.mnuCopyFilePath_Click);
			// 
			// mnuCopyFolderPath
			// 
			this.mnuCopyFolderPath.Name = "mnuCopyFolderPath";
			this.mnuCopyFolderPath.Size = new System.Drawing.Size(253, 22);
			this.mnuCopyFolderPath.Text = "Скопировать каталог файла";
			this.mnuCopyFolderPath.Click += new System.EventHandler(this.mnuCopyFolderPath_Click);
			// 
			// mnuCopyFileName
			// 
			this.mnuCopyFileName.Name = "mnuCopyFileName";
			this.mnuCopyFileName.Size = new System.Drawing.Size(253, 22);
			this.mnuCopyFileName.Text = "Скопировать имя файла";
			this.mnuCopyFileName.Click += new System.EventHandler(this.mnuCopyFileName_Click);
			// 
			// btnSaveToCsv
			// 
			this.btnSaveToCsv.Enabled = false;
			this.btnSaveToCsv.Location = new System.Drawing.Point(297, 28);
			this.btnSaveToCsv.Name = "btnSaveToCsv";
			this.btnSaveToCsv.Size = new System.Drawing.Size(118, 23);
			this.btnSaveToCsv.TabIndex = 6;
			this.btnSaveToCsv.Text = "Save to CSV...";
			this.btnSaveToCsv.UseVisualStyleBackColor = true;
			this.btnSaveToCsv.Click += new System.EventHandler(this.btnSaveToCsv_Click);
			// 
			// sfd
			// 
			this.sfd.DefaultExt = "csv";
			this.sfd.FileName = "doublicates.csv";
			this.sfd.Title = "Save .csv";
			// 
			// DoublicatesCtrl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnSaveToCsv);
			this.Controls.Add(this.lv);
			this.Controls.Add(this.btnFind);
			this.Controls.Add(this.cbMd5);
			this.Controls.Add(this.cbFileSize);
			this.Controls.Add(this.cbFileName);
			this.Controls.Add(this.label1);
			this.Name = "DoublicatesCtrl";
			this.Size = new System.Drawing.Size(608, 253);
			this.VisibleChanged += new System.EventHandler(this.DoublicatesCtrl_VisibleChanged);
			this.mnuFile.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox cbFileName;
		private System.Windows.Forms.CheckBox cbFileSize;
		private System.Windows.Forms.CheckBox cbMd5;
		private System.Windows.Forms.Button btnFind;
		private System.Windows.Forms.ListView lv;
		private System.Windows.Forms.ColumnHeader colName;
		private System.Windows.Forms.ColumnHeader colSize;
		private System.Windows.Forms.ColumnHeader colMd5;
		private System.Windows.Forms.ColumnHeader colFullName;
		private System.Windows.Forms.Button btnSaveToCsv;
		private System.Windows.Forms.SaveFileDialog sfd;
		private System.Windows.Forms.ContextMenuStrip mnuFile;
		private System.Windows.Forms.ToolStripMenuItem mnuOpenFile;
		private System.Windows.Forms.ToolStripMenuItem mnuOpenExplorer;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem mnuCopyFilePath;
		private System.Windows.Forms.ToolStripMenuItem mnuCopyFolderPath;
		private System.Windows.Forms.ToolStripMenuItem mnuCopyFileName;
	}
}
