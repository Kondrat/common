﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DiskContentInspector.Csv;

static class Helper
{
	internal static void WriteDataGridViewContentToCsv(DataGridView gridView, string fileName)
	{
		using(var ostr = new FileStream( fileName, FileMode.Create, FileAccess.Write ))
		using(var txtWrt = new StreamWriter( ostr, Encoding.GetEncoding( 1251 ) ))
		{
			CsvWriter wrt = new CsvWriter(txtWrt, CsvDefines.ExcelCompatible);

			string[] title = gridView.Columns
				.Cast<DataGridViewColumn>()
				.OrderBy(el => el.DisplayIndex)
				.Select( el => el.HeaderText )
				.ToArray();

			wrt.WriteLine( title );

			foreach(DataGridViewRow row in gridView.Rows)
			{
				var cells = row.Cells
					.Cast<DataGridViewCell>()
					.Select( el => el.Value.ToString() )
					.ToArray();

				wrt.WriteLine( cells );
			}
		}
	}
}
