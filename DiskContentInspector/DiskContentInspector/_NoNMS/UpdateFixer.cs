using System;
using System.Collections;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Common.WinApi;

public class UpdateFixer :IDisposable
{

	static readonly object s_syncObj = new object();
	static readonly ArrayList s_lockedHandlesCollector = new ArrayList(20);

	readonly Control control;

	public UpdateFixer(Control control)
	{
		lock(s_syncObj)
		{
			this.control = control;
			WindowTool.BeginUpdate(this.control);
			s_lockedHandlesCollector.Add(this.control.Handle);
		}
	}

	#region IDisposable Members

	public void Dispose()
	{
		lock(s_syncObj)
		{
			WindowTool.EndUpdate(this.control);
			s_lockedHandlesCollector.Remove(this.control.Handle);
//				GC.SuppressFinalize(this);
		}
	}

	#endregion

	public static bool IsControlLocked(Control control)
	{
		lock(s_syncObj)
		{
			return s_lockedHandlesCollector.Contains(control.Handle);
		}
	}

	public static UpdateFixer Instance(Control control)
	{
		return new UpdateFixer(control);
	}
}

namespace Common
{
	namespace WinApi
	{
		internal class WindowTool
		{
			[DllImport("user32.dll")]
			private static extern int SendMessage(IntPtr hWnd, uint msg, uint wParam, uint lParam);
 
			public static void BeginUpdate(Control control)
			{
				if( control != null)
					SendMessage(control.Handle, 11, 0, 0); // WM_SETREDRAW
			}
 
			public static void EndUpdate(Control control)
			{
				if( control != null)
				{
					SendMessage(control.Handle, 11, 1, 0); // WM_SETREDRAW
					control.Refresh();
				}
			}
		}
	}
}
