﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

public static class MyGuiIterators
{
	public static IEnumerable<TreeNode> TreeNodesIterator(this TreeNodeCollection nodes)
	{
		foreach(TreeNode node in nodes)
		{
			yield return node;
			foreach(TreeNode childNode in TreeNodesIterator(node.Nodes))
			{
				yield return childNode;
			}
		}
	}
}
