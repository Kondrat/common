using System;
using System.Windows.Forms;

/// <summary>
/// Summary description for MsgBoxes.
/// </summary>
public static class MsgBoxes
{

	public static DialogResult QuestionDlg(Control parent, string msg, MessageBoxDefaultButton defBtn, string title)
	{
		return MessageBox.Show(
			parent,
			msg,
			title,
			MessageBoxButtons.YesNo,
			MessageBoxIcon.Question,
			defBtn
			);
	}

	public static DialogResult QuestionDlg(Control parent, string msg, MessageBoxDefaultButton defBtn)
	{
		return QuestionDlg(parent, msg, defBtn, "������...");
	}

	public static DialogResult QuestionDlg(Control parent, string msg)
	{
		return QuestionDlg(parent, msg, MessageBoxDefaultButton.Button1);
	}

	public static DialogResult QuestionDlg(string msg, MessageBoxDefaultButton defBtn, string title)
	{
		return MessageBox.Show(
			GuiUtils.ForegroundWindow,
			msg,
			title,
			MessageBoxButtons.YesNo,
			MessageBoxIcon.Question,
			defBtn
			);
	}

	public static DialogResult QuestionDlg(string msg, MessageBoxDefaultButton defBtn)
	{
		return QuestionDlg(msg, defBtn, "������...");
	}

	public static DialogResult QuestionDlg(string msg)
	{
		return QuestionDlg(msg, MessageBoxDefaultButton.Button1);
	}


	public static void NotifyDlg(Control parent, string msg)
	{
		MessageBox.Show(parent,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Information
			);
	}
	public static void NotifyDlg(string msg)
	{
		MessageBox.Show(GuiUtils.ForegroundWindow,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Information
			);
	}


	public static void ErrorDlg(Control parent, string msg)
	{
		MessageBox.Show(parent,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Error
			);
	}
	public static void ErrorDlg(string msg)
	{
		MessageBox.Show(GuiUtils.ForegroundWindow,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Error
			);
	}


	public static void WarningDlg(Control parent, string msg)
	{
		MessageBox.Show(parent,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Warning
			);
	}
	public static void WarningDlg(string msg)
	{
		MessageBox.Show(GuiUtils.ForegroundWindow,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Warning
			);
	}


	public static void StopDlg(Control parent, string msg)
	{
		MessageBox.Show(parent,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Stop
			);
	}
	public static void StopDlg(string msg)
	{
		MessageBox.Show(GuiUtils.ForegroundWindow,
			msg,
			"�����������...",
			MessageBoxButtons.OK,
			MessageBoxIcon.Stop
			);
	}


}
