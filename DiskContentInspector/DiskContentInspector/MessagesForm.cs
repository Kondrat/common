﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DiskContentInspector
{
	public partial class MessagesForm :Form
	{
		public MessagesForm()
		{
			InitializeComponent();
		}

		public void AddMessage(string msg)
		{
			if(msg.Length == 0)
				return;

			if(this.txtMessages.TextLength > 0)
				this.txtMessages.AppendText(Environment.NewLine);

			this.txtMessages.AppendText("* " + msg);
		}
	}
}
