﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiskContentInspector
{
	public partial class DirectoryTree :UserControl
	{
		public DirectoryTree()
		{
			InitializeComponent();
		}

		private void DirectoryTree_Load(object sender, EventArgs e)
		{
			this.tv.Dock = DockStyle.Fill;

			if( ! DesignMode)
			{
				LoadTree();
			}
		}

		private void LoadTree()
		{
			this.tv.Nodes.Clear();

			TreeNode root = new TreeNode( "Local Computer" )
				{
					ImageIndex = 4,
					SelectedImageIndex = 4
				};
			this.tv.Nodes.Add( root );

			var drives = DriveInfo.GetDrives()
				.Where(el => el.DriveType == DriveType.Fixed || el.DriveType == DriveType.Network)
				.ToArray();
			
			foreach(var driveInfo in drives)
			{
				if(driveInfo.DriveType == DriveType.Fixed || driveInfo.DriveType == DriveType.Network)
				{
					string name = string.Format("{0} ('{1}')", driveInfo.Name, driveInfo.VolumeLabel);
					FsTreeNode tn = new FsTreeNode(name, driveInfo.Name);
					tn.ImageIndex = tn.SelectedImageIndex = driveInfo.DriveType == DriveType.Fixed? 0: 5;
					root.Nodes.Add(tn);

					switch(driveInfo.DriveType)
					{
						case DriveType.Fixed:
							tn.Text += " /fixed disk/";
							break;
						case DriveType.Network:
							tn.Text += " /network disk/";
							break;
					}

					if(driveInfo.DriveType == DriveType.Fixed)
						tn.LoadSubItems();
				}
			}

			root.Expand();
		}

		private void tv_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			using(SetCursor.Wait(this))
			{
				FsTreeNode node = e.Node as FsTreeNode;
				if(node != null)
					node.LoadSubItems();
			}
		}

		public string SelectedPath
		{
			get
			{
				TreeNode selectedNode = this.tv.SelectedNode;
				if(selectedNode == null)
					return string.Empty;
				else
				{
					FsTreeNode node = selectedNode as FsTreeNode;
					if(node != null)
						return node.Path;
					else
						return null;
				}
			}
		}

		public event TreeViewEventHandler AfterSelect;

		private void tv_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if(this.AfterSelect != null)
				AfterSelect( sender, e );
		}

		private const string cnstSNetworkNodeTitle = "* Network *";

		public bool IsSelectedPathIsShare
		{
			get
			{
				if(this.tv.SelectedNode.Parent == null)
					return false;

				return this.tv.SelectedNode.Parent.Text == cnstSNetworkNodeTitle;
			}
		}

		private TreeNode AddGroupNetworkNode()
		{
			TreeNode node = new TreeNode( cnstSNetworkNodeTitle )
				{
					ImageIndex = 3,
					SelectedImageIndex = 3
				};
			this.tv.Nodes.Add( node);

			return node;
		}

		public void AddNetworkSharesFromSettings(string[] shareList)
		{
			foreach(var share in shareList)
			{
				AddNetworkShare( share );
			}
		}

		public void AddNetworkShare(string share)
		{
			string[] shareList = GetNetworkShareList();

			if(Array.IndexOf( shareList, share) != -1)
				return;

			Task task = new Task( () => AddShareAsync( share ));
			task.ContinueWith( el => TryReportError(el, share), TaskContinuationOptions.OnlyOnFaulted);
			task.Start();
		}

		private void TryReportError(Task task, string share)
		{
			if(task.IsFaulted)
			{
				this.Invoke( () => MsgBoxes.ErrorDlg( this, task.Exception.InnerException.Message + Environment.NewLine + share ) );
			}
		}

		private void AddShareAsync(string share)
		{
			TreeNode shareNode = new FsTreeNode( share, share )
				{
					ImageIndex = 2,
					SelectedImageIndex = 2
				};

			this.Invoke( () =>
			{
				TreeNode groupNode = GetNetworkRoot();

				if(groupNode == null)
					groupNode = AddGroupNetworkNode();

				groupNode.Nodes.Add( shareNode );
				shareNode.EnsureVisible();

				// Sorting
				var selectedNode = this.tv.SelectedNode;
				var nodesToSort = groupNode.Nodes.OfType<FsTreeNode>()
					.OrderBy( el => el.Text )
					.ToArray();
				groupNode.Nodes.Clear();
				groupNode.Nodes.AddRange( nodesToSort );
				this.tv.SelectedNode = selectedNode;

				groupNode.Expand();
			} );
		}

		private TreeNode GetNetworkRoot()
		{
			return this.tv.Nodes.TreeNodesIterator()
				.FirstOrDefault( el => el.Text == cnstSNetworkNodeTitle );
		}

		public string[] GetNetworkShareList()
		{
			var root = GetNetworkRoot();
			if(root == null)
				return new string[0];
			else
				return root.Nodes.OfType<FsTreeNode>()
					.Select( el => el.Path )
					.ToArray();
		}

		public void RemoveSelectedShare()
		{
			if(this.IsSelectedPathIsShare)
			{
				this.tv.Nodes.Remove( this.tv.SelectedNode );

				TreeNode networkRoot = GetNetworkRoot();
				if(networkRoot != null && networkRoot.Nodes.Count == 0)
					this.tv.Nodes.Remove( networkRoot );
			}
		}
	}
}
