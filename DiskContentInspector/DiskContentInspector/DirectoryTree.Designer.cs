﻿namespace DiskContentInspector
{
	partial class DirectoryTree
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DirectoryTree));
			this.tv = new System.Windows.Forms.TreeView();
			this.imageList = new System.Windows.Forms.ImageList(this.components);
			this.SuspendLayout();
			// 
			// tv
			// 
			this.tv.HideSelection = false;
			this.tv.ImageIndex = 0;
			this.tv.ImageList = this.imageList;
			this.tv.Location = new System.Drawing.Point(33, 37);
			this.tv.Name = "tv";
			this.tv.SelectedImageIndex = 0;
			this.tv.ShowLines = false;
			this.tv.Size = new System.Drawing.Size(233, 212);
			this.tv.TabIndex = 0;
			this.tv.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.tv_BeforeExpand);
			this.tv.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tv_AfterSelect);
			// 
			// imageList
			// 
			this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
			this.imageList.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList.Images.SetKeyName(0, "Disk.ico");
			this.imageList.Images.SetKeyName(1, "Folder.ico");
			this.imageList.Images.SetKeyName(2, "SharedFolder.ico");
			this.imageList.Images.SetKeyName(3, "Network.ico");
			this.imageList.Images.SetKeyName(4, "Computer.ico");
			this.imageList.Images.SetKeyName(5, "NetworkDisk.ico");
			// 
			// DirectoryTree
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tv);
			this.Name = "DirectoryTree";
			this.Size = new System.Drawing.Size(311, 282);
			this.Load += new System.EventHandler(this.DirectoryTree_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TreeView tv;
		private System.Windows.Forms.ImageList imageList;
	}
}
