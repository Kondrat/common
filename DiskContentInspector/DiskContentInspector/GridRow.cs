﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using DiskContentInspector.Core;

namespace DiskContentInspector
{
	class GridRow
	{
		private readonly FsElementBase fsElement;

		public GridRow(FsElementBase fsElement)
		{
			this.fsElement = fsElement;
		}

		public string Name
		{
			get
			{
				return fsElement.Name;
			}
		}

		public string ElementType
		{
			get
			{
				return fsElement.ElementType.ToString();
			}
		}

		public string FullName
		{
			get
			{
				return fsElement.FullName;
			}
		}

		public string Ext
		{
			get
			{
				return fsElement.Ext;
			}
		}

		public string Size
		{
			get
			{
				return fsElement.Size.ToString("N");
			}
		}

		public string CountOfFilesTotal
		{
			get
			{
				return fsElement.CountOfFilesTotal.HasValue?
					fsElement.CountOfFilesTotal.Value.ToString("N")
					: string.Empty;
			}
		}

		public string CountOfFilesOneLevel
		{
			get
			{
				return fsElement.CountOfFilesOneLevel.HasValue ?
					fsElement.CountOfFilesOneLevel.Value.ToString("N")
					: string.Empty;
			}
		}

		public string CreationTime
		{
			get
			{
				return fsElement.CreationTime.ToString();
			}
		}

		public string LastChangeTime
		{
			get
			{
				return fsElement.LastChangeTime.ToString();
			}
		}

		public FileSystemAccessRule[] Acl
		{
			get
			{
				return fsElement.ACL;
			}
		}

		public string Md5
		{
			get
			{
				return fsElement.Md5;
			}
		}
	}
}
