﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DiskContentInspector
{
	public class FsTreeNode :TreeNode
	{
		private const string cnstEmptyNode = "*****empTy*****";

		private readonly string path;

		public FsTreeNode(string text, string path)
			:base( text )
		{
			this.path = path;

			try
			{
				try
				{
					var directoryInfo = new DirectoryInfo( path );
					if(directoryInfo.EnumerateDirectories().Count() > 0)
						this.Nodes.Add( cnstEmptyNode );
				}
				catch(IOException)
				{
					this.Nodes.Add(cnstEmptyNode);
				}
			}
			catch(UnauthorizedAccessException)
			{
				// Continue
			}
		}

		public string Path
		{
			get
			{
				return path;
			}
		}

		public void LoadSubItems()
		{
			using(SetCursor.Wait(this.TreeView.FindForm()))
			{
				if(this.Nodes.Count == 1 && this.Nodes[0].Text == cnstEmptyNode)
				{
					this.Nodes.Clear();

					var dir = new DirectoryInfo(this.path);
					foreach(var directory in dir.EnumerateDirectories())
					{
						FsTreeNode node = new FsTreeNode(directory.Name, directory.FullName);
						node.ImageIndex = node.SelectedImageIndex = 1;
						this.Nodes.Add(node);
					}
				}
			}
		}
	}
}
