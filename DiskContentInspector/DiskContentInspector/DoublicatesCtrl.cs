﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DiskContentInspector.Core;
using DiskContentInspector.Csv;

namespace DiskContentInspector
{
	public partial class DoublicatesCtrl :UserControl
	{
		private FileFsElement[] data;

		readonly Font fntSeparatop = null;
		readonly List<object> lstDoublicates = new List<object>();

		public DoublicatesCtrl()
		{
			InitializeComponent();
			this.fntSeparatop = new Font( this.lv.Font, FontStyle.Bold );
		}

		private void cb_CheckedChanged(object sender, EventArgs e)
		{
			this.btnFind.Enabled = cbFileName.Checked || cbFileSize.Checked || cbMd5.Checked;
		}

		internal void SetData(FileFsElement[] fileFsElements)
		{
			this.lv.VirtualListSize = 0;
			this.data = fileFsElements;
		}

		private static string KeySelector(IEnumerable<Func<FileFsElement, string>> selectors, FileFsElement fsElement)
		{
			string res = string.Empty;

			foreach(var selector in selectors)
			{
				string str = selector( fsElement );
				res += string.Format( "|{0}", str );
			}

			return res;
		}

		private void btnFind_Click(object sender, EventArgs e)
		{
			using(SetCursor.Wait( this.FindForm() ))
			{
				var selectors = new List<Func<FileFsElement, string>>();
				if(cbFileName.Checked)
					selectors.Add( el => el.Name );

				if(cbFileSize.Checked)
					selectors.Add( el => el.Size.ToString() );

				if(cbMd5.Checked)
					selectors.Add( el => el.Md5 );

				var seq = this.data
					.Where( el => cbMd5.Checked? IsMd5Hash( el.Md5 ): true )
					.GroupBy( el => KeySelector( selectors, el ), el => el )
					.Where( el => el.Count() > 1 )
					.OrderBy( el => el.Count() )
					.ToArray();

				lstDoublicates.Clear();

				foreach(IGrouping<string, FileFsElement> grp in seq)
				{
					lstDoublicates.Add( grp.Count() );
					lstDoublicates.AddRange( grp.OrderBy( el => el.Size ) );
				}

				lv.VirtualListSize = lstDoublicates.Count;
				btnSaveToCsv.Enabled = lstDoublicates.Count > 0;

				if(lstDoublicates.Count > 0)
					lv.AutoResizeColumns( ColumnHeaderAutoResizeStyle.ColumnContent );
			}
		}

		private unsafe static bool IsMd5Hash(string hash)
		{
			int len = hash.Length;
			if(len != 32)
				return false;

			fixed(char* str = hash)
			{
				for(int idx = 0; idx < len; idx++)
				{
					char* ch = str + idx;
					if(! IsHashAllowedChar( ch ))
						return false;
				}
			}

			return true;
		}

		private static unsafe bool IsHashAllowedChar(char* ch)
		{
			int code = *ch;

			if(code >= /*0*/0x30 && code <= /*9*/0x39)
				return true;

			if(code >= /*A*/0x41 && code <= /*F*/0x46)
				return true;

			if(code >= /*a*/0x61 && code <= /*f*/0x66)
				return true;

			return false;
		}

		private void lst_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
		{
			object oItem = lstDoublicates[e.ItemIndex];

			ListViewItem lvi = null;

			FileFsElement fs = oItem as FileFsElement;
			if(fs != null)
			{
				lvi = new ListViewItem(fs.Name);
				lvi.SubItems.Add(fs.Size.ToString());
				lvi.SubItems.Add(fs.Md5);
				lvi.SubItems.Add(fs.FullName);
			}
			else
			{
				int cnt = (int)oItem;

				lvi = new ListViewItem("Count: " + cnt.ToString())
				{
					Font = this.fntSeparatop
				};
				lvi.SubItems.Add( string.Empty );
				lvi.SubItems.Add( string.Empty );
				lvi.SubItems.Add( string.Empty );
				lvi.BackColor = Color.Gainsboro;
			}

			e.Item = lvi;
		}

		private void btnSaveToCsv_Click(object sender, EventArgs e)
		{
			try
			{
				if(sfd.ShowDialog(this) == DialogResult.OK)
				{
					using(SetCursor.Wait( this.FindForm() ))
					using(FileStream ostr = new FileStream( sfd.FileName, FileMode.Create ))
					{
						using(TextWriter wrt = new StreamWriter( ostr, Encoding.GetEncoding( 1251 ) ))
						{
							SaveCsv( lstDoublicates, wrt);
						}
					}
				}
			}
			catch(Exception ex)
			{
				MsgBoxes.ErrorDlg( this, ex.Message );
			}
		}

		private static void SaveCsv(IEnumerable<object> data, TextWriter wrt)
		{
			var csv = new CsvWriter( wrt, CsvDefines.ExcelCompatible );
			csv.WriteLine( "GroupNum", "CountInGroup", "Name", "Size", "Md5", "FullName");

			int count = 0, grpNum = 0;

			foreach(object item in data)
			{
				FileFsElement fs = item as FileFsElement;
				if(fs != null)
					csv.WriteLine( grpNum.ToString(), count.ToString(), fs.Name, fs.Size.ToString(), fs.Md5, fs.FullName );
				else
				{
					count = (int)item;
					grpNum++;
				}
			}
		}

		private void DoublicatesCtrl_VisibleChanged(object sender, EventArgs e)
		{
			if(this.Visible)
				this.btnSaveToCsv.Enabled = false;
		}

		private void mnuFile_Opening(object sender, CancelEventArgs e)
		{
			e.Cancel = !IsFileElementSelected;
		}

		private FileFsElement GetSelectedFileElement()
		{
			return (FileFsElement) lstDoublicates[lv.SelectedIndices[0]];
		}

		private bool IsFileElementSelected
		{
			get { return lv.SelectedIndices.Count == 1 && lstDoublicates[lv.SelectedIndices[0]] is FileFsElement; }
		}

		private void lv_DoubleClick(object sender, EventArgs e)
		{
			OpenInExplorer();
		}

		private void OpenInExplorer()
		{
			if(IsFileElementSelected)
			{
				var fileElement = GetSelectedFileElement();

				string argument = string.Format("/select, \"{0}\"", fileElement.FullName);
				System.Diagnostics.Process.Start("explorer.exe", argument);
			}
		}

		private void mnuOpenExplorer_Click(object sender, EventArgs e)
		{
			OpenInExplorer();
		}

		private void mnuOpenFile_Click(object sender, EventArgs e)
		{
			if (IsFileElementSelected)
			{
				var fileElement = GetSelectedFileElement();
				System.Diagnostics.Process.Start(fileElement.FullName);
			}
		}

		private void TryCopyToClipboard(Func<FileFsElement,string> textSelector)
		{
			if (IsFileElementSelected)
			{
				FileFsElement fileElement = GetSelectedFileElement();
				Clipboard.SetText(textSelector(fileElement));
			}
		}

		private void mnuCopyFilePath_Click(object sender, EventArgs e)
		{
			TryCopyToClipboard( el => el.FullName);
		}

		private void mnuCopyFolderPath_Click(object sender, EventArgs e)
		{
			TryCopyToClipboard(el => Path.GetDirectoryName(el.FullName));
		}

		private void mnuCopyFileName_Click(object sender, EventArgs e)
		{
			TryCopyToClipboard(el => Path.GetFileName(el.FullName));
		}
	}
}
