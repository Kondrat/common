﻿namespace DiskContentInspector
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if(disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.btnShowHideTree = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
			this.txtNeworkPath = new System.Windows.Forms.ToolStripTextBox();
			this.btnAddNetworkShare = new System.Windows.Forms.ToolStripLabel();
			this.btnRemoveShare = new System.Windows.Forms.ToolStripLabel();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btnMd5 = new System.Windows.Forms.ToolStripButton();
			this.btnScan = new System.Windows.Forms.ToolStripButton();
			this.btnStopScan = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.btnShowInheritedAcl = new System.Windows.Forms.ToolStripButton();
			this.btnShowFiles = new System.Windows.Forms.ToolStripButton();
			this.btnCopy = new System.Windows.Forms.ToolStripDropDownButton();
			this.btnCopyWithHeaders = new System.Windows.Forms.ToolStripMenuItem();
			this.btnCopyOnlyCells = new System.Windows.Forms.ToolStripMenuItem();
			this.btnExportToCsv = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.btnDoublicates = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.btnClose = new System.Windows.Forms.ToolStripButton();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.lblTaskName = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblScanTime = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblScanProgress = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblMd5Progress = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblRowCount = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblFsCount = new System.Windows.Forms.ToolStripStatusLabel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.dgw = new System.Windows.Forms.DataGridView();
			this.colRowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colExt = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colCountOfFilesOneLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colCountOfFilesTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colCreationTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colLastChangeTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colACL = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colMd5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.idleTimer = new System.Windows.Forms.Timer(this.components);
			this.sfd = new System.Windows.Forms.SaveFileDialog();
			this.splitterDoublicates = new System.Windows.Forms.Splitter();
			this.doublicatesCtrl = new DiskContentInspector.DoublicatesCtrl();
			this.directoryTree = new DiskContentInspector.DirectoryTree();
			this.toolStrip.SuspendLayout();
			this.statusStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgw)).BeginInit();
			this.SuspendLayout();
			// 
			// toolStrip
			// 
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnShowHideTree,
            this.toolStripSeparator4,
            this.toolStripLabel1,
            this.txtNeworkPath,
            this.btnAddNetworkShare,
            this.btnRemoveShare,
            this.toolStripSeparator2,
            this.btnMd5,
            this.btnScan,
            this.btnStopScan,
            this.toolStripSeparator3,
            this.btnShowInheritedAcl,
            this.btnShowFiles,
            this.btnCopy,
            this.btnExportToCsv,
            this.toolStripSeparator5,
            this.btnDoublicates,
            this.toolStripSeparator1,
            this.btnClose});
			this.toolStrip.Location = new System.Drawing.Point(0, 0);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(1319, 25);
			this.toolStrip.TabIndex = 0;
			this.toolStrip.Text = "toolStrip1";
			// 
			// btnShowHideTree
			// 
			this.btnShowHideTree.CheckOnClick = true;
			this.btnShowHideTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnShowHideTree.Image = ((System.Drawing.Image)(resources.GetObject("btnShowHideTree.Image")));
			this.btnShowHideTree.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnShowHideTree.Name = "btnShowHideTree";
			this.btnShowHideTree.Size = new System.Drawing.Size(91, 22);
			this.btnShowHideTree.Text = "Show/hide tree";
			this.btnShowHideTree.Click += new System.EventHandler(this.btnShowHideTree_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripLabel1
			// 
			this.toolStripLabel1.Name = "toolStripLabel1";
			this.toolStripLabel1.Size = new System.Drawing.Size(76, 22);
			this.toolStripLabel1.Text = "Share to add:";
			// 
			// txtNeworkPath
			// 
			this.txtNeworkPath.Name = "txtNeworkPath";
			this.txtNeworkPath.Size = new System.Drawing.Size(250, 25);
			this.txtNeworkPath.TextChanged += new System.EventHandler(this.txtNeworkPath_TextChanged);
			// 
			// btnAddNetworkShare
			// 
			this.btnAddNetworkShare.Enabled = false;
			this.btnAddNetworkShare.Name = "btnAddNetworkShare";
			this.btnAddNetworkShare.Size = new System.Drawing.Size(60, 22);
			this.btnAddNetworkShare.Text = "Add share";
			this.btnAddNetworkShare.Click += new System.EventHandler(this.btnAddNetworkShare_Click);
			// 
			// btnRemoveShare
			// 
			this.btnRemoveShare.Enabled = false;
			this.btnRemoveShare.Name = "btnRemoveShare";
			this.btnRemoveShare.Size = new System.Drawing.Size(71, 22);
			this.btnRemoveShare.Text = "Delete share";
			this.btnRemoveShare.Click += new System.EventHandler(this.btnRemoveShare_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// btnMd5
			// 
			this.btnMd5.CheckOnClick = true;
			this.btnMd5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnMd5.Image = ((System.Drawing.Image)(resources.GetObject("btnMd5.Image")));
			this.btnMd5.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnMd5.Name = "btnMd5";
			this.btnMd5.Size = new System.Drawing.Size(139, 22);
			this.btnMd5.Text = "Calc MD5 for files (slow)";
			this.btnMd5.Click += new System.EventHandler(this.btnMd5_Click);
			// 
			// btnScan
			// 
			this.btnScan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnScan.Image = ((System.Drawing.Image)(resources.GetObject("btnScan.Image")));
			this.btnScan.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnScan.Name = "btnScan";
			this.btnScan.Size = new System.Drawing.Size(53, 22);
			this.btnScan.Text = "Do scan";
			this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
			// 
			// btnStopScan
			// 
			this.btnStopScan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnStopScan.Enabled = false;
			this.btnStopScan.Image = ((System.Drawing.Image)(resources.GetObject("btnStopScan.Image")));
			this.btnStopScan.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnStopScan.Name = "btnStopScan";
			this.btnStopScan.Size = new System.Drawing.Size(86, 22);
			this.btnStopScan.Text = "Stop scanning";
			this.btnStopScan.Click += new System.EventHandler(this.btnStopScan_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// btnShowInheritedAcl
			// 
			this.btnShowInheritedAcl.CheckOnClick = true;
			this.btnShowInheritedAcl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnShowInheritedAcl.Image = ((System.Drawing.Image)(resources.GetObject("btnShowInheritedAcl.Image")));
			this.btnShowInheritedAcl.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnShowInheritedAcl.Name = "btnShowInheritedAcl";
			this.btnShowInheritedAcl.Size = new System.Drawing.Size(143, 22);
			this.btnShowInheritedAcl.Text = "Show inherited ACL rules";
			this.btnShowInheritedAcl.Click += new System.EventHandler(this.btnShowInheritedAcl_Click);
			// 
			// btnShowFiles
			// 
			this.btnShowFiles.CheckOnClick = true;
			this.btnShowFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnShowFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnShowFiles.Name = "btnShowFiles";
			this.btnShowFiles.Size = new System.Drawing.Size(64, 22);
			this.btnShowFiles.Text = "Show files";
			this.btnShowFiles.Click += new System.EventHandler(this.btnShowFiles_Click);
			// 
			// btnCopy
			// 
			this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnCopy.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCopyWithHeaders,
            this.btnCopyOnlyCells});
			this.btnCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnCopy.Image")));
			this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnCopy.Name = "btnCopy";
			this.btnCopy.Size = new System.Drawing.Size(87, 22);
			this.btnCopy.Text = "To clipboard";
			// 
			// btnCopyWithHeaders
			// 
			this.btnCopyWithHeaders.Name = "btnCopyWithHeaders";
			this.btnCopyWithHeaders.Size = new System.Drawing.Size(160, 22);
			this.btnCopyWithHeaders.Text = "With header text";
			this.btnCopyWithHeaders.Click += new System.EventHandler(this.btnCopyWithHeaders_Click);
			// 
			// btnCopyOnlyCells
			// 
			this.btnCopyOnlyCells.Name = "btnCopyOnlyCells";
			this.btnCopyOnlyCells.Size = new System.Drawing.Size(160, 22);
			this.btnCopyOnlyCells.Text = "Only cells";
			this.btnCopyOnlyCells.Click += new System.EventHandler(this.btnCopyOnlyCells_Click);
			// 
			// btnExportToCsv
			// 
			this.btnExportToCsv.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnExportToCsv.Image = ((System.Drawing.Image)(resources.GetObject("btnExportToCsv.Image")));
			this.btnExportToCsv.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnExportToCsv.Name = "btnExportToCsv";
			this.btnExportToCsv.Size = new System.Drawing.Size(82, 22);
			this.btnExportToCsv.Text = "Export to CSV";
			this.btnExportToCsv.Click += new System.EventHandler(this.btnExportToCsv_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
			// 
			// btnDoublicates
			// 
			this.btnDoublicates.CheckOnClick = true;
			this.btnDoublicates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnDoublicates.Image = ((System.Drawing.Image)(resources.GetObject("btnDoublicates.Image")));
			this.btnDoublicates.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDoublicates.Name = "btnDoublicates";
			this.btnDoublicates.Size = new System.Drawing.Size(73, 19);
			this.btnDoublicates.Text = "Doublicates";
			this.btnDoublicates.Click += new System.EventHandler(this.btnDoublicates_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// btnClose
			// 
			this.btnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
			this.btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(29, 19);
			this.btnClose.Text = "Exit";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblTaskName,
            this.lblScanTime,
            this.lblScanProgress,
            this.lblMd5Progress,
            this.lblRowCount,
            this.lblFsCount});
			this.statusStrip.Location = new System.Drawing.Point(0, 635);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(1319, 24);
			this.statusStrip.TabIndex = 1;
			this.statusStrip.Text = "statusStrip1";
			// 
			// lblTaskName
			// 
			this.lblTaskName.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
			this.lblTaskName.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
			this.lblTaskName.Name = "lblTaskName";
			this.lblTaskName.Size = new System.Drawing.Size(40, 19);
			this.lblTaskName.Text = "None";
			// 
			// lblScanTime
			// 
			this.lblScanTime.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
			this.lblScanTime.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
			this.lblScanTime.Name = "lblScanTime";
			this.lblScanTime.Size = new System.Drawing.Size(20, 19);
			this.lblScanTime.Text = "...";
			// 
			// lblScanProgress
			// 
			this.lblScanProgress.Name = "lblScanProgress";
			this.lblScanProgress.Size = new System.Drawing.Size(1244, 19);
			this.lblScanProgress.Spring = true;
			this.lblScanProgress.Text = "..";
			this.lblScanProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMd5Progress
			// 
			this.lblMd5Progress.AutoSize = false;
			this.lblMd5Progress.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
			this.lblMd5Progress.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.lblMd5Progress.Name = "lblMd5Progress";
			this.lblMd5Progress.Size = new System.Drawing.Size(350, 19);
			this.lblMd5Progress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblMd5Progress.Visible = false;
			// 
			// lblRowCount
			// 
			this.lblRowCount.Name = "lblRowCount";
			this.lblRowCount.Size = new System.Drawing.Size(0, 19);
			// 
			// lblFsCount
			// 
			this.lblFsCount.Name = "lblFsCount";
			this.lblFsCount.Size = new System.Drawing.Size(0, 19);
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(277, 25);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 610);
			this.splitter1.TabIndex = 3;
			this.splitter1.TabStop = false;
			// 
			// dgw
			// 
			this.dgw.AllowUserToAddRows = false;
			this.dgw.AllowUserToDeleteRows = false;
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.dgw.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
			this.dgw.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
			this.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgw.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colRowNumber,
            this.colType,
            this.colName,
            this.colExt,
            this.colFullName,
            this.colSize,
            this.colCountOfFilesOneLevel,
            this.colCountOfFilesTotal,
            this.colCreationTime,
            this.colLastChangeTime,
            this.colACL,
            this.colMd5});
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dgw.DefaultCellStyle = dataGridViewCellStyle3;
			this.dgw.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgw.GridColor = System.Drawing.SystemColors.ControlLight;
			this.dgw.Location = new System.Drawing.Point(280, 25);
			this.dgw.Name = "dgw";
			this.dgw.ReadOnly = true;
			this.dgw.RowHeadersVisible = false;
			this.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgw.Size = new System.Drawing.Size(1039, 400);
			this.dgw.TabIndex = 5;
			this.dgw.VirtualMode = true;
			this.dgw.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgw_CellDoubleClick);
			this.dgw.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dgw_CellValueNeeded);
			// 
			// colRowNumber
			// 
			this.colRowNumber.HeaderText = "№";
			this.colRowNumber.Name = "colRowNumber";
			this.colRowNumber.ReadOnly = true;
			this.colRowNumber.Width = 43;
			// 
			// colType
			// 
			this.colType.HeaderText = "Type";
			this.colType.Name = "colType";
			this.colType.ReadOnly = true;
			// 
			// colName
			// 
			this.colName.HeaderText = "Name";
			this.colName.Name = "colName";
			this.colName.ReadOnly = true;
			// 
			// colExt
			// 
			this.colExt.HeaderText = "Ext";
			this.colExt.Name = "colExt";
			this.colExt.ReadOnly = true;
			// 
			// colFullName
			// 
			this.colFullName.HeaderText = "FullName";
			this.colFullName.Name = "colFullName";
			this.colFullName.ReadOnly = true;
			// 
			// colSize
			// 
			this.colSize.HeaderText = "Size";
			this.colSize.Name = "colSize";
			this.colSize.ReadOnly = true;
			// 
			// colCountOfFilesOneLevel
			// 
			this.colCountOfFilesOneLevel.HeaderText = "Files count";
			this.colCountOfFilesOneLevel.Name = "colCountOfFilesOneLevel";
			this.colCountOfFilesOneLevel.ReadOnly = true;
			// 
			// colCountOfFilesTotal
			// 
			this.colCountOfFilesTotal.HeaderText = "Files count (total)";
			this.colCountOfFilesTotal.Name = "colCountOfFilesTotal";
			this.colCountOfFilesTotal.ReadOnly = true;
			// 
			// colCreationTime
			// 
			this.colCreationTime.HeaderText = "Creation time";
			this.colCreationTime.Name = "colCreationTime";
			this.colCreationTime.ReadOnly = true;
			// 
			// colLastChangeTime
			// 
			this.colLastChangeTime.HeaderText = "Last change time";
			this.colLastChangeTime.Name = "colLastChangeTime";
			this.colLastChangeTime.ReadOnly = true;
			// 
			// colACL
			// 
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.colACL.DefaultCellStyle = dataGridViewCellStyle2;
			this.colACL.HeaderText = "ACL";
			this.colACL.Name = "colACL";
			this.colACL.ReadOnly = true;
			// 
			// colMd5
			// 
			this.colMd5.HeaderText = "MD5";
			this.colMd5.Name = "colMd5";
			this.colMd5.ReadOnly = true;
			// 
			// idleTimer
			// 
			this.idleTimer.Enabled = true;
			this.idleTimer.Interval = 500;
			this.idleTimer.Tick += new System.EventHandler(this.idleTimer_Tick);
			// 
			// sfd
			// 
			this.sfd.DefaultExt = "csv";
			this.sfd.Title = "Save .csv";
			// 
			// splitterDoublicates
			// 
			this.splitterDoublicates.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitterDoublicates.Location = new System.Drawing.Point(280, 425);
			this.splitterDoublicates.Name = "splitterDoublicates";
			this.splitterDoublicates.Size = new System.Drawing.Size(1039, 3);
			this.splitterDoublicates.TabIndex = 7;
			this.splitterDoublicates.TabStop = false;
			this.splitterDoublicates.Visible = false;
			// 
			// doublicatesCtrl
			// 
			this.doublicatesCtrl.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.doublicatesCtrl.Location = new System.Drawing.Point(280, 428);
			this.doublicatesCtrl.Name = "doublicatesCtrl";
			this.doublicatesCtrl.Size = new System.Drawing.Size(1039, 207);
			this.doublicatesCtrl.TabIndex = 6;
			this.doublicatesCtrl.Visible = false;
			// 
			// directoryTree
			// 
			this.directoryTree.Dock = System.Windows.Forms.DockStyle.Left;
			this.directoryTree.Location = new System.Drawing.Point(0, 25);
			this.directoryTree.Name = "directoryTree";
			this.directoryTree.Size = new System.Drawing.Size(277, 610);
			this.directoryTree.TabIndex = 4;
			this.directoryTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.directoryTree_AfterSelect);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1319, 659);
			this.Controls.Add(this.dgw);
			this.Controls.Add(this.splitterDoublicates);
			this.Controls.Add(this.doublicatesCtrl);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.directoryTree);
			this.Controls.Add(this.toolStrip);
			this.Controls.Add(this.statusStrip);
			this.MinimumSize = new System.Drawing.Size(1260, 680);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Disc Content Inspector";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgw)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton btnClose;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.ToolStripLabel btnAddNetworkShare;
		private DirectoryTree directoryTree;
		private System.Windows.Forms.ToolStripButton btnScan;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton btnShowFiles;
		private System.Windows.Forms.ToolStripButton btnMd5;
		private System.Windows.Forms.DataGridView dgw;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton btnExportToCsv;
		private System.Windows.Forms.ToolStripButton btnStopScan;
		private System.Windows.Forms.ToolStripStatusLabel lblScanProgress;
		private System.Windows.Forms.ToolStripStatusLabel lblTaskName;
		private System.Windows.Forms.Timer idleTimer;
		private System.Windows.Forms.ToolStripStatusLabel lblScanTime;
		private System.Windows.Forms.ToolStripButton btnShowHideTree;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripStatusLabel lblRowCount;
		private System.Windows.Forms.ToolStripStatusLabel lblFsCount;
		private System.Windows.Forms.ToolStripButton btnShowInheritedAcl;
		private System.Windows.Forms.SaveFileDialog sfd;
		private System.Windows.Forms.ToolStripTextBox txtNeworkPath;
		private System.Windows.Forms.ToolStripLabel btnRemoveShare;
		private System.Windows.Forms.ToolStripDropDownButton btnCopy;
		private System.Windows.Forms.ToolStripMenuItem btnCopyWithHeaders;
		private System.Windows.Forms.ToolStripMenuItem btnCopyOnlyCells;
		private System.Windows.Forms.ToolStripLabel toolStripLabel1;
		private System.Windows.Forms.DataGridViewTextBoxColumn colRowNumber;
		private System.Windows.Forms.DataGridViewTextBoxColumn colType;
		private System.Windows.Forms.DataGridViewTextBoxColumn colName;
		private System.Windows.Forms.DataGridViewTextBoxColumn colExt;
		private System.Windows.Forms.DataGridViewTextBoxColumn colFullName;
		private System.Windows.Forms.DataGridViewTextBoxColumn colSize;
		private System.Windows.Forms.DataGridViewTextBoxColumn colCountOfFilesOneLevel;
		private System.Windows.Forms.DataGridViewTextBoxColumn colCountOfFilesTotal;
		private System.Windows.Forms.DataGridViewTextBoxColumn colCreationTime;
		private System.Windows.Forms.DataGridViewTextBoxColumn colLastChangeTime;
		private System.Windows.Forms.DataGridViewTextBoxColumn colACL;
		private System.Windows.Forms.DataGridViewTextBoxColumn colMd5;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripButton btnDoublicates;
		private DoublicatesCtrl doublicatesCtrl;
		private System.Windows.Forms.Splitter splitterDoublicates;
		private System.Windows.Forms.ToolStripStatusLabel lblMd5Progress;
	}
}

