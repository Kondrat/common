﻿namespace DiskContentInspector.Csv
{
	public sealed class CsvDefines
	{
		public const string cnst_LineSeparator = @"
";
		public const char cnst_FieldSeparator = ',';
		public const char cnst_Quote = '\"';

		public string LineSeparator
		{ 
			get; private set;
		}

		public char FieldSeparator 
		{ 
			get; private set;
		}

		public char Quote
		{
			get;
			private set;
		}

		public CsvDefines(string lineSeparator, char fieldSeparator, char quote)
		{
			LineSeparator = lineSeparator;
			FieldSeparator = fieldSeparator;
			Quote = quote;
		}

		#region Factory's

		public static CsvDefines ExcelCompatible
		{
			get
			{
				return new CsvDefines(@"
", ';', '\"');
			}
		}

		#endregion

	}
}