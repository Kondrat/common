﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DiskContentInspector.Core
{
	class DataCollector
	{
		const int cnstBigFileSize = /*in bytes*/ 1 * 1024 * 1024;

		private struct Pair
		{
			public Pair(FileInfo fileInfo, FileFsElement fileFsElement)
			{
				this.fileInfo = fileInfo;
				this.fileFsElement = fileFsElement;
			}

			public readonly FileInfo fileInfo;
			public readonly FileFsElement fileFsElement;
		} 

		readonly MyCountEvent md5Event = new MyCountEvent();

		private readonly Action<string> errorNotification;
		private readonly Md5Provider md5Provider = new Md5Provider();
		private readonly AutoResetEvent stopWait = new AutoResetEvent(false);
		private volatile bool bNeedStop = false;

		private readonly List<Pair> listOfbigFiles = new List<Pair>();

		public DataCollector(Action<string> errorNotification)
		{
			this.errorNotification = errorNotification;
		}

		public Tree<FsElementBase> Read(string rootFolder, bool bCalcMd5, Action<string, string> scanProgressNotification, Action<string> md5ProgressNotification)
		{
			Tree<FsElementBase> tree;
			this.bNeedStop = false;

			this.md5Event.Reset();

			try
			{
				var di = new DirectoryInfo( rootFolder );

				var root = new FolderFsElement(rootFolder, new Tree<FsElementBase>(), di.CreationTime, di.LastWriteTime, GetAcl(di));
				ReadInner(root.Node, bCalcMd5, scanProgressNotification, md5ProgressNotification);

				if(this.bNeedStop)
				{
					this.stopWait.Set();
					tree = null;
				}
				else
					tree = root.Node;

				if(bCalcMd5)
				{
					scanProgressNotification( "md5", "Waiting md5..." );

					if(this.md5Event.CountOfProcessingFiles > 0)
						this.md5Event.WaitOne();

					if(listOfbigFiles.Count > 0)
					{
						if(md5ProgressNotification != null)
							md5ProgressNotification( string.Empty );

						scanProgressNotification("md5", "Waiting md5 (large files)...");
						int remainCount = listOfbigFiles.Count;

						foreach(Pair pair in listOfbigFiles)
						{
							string msg = string.Format( "Remains - {0}; {1} (size {2})", remainCount, pair.fileInfo.Name, FormatFileSize( pair.fileInfo.Length ) );
							scanProgressNotification( "md5: large files", msg );

							if(bNeedStop)
								break;

							remainCount--;
							pair.fileFsElement.SetMd5( CalculateMd5( pair.fileInfo ) );
						}
					}
				}
			}
			finally
			{
				md5Provider.Clear();
				listOfbigFiles.Clear();
			}

			return tree;
		}

		static readonly string[] s_fileSizeSuffix = { "B", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb" }; //Longs run out around EB
		private static string FormatFileSize(long length)
		{
			if(length == 0)
				return "0" + s_fileSizeSuffix[0];
			long bytes = Math.Abs(length);
			int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
			double num = Math.Round(bytes / Math.Pow(1024, place), 1);
			return (Math.Sign(length) * num).ToString() + " " + s_fileSizeSuffix[place];
		}

		private static FileSystemAccessRule[] GetAcl(DirectoryInfo di)
		{
			try
			{
				AuthorizationRuleCollection rules = di.GetAccessControl()
					.GetAccessRules( true, true, typeof(NTAccount) );

				return rules.OfType<FileSystemAccessRule>()
					.Where(el => el.IdentityReference.Value.ToLower().StartsWith("mbp"))
					.ToArray();
			}
			catch(Exception)
			{
				return new FileSystemAccessRule[0];
			}
		}

		private static FileSystemAccessRule[] GetAcl(FileInfo fi)
		{
			try
			{
				AuthorizationRuleCollection rules = fi.GetAccessControl()
					.GetAccessRules(true, true, typeof(NTAccount));

				return rules.OfType<FileSystemAccessRule>()
					.Where(el => el.IdentityReference.Value.ToLower().StartsWith( "mbp" ))
					.ToArray();
			}
			catch(Exception)
			{
				return new FileSystemAccessRule[0];
			}
		}

		private void ReadInner(Tree<FsElementBase> parentNode, bool bCalcMd5, Action<string, string> progressNotification, Action<string> md5ProgressNotification)
		{
			if(this.bNeedStop)
				return;

			if(progressNotification != null)
				progressNotification( "Scanning...", parentNode.Value.FullName );

			try
			{
				DirectoryInfo di = new DirectoryInfo( parentNode.Value.FullName );
				FolderFsElement[] folders =  di.GetDirectories()
					.Select(el => new
							{ 
								Name = Path.GetFileName( el.FullName ),
								CreationTime = el.CreationTime,
								LastWriteTime = el.LastWriteTime,
								Acl = GetAcl( el )
							})
					.Select( el => new FolderFsElement( el.Name, new Tree<FsElementBase>(), el.CreationTime, el.LastWriteTime, el.Acl ) )
					.ToArray();

				Tree<FsElementBase>[] folderNodes = folders
					.Select( el => el.Node )
					.ToArray();
				parentNode.AddRange(folderNodes);

				if(this.bNeedStop)
					return;

				foreach(var folderNode in folderNodes)
				{
					ReadInner( folderNode, bCalcMd5, progressNotification, md5ProgressNotification );
				}

				if(this.bNeedStop)
					return;

				var fileInfos = new DirectoryInfo( parentNode.Value.FullName )
					.GetFiles();

				foreach(FileInfo fileInfo in fileInfos)
				{
					if(this.bNeedStop)
						return;

					FileFsElement elem = new FileFsElement(
						Path.GetFileName( fileInfo.Name ),
						new Tree<FsElementBase>(),
						fileInfo.Length,
						fileInfo.CreationTime,
						fileInfo.LastWriteTime,
						/*md5*/string.Empty,
						GetAcl(fileInfo)
						);

					if(bCalcMd5)
					{
						if(fileInfo.Length <= cnstBigFileSize)
						{
							// Run small files async...
							this.md5Event.AddCount();

							FileInfo fInfo = fileInfo;
							var task = new Task( () =>
							{
								if(!bNeedStop)
								{
									if(md5ProgressNotification != null)
										md5ProgressNotification( "Md5: " + fInfo.Name );

									string strMd5 = CalculateMd5( fInfo );
									elem.SetMd5( strMd5 );
								}

								this.md5Event.Signal();
							} );

							task.Start();
						}
						else
							listOfbigFiles.Add( new Pair(fileInfo, elem) );
					}

					parentNode.AddNode( elem.Node );
				}
			}
			catch(UnauthorizedAccessException ex)
			{
				NotificateForError( ex );
			}
		}

		private string CalculateMd5(FileInfo fileInfo)
		{
			try
			{
				using(Stream istr = fileInfo.OpenRead())
				{
					var md5 = this.md5Provider.GetMd5();
					byte[] hash = null;

					try
					{
						BufferedStream bs = new BufferedStream( istr );
						hash = md5.ComputeHash(bs);
					}
					finally
					{
						this.md5Provider.ReturnMd5( md5 );
					}

					StringBuilder sb = new StringBuilder(hash.Length * 2);

					foreach(byte b in hash)
					{
						sb.AppendFormat("{0:X2}", b);
					}

					return sb.ToString();
				}
			}
			catch(Exception ex)
			{
				NotificateForError( ex );
				return "ERR:" + ex.Message;
			}
		}

		private void NotificateForError(Exception ex)
		{
			if(this.errorNotification != null)
				this.errorNotification( ex.Message );
		}

		public WaitHandle RequestStop()
		{
			this.stopWait.Reset();
			this.md5Event.Set();
			this.bNeedStop = true;
			return this.stopWait;
		}
	}
}
