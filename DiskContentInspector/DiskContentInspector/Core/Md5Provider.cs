﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace DiskContentInspector.Core
{
	class Md5Provider
	{
		readonly object sync = new object();

		private readonly Stack<MD5> stack = new Stack<MD5>();

		public MD5 GetMd5()
		{
			try
			{
				lock(sync)
				{
					return stack.Pop();
				}
			}
			catch(InvalidOperationException)
			{
				MD5 md5 = MD5.Create();
				return md5;
			}
		}

		public void ReturnMd5(MD5 md5)
		{
			lock(sync)
			{
				stack.Push( md5 );
			}
		}

		public void Clear()
		{
			foreach(MD5 md5 in stack)
			{
				md5.Clear();
			}

			stack.Clear();
		}
	}
}
