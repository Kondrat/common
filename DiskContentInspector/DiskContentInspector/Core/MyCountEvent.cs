﻿using System.Threading;

namespace DiskContentInspector.Core
{
	class MyCountEvent 
		:EventWaitHandle 
	{
		private int count = 0;

		public MyCountEvent(): 
			base( false, EventResetMode.ManualReset )
		{
			// Empty
		}

		public void AddCount()
		{
			Interlocked.Increment( ref this.count );
			base.Reset();
		}

		public void Signal()
		{
			int res = Interlocked.Decrement( ref this.count );
			if(res == 0)
				base.Set();
		}

		public int CountOfProcessingFiles
		{
			get { return count; }
		}
	}
}
