﻿using System;
using System.IO;
using System.Security.AccessControl;

namespace DiskContentInspector.Core
{
	internal class FileFsElement: FsElementBase
	{
		private readonly long size;
		private readonly DateTime creationTime;
		private readonly DateTime lastChangeTime;
		private string md5;

		public FileFsElement(string name, Tree<FsElementBase> node, long size, DateTime creationTime, DateTime lastChangeTime, string md5, FileSystemAccessRule[] acl)
			:base( name, Type.File, acl, node )
		{
			// Empty
			this.size = size;
			this.md5 = md5;
			this.lastChangeTime = lastChangeTime;
			this.creationTime = creationTime;
		}

		#region Overrides of FsElementBase

		public override string Ext
		{
			get
			{
				return Path.GetExtension( this.Name )
					.TrimStart( '.' )
					.ToLower();
			}
		}

		public override long Size
		{
			get
			{
				return this.size;
			}
		}

		public override long? CountOfFilesTotal
		{
			get
			{
				return null;
			}
		}

		public override long? CountOfFilesOneLevel
		{
			get
			{
				return null;
			}
		}

		public override DateTime CreationTime
		{
			get
			{
				return this.creationTime;
			}
		}

		public override DateTime LastChangeTime
		{
			get
			{
				return this.lastChangeTime;
			}
		}

		public override string Md5
		{
			get
			{
				return this.md5;
			}
		}

		internal void SetMd5(string md5)
		{
			this.md5 = md5;
		}

		#endregion
	}
}