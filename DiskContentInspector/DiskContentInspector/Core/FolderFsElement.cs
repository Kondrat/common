﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;

namespace DiskContentInspector.Core
{
	internal class FolderFsElement: FsElementBase
	{
		public FolderFsElement(string name, Tree<FsElementBase> node, DateTime creationTime, DateTime lastChangeTime, FileSystemAccessRule[] acl)
			: base( name, Type.Folder, acl, node )
		{
			this.creationTime = creationTime;
			this.lastChangeTime = lastChangeTime;
		}

		private IEnumerable<Tree<FsElementBase>> IteratorChildFilesAllLevels
		{
			get
			{
				return this.Node.AllLevelChildNodesIterator
					.Where(el => el.Value.ElementType == Type.File);
			}
		}

		private IEnumerable<Tree<FsElementBase>> IteratorChildFilesOneLevel
		{
			get
			{
				return this.Node.ChildNodesAtLevelIterator
					.Where(el => el.Value.ElementType == Type.File);
			}
		}

		#region Overrides of FsElementBase

		public override string Ext
		{
			get
			{
				return string.Empty;
			}
		}

		private long size = -1;
		public override long Size
		{
			get
			{
				if(this.size == -1)
				{
					this.size = this.IteratorChildFilesAllLevels
						.Select( el => el.Value.Size )
						.Sum();
				}
				return this.size;
			}
		}

		private long countOfFilesTotal = -1;
		public override long? CountOfFilesTotal
		{
			get
			{
				if(this.countOfFilesTotal == -1)
				{
					this.countOfFilesTotal = this.IteratorChildFilesAllLevels
						.Count();
				}
				return this.countOfFilesTotal;
			}
		}

		private long countOfFilesOneLevel = -1;
		public override long? CountOfFilesOneLevel
		{
			get
			{
				if(this.countOfFilesOneLevel == -1)
				{
					this.countOfFilesOneLevel = this.IteratorChildFilesOneLevel
						.Count();
				}
				return this.countOfFilesOneLevel;
			}
		}

		private readonly DateTime creationTime;
		public override DateTime CreationTime
		{
			get
			{
				return creationTime;
			}
		}

		private readonly DateTime lastChangeTime;
		private DateTime? lastChangeTimeFromFiles = null;
		public override DateTime LastChangeTime
		{
			get
			{
				if(this.CountOfFilesTotal == 0)
					return lastChangeTime;
				else
				{
					if(! lastChangeTimeFromFiles.HasValue)
					{
						lastChangeTimeFromFiles = IteratorChildFilesAllLevels
							.Select( el => el.Value.LastChangeTime )
							.Max();
					}
					return lastChangeTimeFromFiles.Value;
				}
			}
		}

		public override string Md5
		{
			get
			{
				return string.Empty;
			}
		}

		#endregion
	}
}