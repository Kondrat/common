﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace DiskContentInspector.Core
{
	[DebuggerDisplay("Level={Level}, Childs={CountChilds}, Value={Value}")]
	public class Tree<TValue>: ICloneable
	{
		#region Constructors

		public Tree()
		{
			// Empty
		}

		public Tree(TValue value)
		{
			this.nodeValue = value;
		}

		#endregion

		#region Value

		private TValue nodeValue;
		/// <summary>
		/// Величина, хранящаяся в узле
		/// </summary>
		public TValue Value
		{
			get { return this.nodeValue; }
			set { this.nodeValue = value; }
		}

		#endregion

		#region Level

		private int level = 0;
		/// <summary>
		/// Уровень вложенности
		/// </summary>
		public int Level
		{
			get { return this.level; }
		}

		#endregion

		#region IsLeaf

		/// <summary>
		/// true, если узел листьевой, т.е. не имеет подчинённых узлов
		/// </summary>
		public bool IsLeaf
		{
			get
			{
				return this.CountChildsAtLevel == 0;
			}
		}

		#endregion

		#region Nodes

		private readonly List<Tree<TValue>> nodes = new List<Tree<TValue>>();

		/// <summary>
		/// Добавление узла
		/// </summary>
		/// <param name="value">Хранимое значение</param>
		/// <returns>Добавленный узел</returns>
		public Tree<TValue> AddNode(TValue value)
		{
			return this.AddNode( new Tree<TValue>( value ) );
		}

		/// <summary>
		/// Добавление узла
		/// </summary>
		/// <param name="nodeToAdd">Узел для добавления</param>
		/// <returns>Добавленный узел (совпадает с узлом для добавления)</returns>
		public Tree<TValue> AddNode(Tree<TValue> nodeToAdd)
		{
			if(nodeToAdd.parent != null)
				throw new ArgumentException("Node already assigned!");

			nodeToAdd.parent = this;
			nodeToAdd.level = this.level + 1;
			this.nodes.Add( nodeToAdd );
			return nodeToAdd;
		}

		/// <summary>
		/// Добавление узлов
		/// </summary>
		/// <param name="nodesToAdd">Узлы для добавления</param>
		public void AddRange( IEnumerable<Tree<TValue>> nodesToAdd)
		{
			foreach(Tree<TValue> node in nodesToAdd)
			{
				this.AddNode( node );
			}
		}

		/// <summary>
		/// Добавление объектов
		/// </summary>
		/// <param name="valuesToAdd">Объекты для добавления</param>
		public void AddRange( IEnumerable<TValue> valuesToAdd)
		{
			foreach(TValue value in valuesToAdd)
			{
				this.AddNode( new Tree<TValue>(value) );
			}
		}

		/// <summary>
		/// Возвращает узел в позиции
		/// </summary>
		/// <param name="idx">индекс</param>
		/// <returns>Узел в позиции</returns>
		public Tree<TValue> NodeAt(int idx)
		{
			return this.nodes[idx];
		}

		/// <summary>
		/// Удаление узла
		/// </summary>
		/// <param name="node">Узел для удаления</param>
		/// <returns>true - если удаление прошло успешно</returns>
		public bool RemoveChildNode(Tree<TValue> node)
		{
			if(this.nodes.Remove(node))
			{
				node.parent = null;
				node.level = 0;
				return true;
			}
			return false;
		}

		/// <summary>
		/// Удаление из позиции
		/// </summary>
		/// <param name="idx">Позиция</param>
		/// <returns>Удалённый узел</returns>
		public Tree<TValue> RemoveAt(int idx)
		{
			Tree<TValue> node = this.NodeAt( idx );
			if(this.RemoveChildNode( node ))
				return node;
			else
				return null;
		}

		/// <summary>
		/// Кол-во подузлов на узле этого уровня
		/// </summary>
		public int CountChildsAtLevel
		{
			get
			{
				return this.nodes.Count;
			}
		}

		/// <summary>
		/// Кол-во всех подузлов на всех уровнях
		/// </summary>
		public int CountChilds
		{
			get
			{
				return this.AllLevelChildNodesIterator.Count();
			}
		}

		/// <summary>
		/// Итератор для перечисления непосредственных дочерних узлов
		/// </summary>
		public IEnumerable<Tree<TValue>> ChildNodesAtLevelIterator
		{
			get
			{
				foreach(Tree<TValue> node in nodes)
				{
					yield return node;
				}
			}
		}

		/// <summary>
		/// Итератор для перечисления узлов на всех подуровнях (в т.ч. корневого)
		/// </summary>
		public IEnumerable<Tree<TValue>> AllNodesIterator
		{
			get
			{
				yield return this;
				foreach(Tree<TValue> tree in this.AllLevelChildNodesIterator)
				{
					yield return tree;
				}
			}
		}

		/// <summary>
		/// Итератор для перечисления узлов на всех подуровнях (без корневого)
		/// </summary>
		public IEnumerable<Tree<TValue>> AllLevelChildNodesIterator
		{
			get
			{
				foreach(Tree<TValue> tree in this.ChildNodesAtLevelIterator)
				{
					yield return tree;
					foreach(var node in tree.AllLevelChildNodesIterator)
					{
						yield return node;
					}
				}
			}
		}

		/// <summary>
		/// Итератор для перечисления узлов от текущего до корневого
		/// </summary>
		public IEnumerable<Tree<TValue>> FromThisToParentIterator
		{
			get
			{
				var currNode = this;

				do
				{
					yield return currNode;
					currNode = currNode.parent;
				} 
				while(currNode != null);
			}
		}

		#endregion

		#region Parent

		private Tree<TValue> parent = null;
		/// <summary>
		/// Родительский узел
		/// </summary>
		public Tree<TValue> Parent
		{
			get
			{
				return this.parent;
			}
		}

		#endregion

		#region Clone

		public object Clone()
		{
			return this.CloneTree();
		}

		public Tree<TValue> CloneTree()
		{
			var copy = new Tree<TValue>();

			ICloneable iClonable = this.Value as ICloneable;
			if(iClonable != null)
				copy.Value = (TValue)iClonable.Clone();
			else
				copy.Value = this.Value;

			foreach(var childNode in ChildNodesAtLevelIterator)
			{
				copy.AddNode( childNode.CloneTree() );
			}

			return copy;
		}

		#endregion
	}
}
