﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;

namespace DiskContentInspector.Core
{
	[DebuggerDisplay("Name = {Name}")]
	abstract internal class FsElementBase
	{
		public enum Type
		{
			File,
			Folder
		}

		private readonly Tree<FsElementBase> node;
		private readonly Type elementType;
		private readonly string name;

		protected FsElementBase(string name, Type elementType, FileSystemAccessRule[] acl, Tree<FsElementBase> node)
		{
			this.node = node;
			this.acl = acl;
			this.node.Value = this;
			this.elementType = elementType;
			this.name = name;
		}

		private readonly FileSystemAccessRule[] acl;
		public FileSystemAccessRule[] ACL
		{
			get
			{
				return acl;
			}
		}

		public string Name
		{
			get
			{
				return name;
			}
		}

		public Type ElementType
		{
			get
			{
				return elementType;
			}
		}

		public string FullName
		{
			get
			{
				IEnumerable<string> parts = this.node.FromThisToParentIterator
					.Reverse()
					.Select( el => el.Value.Name.TrimEnd( Path.DirectorySeparatorChar ) );
				string res = String.Join( new string( Path.DirectorySeparatorChar, 1), parts );

				if(this.elementType == Type.Folder)
					res += Path.DirectorySeparatorChar;

				return res;
			}
		}

		public Tree<FsElementBase> Node
		{
			get
			{
				return this.node;
			}
		}

		public abstract string Ext { get; }
		public abstract long Size { get; }
		public abstract long? CountOfFilesTotal { get; }
		public abstract long? CountOfFilesOneLevel { get; }
		public abstract DateTime CreationTime { get; }
		public abstract DateTime LastChangeTime { get; }
		public abstract string Md5 { get; }
	}
}