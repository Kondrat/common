﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DiskContentInspector.Core;

namespace DiskContentInspector
{
	public partial class MainForm :Form
	{
		private WaitingStopForm waitingStopForm = null;

		private CultureInfo cultureInfo;
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			this.btnShowHideTree.Checked = this.directoryTree.Visible;

			cultureInfo = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
			cultureInfo.NumberFormat.NumberGroupSeparator = " ";
			cultureInfo.NumberFormat.NumberDecimalDigits = 0;

			this.directoryTree.AddNetworkSharesFromSettings( Settings.ReadShareList() );
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = !this.btnClose.Enabled;
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Settings.WriteShareList(this.directoryTree.GetNetworkShareList());
			this.Close();
		}

		private string RulesToString(IEnumerable<FileSystemAccessRule> rules)
		{
			string result = string.Empty;

			IEnumerable<FileSystemAccessRule> seq = this.btnShowInheritedAcl.Checked? rules: rules.Where( el => !el.IsInherited );

			foreach(FileSystemAccessRule rule in seq)
			{
				string access = string.Format("{0}{1} - {2}: {3}", 
					rule.IsInherited? " ^": string.Empty, 
					rule.IdentityReference.Value, 
					rule.AccessControlType, 
					rule.FileSystemRights
					);

				if(result.Length > 0)
					result += Environment.NewLine;

				result += access;
			}
			return result;
		}

		private void dgw_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			try
			{
				Thread thread = Thread.CurrentThread;
				var oldCulture = thread.CurrentCulture;
				thread.CurrentCulture = cultureInfo;

				if(e.RowIndex < this.gridData.Length)
				{
					GridRow row = this.gridData[e.RowIndex];
					switch(e.ColumnIndex)
					{
						case 0:
							e.Value = (e.RowIndex + 1).ToString();
							break;
						case 1:
							e.Value = row.ElementType;
							break;
						case 2:
							e.Value = row.Name;
							break;
						case 3:
							e.Value = row.Ext;
							break;
						case 4:
							e.Value = row.FullName;
							break;
						case 5:
							e.Value = row.Size;
							break;
						case 6:
							e.Value = row.CountOfFilesOneLevel;
							break;
						case 7:
							e.Value = row.CountOfFilesTotal;
							break;
						case 8:
							e.Value = row.CreationTime;
							break;
						case 9:
							e.Value = row.LastChangeTime;
							break;
						case 10:
							e.Value = RulesToString(row.Acl);
							break;
						case 11:
							e.Value = row.Md5;
							break;
					}
				}

				thread.CurrentCulture = oldCulture;
			}
			catch(Exception ex)
			{
				MsgBoxes.ErrorDlg( this, ex.Message );
			}
		}

		private DataCollector dataCollector;
		private DateTime timeOfScanStart;
		private Tree<FsElementBase> tree;
		private GridRow[] gridData = new GridRow[0];

		private void btnScan_Click(object sender, EventArgs e)
		{
			var pathToScan = this.directoryTree.SelectedPath;
			if(pathToScan == null)
			{
				MsgBoxes.StopDlg(this, "Please select disk, folder or share");
			}
			else
			{
				if(btnMd5.Checked)
					lblMd5Progress.Visible = true;

				this.btnScan.Enabled = false;
				this.btnClose.Enabled = false;
				this.btnStopScan.Enabled = true;
				this.Cursor = Cursors.AppStarting;
				this.dgw.Cursor = Cursors.AppStarting;

				this.timeOfScanStart = DateTime.Now;

				Task<Tree<FsElementBase>> collectorTask = new Task<Tree<FsElementBase>>(() => GetTreeAsync(pathToScan));

				collectorTask.ContinueWith(ScanCompletedAsync);

				collectorTask.Start();
				ShowScanningTime();
				StartAnimation();

				ShowHideDoublicatesPane( false );
				btnDoublicates.Checked = false;
			}
		}

		private void ScanCompletedAsync(Task<Tree<FsElementBase>> task)
		{
			if(task.Status == TaskStatus.Faulted)
			{
				Exception ex = task.Exception.InnerException;
				this.Invoke(() =>
				{
					MsgBoxes.ErrorDlg( ex.Message );
					ScanConpleted( null );
				});
			}
			else
				this.Invoke( () => ScanConpleted( task.Result ) );
		}

		private void ScanConpleted(Tree<FsElementBase> result)
		{
			StopAnimation();
			this.btnScan.Enabled = true;
			this.btnClose.Enabled = true;
			this.btnStopScan.Enabled = false;
			this.Cursor = Cursors.Default;
			this.dgw.Cursor = Cursors.Default;

			lblScanProgress.Text = string.Empty;
			lblMd5Progress.Text = string.Empty;
			lblMd5Progress.Visible = false;

			ShowScanningTime();
			this.dataCollector = null;

			if(this.waitingStopForm != null)
			{
				this.waitingStopForm.Close();
				this.waitingStopForm = null;
			}

			if(result != null)
			{
				this.tree = result;
				ShowResultInGrid(this.tree);
			}
		}

		private void ShowResultInGrid(Tree<FsElementBase> result)
		{
			if(result != null)
			{
				bool bShowFiles = btnShowFiles.Checked;

				this.gridData = result.AllNodesIterator
					.Select(el => el.Value)
					.Where(el => bShowFiles ? true : el.ElementType == FsElementBase.Type.Folder)
					.Select(el => new GridRow(el))
					.ToArray();

				this.dgw.Rows.Clear();

				this.dgw.RowCount = this.gridData.Length;
				ShowRowCount( this.dgw.RowCount );

				this.lblFsCount.Text = string.Format( "Folders: {0}, files: {1}", 
					this.tree.AllNodesIterator.Where( el => el.Value.ElementType == FsElementBase.Type.Folder ).Count(),
					this.tree.AllNodesIterator.Where( el => el.Value.ElementType == FsElementBase.Type.File ).Count()
					);

				//this.dgw.AutoResizeColumn( colACL.Index );
				//this.dgw.AutoResizeRows();
			}
			else
			{
				this.dgw.Rows.Clear();
				ShowRowCount( 0 );
				this.lblFsCount.Text = string.Empty;
			}

			this.dgw.Invalidate();
		}

		private void ShowRowCount(int count)
		{
			this.lblRowCount.Text = "Rows: " + count;
		}

		private MessagesForm messagesForm = null;
		public void ErrorNotificationAsync(string message)
		{
			this.Invoke( () =>
			{
				if(this.messagesForm == null)
				{
					this.messagesForm = new MessagesForm();
					this.messagesForm.Show();
					this.messagesForm.Closed += (e, a)=> this.messagesForm = null;
				}

				this.messagesForm.AddMessage( message );
			} );
		}

		private Tree<FsElementBase> GetTreeAsync(string path)
		{
			this.dataCollector = new DataCollector(ErrorNotificationAsync);
			return this.dataCollector.Read(path, btnMd5.Checked, ScanProgressNotificationAsync, Md5ProgressNotificationAsync);
		}

		private void Md5ProgressNotificationAsync(string msg)
		{
			this.Invoke( () =>
			{
				lblMd5Progress.Text = msg;
			} );
		}

		private void ScanProgressNotificationAsync(string stage, string path)
		{
			this.Invoke(() =>
			{
				lblTaskName.Text = stage;
				lblScanProgress.Text = path;
			});
		}

		private void btnStopScan_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.Default;
			using(SetCursor.Wait(this.FindForm()))
			{
				if(this.dataCollector != null)
				{
					bool bNeedStop = true;

					if(ScaningTime.TotalSeconds > 10)
						bNeedStop = MsgBoxes.QuestionDlg( this, "Stop scaning?",MessageBoxDefaultButton.Button2 ) == DialogResult.Yes;

					if(bNeedStop)
					{
						ShowWaitStopForm();

						this.btnStopScan.Enabled = false;
						this.FindForm().Refresh();
						this.dataCollector.RequestStop();

						Thread.Sleep(200);
					}
				}
			}
		}

		private void ShowWaitStopForm()
		{
			this.waitingStopForm = new WaitingStopForm();

			Point locPoint = new Point
				{
					X = this.Location.X + (this.Size.Width - this.waitingStopForm.Size.Width) / 2, 
					Y = this.Location.Y + (this.Size.Height - this.waitingStopForm.Size.Height) / 2
				};
			this.waitingStopForm.Location = locPoint ;

			this.waitingStopForm.Show(this);
			this.waitingStopForm.Refresh();
		}

		private void idleTimer_Tick(object sender, EventArgs e)
		{
			if(this.dataCollector == null)
				this.lblTaskName.Text = "None";

			if(this.dataCollector != null)
				ShowScanningTime();
		}

		private void ShowScanningTime()
		{
			TimeSpan ts = ScaningTime;
			this.lblScanTime.Text = string.Format("{0}.{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
		}

		private TimeSpan ScaningTime
		{
			get
			{
				return DateTime.Now - this.timeOfScanStart;
			}
		}

		private void btnShowHideTree_Click(object sender, EventArgs e)
		{
			this.directoryTree.Visible = this.splitter1.Visible = this.btnShowHideTree.Checked;
		}

		private void btnShowFiles_Click(object sender, EventArgs e)
		{
			using(SetCursor.Wait( this ))
			{
				ShowResultInGrid(this.tree);
			}
		}

		private void btnShowInheritedAcl_Click(object sender, EventArgs e)
		{
			using(SetCursor.Wait(this))
			{
				ShowResultInGrid(this.tree);
			}
		}

		private void btnMd5_Click(object sender, EventArgs e)
		{
			if(btnMd5.Checked && !btnShowFiles.Checked)
				btnShowFiles.Checked = true;
		}

		private void btnExportToCsv_Click(object sender, EventArgs e)
		{
			try
			{
				if(sfd.ShowDialog(this) == DialogResult.OK)
				{
					DataGridView gridView = this.dgw;
					string fileName = sfd.FileName;
					using(SetCursor.Wait(this))
					{
						Helper.WriteDataGridViewContentToCsv( gridView, fileName );
					}
				}
			}
			catch(Exception ex)
			{
				MsgBoxes.ErrorDlg( this, ex.Message );
			}
		}

		private void txtNeworkPath_TextChanged(object sender, EventArgs e)
		{
			btnAddNetworkShare.Enabled = txtNeworkPath.TextLength > 0;
		}

		private void btnCopyWithHeaders_Click(object sender, EventArgs e)
		{
			CopyToClipboard( DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText );
		}

		private void btnCopyOnlyCells_Click(object sender, EventArgs e)
		{
			CopyToClipboard(DataGridViewClipboardCopyMode.EnableWithoutHeaderText);
		}

		private void CopyToClipboard(DataGridViewClipboardCopyMode clipboardCopyMode)
		{
			this.dgw.SelectAll();
			this.dgw.ClipboardCopyMode = clipboardCopyMode;
			var data = this.dgw.GetClipboardContent();
			Clipboard.SetDataObject( data );
		}

		private void btnAddNetworkShare_Click(object sender, EventArgs e)
		{
			using(SetCursor.Wait( this ))
			{
				string share = txtNeworkPath.Text.Trim();
				try
				{
					if(new Uri( share ).IsUnc)
						this.directoryTree.AddNetworkShare( share );
					else
						MsgBoxes.StopDlg( this, "This path is not UNC." );
				}
				catch(Exception ex)
				{
					MsgBoxes.StopDlg( this, ex.Message );
				}
			}
		}

		private void directoryTree_AfterSelect(object sender, TreeViewEventArgs e)
		{
			this.btnRemoveShare.Enabled = this.directoryTree.IsSelectedPathIsShare;
		}

		private void btnRemoveShare_Click(object sender, EventArgs e)
		{
			this.directoryTree.RemoveSelectedShare();
			Settings.WriteShareList(this.directoryTree.GetNetworkShareList());
		}

		private void btnDoublicates_Click(object sender, EventArgs e)
		{
			ShowHideDoublicatesPane( btnDoublicates.Checked );
			FileFsElement[] files = null;
			if(tree != null)
			{
				files = this.tree.AllNodesIterator
					.Where(el => el.Value.ElementType == FsElementBase.Type.File)
					.Select(el => el.Value)
					.Cast<FileFsElement>()
					.ToArray();
			}

			this.doublicatesCtrl.SetData( files);
		}

		private void ShowHideDoublicatesPane(bool bVisible)
		{
			this.doublicatesCtrl.Visible = splitterDoublicates.Visible = bVisible;
		}

		private string nonAnimatedHeader;
		private const string cnstJobSymbol = ">>";

		private void StartAnimation()
		{
			this.nonAnimatedHeader = this.Text;
			this.Text = cnstJobSymbol + " " + this.Text;
		}

		private void StopAnimation()
		{
			this.Text = this.nonAnimatedHeader;
		}

		private void dgw_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if(e.RowIndex < 0)
				return;

			GridRow row = this.gridData[e.RowIndex];

			string argument = string.Format("/select, \"{0}\"", row.FullName);
			System.Diagnostics.Process.Start("explorer.exe", argument);
		}
	}
}
